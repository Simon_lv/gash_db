package tw.tw360.query;

import java.sql.Timestamp;

import tw.tw360.dto.Payment;

@SuppressWarnings("serial")
public class PaymentQuery extends Payment{

	private Timestamp restoreTime;
	private String sn;
	private String carId;//隊編
	private int price;
	private int nums;
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getNums() {
		return nums;
	}
	public void setNums(int nums) {
		this.nums = nums;
	}
	public Timestamp getRestoreTime() {
		return restoreTime;
	}
	public void setRestoreTime(Timestamp restoreTime) {
		this.restoreTime = restoreTime;
	}
	public String getSn() {
		return sn;
	}
	public void setSn(String sn) {
		this.sn = sn;
	}
	public String getCarId() {
		return carId;
	}
	public void setCarId(String carId) {
		this.carId = carId;
	}
	
}
