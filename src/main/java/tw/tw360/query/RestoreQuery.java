package tw.tw360.query;

import tw.tw360.dto.Restore;

/**
 * <pre>
 * 程式功能: 
 * 程式日期: $Date$
 * 程式版本: $Id$
 * 上版人員: $Author$
 * 程式備註:
 * </pre> 
 */
public class RestoreQuery extends Restore {

    /*
     * 隊編
     */
    private String carId;

    public String getCarId() {
        return carId;
    }

    public void setCarId(String carId) {
        this.carId = carId;
    }
    
}
