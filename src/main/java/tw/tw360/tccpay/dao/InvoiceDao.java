package tw.tw360.tccpay.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.Invoice;

@Repository("tccpayInvoiceDao")
public class InvoiceDao extends BaseDao<Object> {
	@Resource(name = "dsMnTccpayQry")
	private DataSource dsMnTccpayQry;

	@Resource(name = "dsMnTccpayUpd")
	private DataSource dsMnTccpayUpd;

	@Autowired
	public InvoiceDao(@Qualifier("dsMnTccpayUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public void updateAddressAndInvoiceNo(String orderId, String address, String invoiceNo) {
//		int order = this.queryForInt(dsMnTccpayUpd, "select count(*) from invoice where order_id = ?", new Object[] { orderId });
//		if (order > 0) {
			this.updateForObject(dsMnTccpayUpd, "update invoice set invoice_address = ?, invoice_no = ? where order_id = ?", new Object[] { address, invoiceNo,
					orderId });
//		} else {
//			this.addForObject(dsMnTccpayUpd, "insert into invoice (order_id, invoice_no, invoice_address) values (?,?,?)", new Invoice(), new Object[] {
//					orderId, invoiceNo, address });
//		}
	}
	
	public void updateInvoice(String orderId, String address, String invoiceNo, String invoiceType, String invoiceName, String invoicePhone,
			String invoiceAddress, String invoiceTitle, String taxId) {
		this.updateForObject(
				dsMnTccpayUpd,
				"update invoice set invoice_no = ?, invoice_type = ?, invoice_name = ? ,invoice_phone = ? ,invoice_address = ? ,invoice_title = ? ,tax_id  = ? where order_id = ?",
				new Object[] { invoiceNo, invoiceType, invoiceName, invoicePhone, invoiceAddress, invoiceTitle, taxId, orderId });
	}

	public boolean exist(String orderId) {
		String sql = "SELECT COUNT(1) FROM invoice WHERE order_id = ?";
		return super.queryForInt(dsMnTccpayQry, sql, new Object[]{orderId}) > 0;
	}

	public void insertInvoice(String orderId, String address, String invoiceNo, String invoiceType, String invoiceName, String invoicePhone, String invoiceAddress, String invoiceTitle, String taxId) {
		this.updateForObject(
				dsMnTccpayUpd,
				"INSERT INTO invoice(invoice_no, invoice_type, invoice_name, invoice_phone, invoice_address, invoice_title, tax_id, order_id) VALUES (?,?,?,?,?,?,?,?)",
				new Object[] {invoiceNo, invoiceType, invoiceName, invoicePhone, invoiceAddress, invoiceTitle, taxId, orderId});
	}
}
