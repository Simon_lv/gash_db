package tw.tw360.tccpay.dao;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.tw360.tccpay.model.Member;

@Repository
public class MemberDao extends BaseDao{
	@Resource(name = "dsMnTccpayQry")
	private DataSource dsMnTccpayQry;
	
	@Resource(name = "dsMnTccpayUpd")
	private DataSource dsMnTccpayUpd;
	
	@Autowired
	public MemberDao(@Qualifier("dsMnTccpayUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	public int authenticate(String uid, String payFrom) {
		String sql = "UPDATE member SET `status`=1 WHERE uid=? AND pay_from=?";
		return updateForObject(dsMnTccpayUpd, sql, new Object[] {uid, payFrom});
	}

	public Member findOne(String uid, String payFrom) {
		String sql = "SELECT * FROM member WHERE uid=? AND pay_from=?";
		Object[] args = new Object[]{uid, payFrom};
		return (Member) queryForObject(dsMnTccpayQry, sql, args, Member.class);
	}

	public void updateById(String uid, String name, String mobile, String address) {
		String sql = "UPDATE member SET name=?, mobile=?, address=? WHERE uid=?";
		updateForObject(dsMnTccpayUpd, sql, new Object[] { name, mobile, address, uid });
	}
}
