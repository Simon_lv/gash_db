package tw.tw360.tccpay.dao;

import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.tw360.tccpay.model.Customer;

@Repository
public class CustomerDao extends BaseDao {
	@Resource(name = "dsMnTccpayQry")
	private DataSource dsMnTccpayQry;
	
	@Resource(name = "dsMnTccpayUpd")
	private DataSource dsMnTccpayUpd;
	
	@Autowired
	public CustomerDao(@Qualifier("dsMnTccpayUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	public Customer findByPayFrom(String payFrom) {
		final String sql = "select * from customer where pay_from=?";
		return (Customer) queryForObject(dsMnTccpayQry, sql, new Object[] {payFrom} , Customer.class);
	}


	public List queryAllPayFrom() {
		final String sql = "select pay_from from customer ORDER BY pay_from";
		return this.queryForObjectList(dsMnTccpayQry, sql, new Object[] {} , String.class);
	}

}
