package tw.tw360.tccpay.dao;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import tw.tw360.tccpay.model.Payment;
import tw.tw360.tccpay.model.PaymentForView;

@Repository
public class TccpayPaymentDao extends BaseDao {
	private static final int COUNT = 0;

	private static final int LIST = 1;

	private static final int MONEY = 2;

	@Resource(name = "dsMnTccpayQry")
	private DataSource dsMnTccpayQry;

	@Resource(name = "dsMnTccpayUpd")
	private DataSource dsMnTccpayUpd;

	@Autowired
	public TccpayPaymentDao(@Qualifier("dsMnTccpayUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public int findByViewConditionCount(String payOrderId, String payStatus, int restoreStatus, String carId, Timestamp startCreateTime,
			Timestamp endCreateTime, int isTest, String payFrom, int invoiceType, String invoiceNo, String channel) {
		StringBuilder condition = new StringBuilder();
		StringBuilder condition2 = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();

		addPayOrderId(condition, params, payOrderId);
		addPayStatus(condition, params, payStatus);
		addRestoreStatus(condition, params, restoreStatus);
		addCarId(condition, params, carId);
		addMinCreateTime(condition, params, startCreateTime);
		addMaxCreateTime(condition, params, endCreateTime);
		addIsTest(condition, params, isTest);
		addPayFrom(condition, params, payFrom);
		addChannel(condition, params, channel);

		addInvoiceType(condition2, params, invoiceType);
		addInvoiceNo(condition2, params, invoiceNo);
		String sql = queryMaker(condition, false, COUNT, condition2);// " SELECT COUNT(*) FROM `payment` WHERE 1=1 "
		// +
		// condition.toString();
		return queryForInt(dsMnTccpayQry, sql, params.toArray());
	}

	public List<PaymentForView> findByViewCondition(String payOrderId, String payStatus, int restoreStatus, String carId, Timestamp startCreateTime,
			Timestamp endCreateTime, int isTest, String payFrom, int pageNo, int pageSize, int invoiceType, String invoiceNo, String channel) {
		StringBuilder condition = new StringBuilder();
		StringBuilder condition2 = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();

		addPayOrderId(condition, params, payOrderId);
		addPayStatus(condition, params, payStatus);
		addRestoreStatus(condition, params, restoreStatus);
		addCarId(condition, params, carId);
		addMinCreateTime(condition, params, startCreateTime);
		addMaxCreateTime(condition, params, endCreateTime);
		addIsTest(condition, params, isTest);
		addPayFrom(condition, params, payFrom);
		addChannel(condition, params, channel);
		addInvoiceType(condition2, params, invoiceType);
		addInvoiceNo(condition2, params, invoiceNo);

		String sql = queryMaker(condition, true, LIST, condition2);

		params.add((pageNo - 1) * pageSize);
		params.add(pageSize);
		return queryForList(dsMnTccpayQry, sql, params.toArray(), PaymentForView.class);
	}

	public List<PaymentForView> queryAll(String payOrderId, String payStatus, int restoreStatus, String carId, Timestamp startCreateTime,
			Timestamp endCreateTime, int isTest, String payFrom, int invoiceType, String invoiceNo, String channel, String sPayFrom) {
		StringBuilder condition = new StringBuilder();
		StringBuilder condition2 = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();

		addSPayFrom(condition, params, sPayFrom);
		addPayOrderId(condition, params, payOrderId);
		addPayStatus(condition, params, payStatus);
		addRestoreStatus(condition, params, restoreStatus);
		addCarId(condition, params, carId);
		addMinCreateTime(condition, params, startCreateTime);
		addMaxCreateTime(condition, params, endCreateTime);
		addIsTest(condition, params, isTest);
		addPayFrom(condition, params, payFrom);
		addChannel(condition, params, channel);
		addInvoiceType(condition2, params, invoiceType);
		addInvoiceNo(condition2, params, invoiceNo);
		addChannel(condition, params, channel);
		String sql = queryMaker(condition, false, LIST, condition2);
		if (params.size() == 0) {
			return queryForList(dsMnTccpayQry, sql, new Object[] {}, PaymentForView.class);
		}
		return queryForList(dsMnTccpayQry, sql, params.toArray(), PaymentForView.class);
	}

	private String queryMaker(StringBuilder condition, boolean pageable, int type, StringBuilder condition2) {
		StringBuilder sb = new StringBuilder("SELECT ");

		switch (type) {
		case COUNT:
			sb.append(" count(1) ");
			break;
		case LIST:
			sb.append(" payment1.channel,payment1.`id` `id`, payment1.money, payment1.user_address, payment1.car_id, payment1.pay_from, payment1.currency, payment1.create_time create_time, payment1.restore_status, payment1.`name` `name`, payment1.user_mobile, payment1.remark, game.`name` game_name, payment1.uid uid, payment1.account_status, payment1.pay_status, payment1.order_id, payment1.cancel_time, payment1.pay_order_id, payment1.driver_id, payment1.is_first, payment1.pay_time, IFNULL(member.`status`,0) member_status , payment1.is_test,"
					+ "IFNULL(invoice.invoice_type, -1) invoice_type, invoice.invoice_no ");
			break;
		case MONEY:
			sb.append(" SUM(money) ");
			break;
		}

		sb.append(" FROM ( (SELECT * FROM `payment` WHERE 1=1 ").append(condition.toString()).append(" ) payment1 ")
				.append(" LEFT JOIN game ON payment1.game_code = game.game_code ")
				.append(" LEFT JOIN member ON (payment1.uid = member.uid AND payment1.pay_from = member.pay_from) ");
		if (condition2.length() == 0) {
			sb.append(" LEFT JOIN (SELECT * FROM invoice WHERE 1 = 1 ");
		} else {
			sb.append(" JOIN (SELECT * FROM invoice WHERE 1 = 1 ");
			sb.append(condition2.toString());
		}

		sb.append(") invoice ON (payment1.order_id = invoice.order_id ) ) ").append(" ORDER BY payment1.id DESC " + (pageable == true ? " LIMIT ?, ? " : ""));

		return sb.toString();
	}

	private void addInvoiceType(StringBuilder condition, ArrayList<Object> params, int invoiceType) {
		if (invoiceType == -1) {
			return;
		} else {
			condition.append(" AND invoice_type= ? ");
			params.add(invoiceType);
		}
	}

	private void addInvoiceNo(StringBuilder condition, ArrayList<Object> params, String invoiceNo) {
		if (StringUtils.isBlank(invoiceNo)) {
			return;
		} else {
			condition.append(" AND invoice_no= ? ");
			params.add(invoiceNo);
		}
	}

	private void addChannel(StringBuilder condition, ArrayList<Object> params, String channel) {
		if (StringUtils.isBlank(channel)) {
			return;
		} else {
			condition.append(" AND channel= ? ");
			params.add(channel);
		}
	}

	private void addCurrency(StringBuilder condition, ArrayList<Object> params, String currency) {
		if (StringUtils.isBlank(currency)) {
			return;
		} else {
			condition.append(" AND currency= ? ");
			params.add(currency);
		}
	}

	private void addPayFrom(StringBuilder condition, ArrayList<Object> params, String payFrom) {
		if (StringUtils.isBlank(payFrom)) {
			return;
		} else {
			condition.append(" AND pay_from= ? ");
			params.add(payFrom);
		}
	}

	private void addIsTest(StringBuilder condition, ArrayList<Object> params, int isTest) {
		if (isTest == -1) {
			return;
		} else {
			condition.append(" AND is_test= ? ");
			params.add(isTest);
		}
	}

	private void addMaxCreateTime(StringBuilder condition, ArrayList<Object> params, Timestamp endCreateTime) {
		if (endCreateTime == null) {
			return;
		} else {
			condition.append(" AND create_time <= ? ");
			params.add(endCreateTime);
		}
	}

	private void addMinCreateTime(StringBuilder condition, ArrayList<Object> params, Timestamp startCreateTime) {
		if (startCreateTime == null) {
			return;
		} else {
			condition.append(" AND create_time >= ? ");
			params.add(startCreateTime);
		}
	}

	private void addCarId(StringBuilder condition, ArrayList<Object> params, String carId) {
		if (carId == null || carId.length() == 0) {
			return;
		} else {
			condition.append(" AND channel= ? ");
			params.add(carId);
		}
	}

	private void addRestoreStatus(StringBuilder condition, ArrayList<Object> params, int restoreStatus) {
		if (restoreStatus == -1) {
			return;
		} else {
			condition.append(" AND restore_status= ? ");
			params.add(restoreStatus);
		}
	}

	private void addPayStatus(StringBuilder condition, ArrayList<Object> params, String payStatus) {
		// 1,2,3... or int
		if (StringUtils.isEmpty(payStatus) || StringUtils.equals("-1", payStatus)) {
			return;
		} else {
			if (StringUtils.isNumeric(payStatus)) {
				condition.append(" AND pay_status= ? ");
				params.add(Integer.parseInt(payStatus));
			} else {
				condition.append(String.format(" AND pay_status in (%s)", payStatus));
			}
		}
	}

	private void addSPayFrom(StringBuilder condition, ArrayList<Object> params, String sPayFrom) {
		if (sPayFrom == null || sPayFrom.length() == 0) {
			return;
		} else {
			condition.append(" AND pay_from = ? ");
			params.add(sPayFrom);
		}
	}
	
	private void addPayOrderId(StringBuilder condition, ArrayList<Object> params, String payOrderId) {
		if (payOrderId == null || payOrderId.length() == 0) {
			return;
		} else {
			condition.append(" AND pay_order_id = ? ");
			params.add(payOrderId);
		}
	}

	public PaymentForView findOneById(long id) {
		// StringBuilder condition = new StringBuilder();
		// ArrayList<Object> params = new ArrayList<Object>();

		String sql = "SELECT payment1.`id` `id`, money, user_address, car_id, payment1.pay_from, payment1.create_time create_time, restore_status, payment1.`name` `name`, user_mobile, remark, game.`name` game_name, payment1.uid uid, account_status, pay_status, payment1.order_id, cancel_time, pay_order_id, driver_id, is_first, pay_time, IFNULL(member.`status`,0) member_status,is_test,invoice.invoice_address,payment1.channel, invoice.invoice_no,IFNULL(invoice.invoice_type, -1) invoice_type,invoice.invoice_name,invoice.invoice_phone,invoice.invoice_address,invoice.invoice_title,invoice.tax_id FROM ( "
				+ " (SELECT * FROM `payment` WHERE payment.id = ? "
				+ " ) payment1 "
				+ " LEFT JOIN game ON payment1.game_code = game.game_code "
				+ " LEFT JOIN member ON (payment1.uid = member.uid AND payment1.pay_from = member.pay_from)) "
				+ " LEFT JOIN invoice ON invoice.order_id = payment1.order_id ";
		return (PaymentForView) queryForObject(dsMnTccpayQry, sql, new Object[] { id }, PaymentForView.class);
	}

	public int updatePayStatus(long id, int payStatus) {
		String sql = "UPDATE payment SET pay_status=? WHERE id=?";
		return updateForObject(dsMnTccpayUpd, sql, new Object[] { payStatus, id });
	}

	public int updatePayStatus(long id, int nowPayStatus, int newPayStatus) {
		String sql = "UPDATE payment SET pay_status=? WHERE id=? AND pay_status=?";
		return updateForObject(dsMnTccpayUpd, sql, new Object[] { newPayStatus, id, nowPayStatus });
	}

	public void updateById(long id, String remark, String name, String mobile, String address, String isTest, String channel, String restoreStatus) {
		String sql = "UPDATE payment SET remark=?, name=?, user_mobile=?, user_address=?, is_test=?,channel = ?,restore_status = ? WHERE id=?";
		updateForObject(dsMnTccpayUpd, sql, new Object[] { remark, name, mobile, address, isTest, channel, restoreStatus, id });
	}

	public Payment add(final Payment pay) {
		final String sql = "INSERT INTO payment(" + "order_id,pay_from,pay_order_id,uid,game_code,money,"
				+ "name,user_mobile,user_address,create_time,is_first) values " + "(?,?,?,?,?,?,?,?,?,?,?)";

		return (Payment) addForObject(
				dsMnTccpayUpd,
				sql,
				pay,
				new Object[] { pay.getOrderId(), pay.getPayFrom(), pay.getPayOrderId(), pay.getUid(), pay.getGameCode(), pay.getMoney(), pay.getName(),
						pay.getUserMobile(), pay.getUserAddress(), pay.getCreateTime(), pay.getIsFirst() });
	}

	public int countPay(String uid) {
		final String sql = "select count(*) from payment where pay_status>1 and uid=?";
		return this.queryForInt(dsMnTccpayQry, sql, new Object[] { uid });
	}

	public void updateRestore(String[] payList, Date date, int rstatus) {
		StringBuilder sb = new StringBuilder();
		sb.append("update payment set restore_time=?, restore_status=? where order_id in (");

		for (String payId : payList) {
			sb.append("'").append(payId).append("',");
		}

		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		super.updateForObject(dsMnTccpayUpd, sb.toString(), new Object[] { date, rstatus });
	}

	public void updateRestore(String[] payList, int ptatus, int rstatus) {
		StringBuilder sb = new StringBuilder();
		sb.append("update payment set pay_status=?, restore_status=? where order_id in (");

		for (String payId : payList) {
			sb.append("'").append(payId).append("',");
		}

		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		super.updateForObject(dsMnTccpayUpd, sb.toString(), new Object[] { ptatus, rstatus });
	}

	public boolean updateCarIdDerverID(Long id, String carId, Long derverId, int isTest) {
		String sql = "UPDATE payment SET car_id=?, driver_id=?, is_test=? WHERE id=?";
		return updateForObject(dsMnTccpayUpd, sql, new Object[] { carId, derverId, isTest, id }) > 0;
	}

	public PaymentForView findByOrderId(String orderId) {
		String sql = "SELECT payment1.`id` `id`, money, user_address, car_id, payment1.pay_from, payment1.create_time create_time, restore_status, payment1.`name` `name`, user_mobile, remark, game.`name` game_name, payment1.uid uid, account_status, pay_status, order_id, cancel_time, pay_order_id, driver_id, is_first, pay_time, IFNULL(member.`status`,0) member_status, is_test FROM ( "
				+ " (SELECT * FROM `payment` WHERE payment.order_id = ? "
				+ " ) payment1 "
				+ " LEFT JOIN game ON payment1.game_code = game.game_code "
				+ " LEFT JOIN member ON (payment1.uid = member.uid AND payment1.pay_from = member.pay_from)) ";
		Object[] args = new Object[] { orderId };
		return (PaymentForView) queryForObject(dsMnTccpayQry, sql, args, PaymentForView.class);
	}

	public List<String> queryHasInvoiceNo(List<String> orderId) {
		String sql = "SELECT p.order_id FROM payment p left join invoice i on p.order_id = i.order_id WHERE (p.pay_status = 4 or p.pay_status = 5) and i.invoice_no is not null and (";
		for (int i = 0; i < orderId.size(); i++) {
			if (i != 0) {
				sql = sql + " or p.order_id = '" + orderId.get(i) + "'";
			} else {
				sql = sql + " p.order_id = '" + orderId.get(i) + "'";
			}
		}
		sql = sql + ")";
		return this.queryForObjectList(dsMnTccpayQry, sql, null, String.class);

	}

	public List<String> queryHasNotInvoiceNo(List<String> orderId) {
		String sql = "SELECT p.order_id FROM payment p left join invoice i on p.order_id = i.order_id  WHERE (p.pay_status = 4 or p.pay_status = 5) and (i.invoice_no is null or i.invoice_no = '') and (";
		for (int i = 0; i < orderId.size(); i++) {
			if (i != 0) {
				sql = sql + " or p.order_id = '" + orderId.get(i) + "'";
			} else {
				sql = sql + " p.order_id = '" + orderId.get(i) + "'";
			}
		}
		sql = sql + ")";
		return this.queryForObjectList(dsMnTccpayQry, sql, null, String.class);

	}

	public List<String> queryExistOrder(List<String> orderId) {
		String sql = "SELECT order_id FROM payment WHERE ";
		for (int i = 0; i < orderId.size(); i++) {
			if (i != 0) {
				sql = sql + " or order_id = '" + orderId.get(i) + "'";
			} else {
				sql = sql + " order_id = '" + orderId.get(i) + "'";
			}
		}
		return this.queryForObjectList(dsMnTccpayQry, sql, null, String.class);

	}

	public void updateOrderInvoiceNo(List<Object[]> params) {
		String sql = "update invoice set invoice_no = ? where order_id = ?";
		for (int i = 0; i < params.size(); i++) {
			this.updateForObject(dsMnTccpayUpd, sql, params.get(i));
		}

	}

	public Integer queryUnHandlePaymentCount(List<Object> params) {
		String sql = "select count(*) from payment where pay_status=0 AND is_test= 0";
		return this.queryForInt(dsMnTccpayQry, sql, params == null ? null : params.toArray());
	}

	public Double sumForPayment(String payOrderId, String payStatus, int restoreStatus, String carId, Timestamp startCreateTime, Timestamp endCreateTime,
			int isTest, String payFrom, int invoiceType, String invoiceNo, String channel, String currency) {
		StringBuilder condition = new StringBuilder();
		StringBuilder condition2 = new StringBuilder();
		ArrayList<Object> params = new ArrayList<Object>();

		addPayOrderId(condition, params, payOrderId);
		addPayStatus(condition, params, payStatus);
		addRestoreStatus(condition, params, restoreStatus);
		addCarId(condition, params, carId);
		addMinCreateTime(condition, params, startCreateTime);
		addMaxCreateTime(condition, params, endCreateTime);
		addIsTest(condition, params, isTest);
		addPayFrom(condition, params, payFrom);
		addChannel(condition, params, channel);
		addCurrency(condition, params, currency);

		addInvoiceType(condition2, params, invoiceType);
		addInvoiceNo(condition2, params, invoiceNo);
		String sql = queryMaker(condition, false, MONEY, condition2);// " SELECT SUM(money) FROM `payment` WHERE 1=1 "
		// +
		// condition.toString();

		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnTccpayQry);
		return jdbcTemplate.queryForObject(sql, params.toArray(), Double.class);
	}
}
