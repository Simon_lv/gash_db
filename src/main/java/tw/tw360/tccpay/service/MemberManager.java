package tw.tw360.tccpay.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.tccpay.dao.MemberDao;
import tw.tw360.tccpay.model.Member;

@Service
@Transactional
public class MemberManager {
	@Autowired
	private MemberDao memberDao;
	
	public int authenticate(String uid, String payFrom) {
		return memberDao.authenticate(uid, payFrom);
	}

	public Member findOne(String uid, String payFrom) {
		return memberDao.findOne(uid, payFrom);
	}

	public void updateById(String uid, String name, String mobile, String address) {
		memberDao.updateById(uid, name, mobile, address);
	}
	
}
