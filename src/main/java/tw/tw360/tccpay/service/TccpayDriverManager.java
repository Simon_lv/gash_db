package tw.tw360.tccpay.service;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.tccpay.dao.TccpayDriverDao;
import tw.tw360.tccpay.model.Driver;

@Service
@Transactional
public class TccpayDriverManager {

	@Autowired
	private TccpayDriverDao driverDao;

	public TccpayDriverDao getDriverDao() {
		return driverDao;
	}
	
//	public Page<Driver> queryUserList(Page<Driver> page,HashMap<String, String> queryParams){
//		StringBuffer strSQL = new StringBuffer("SELECT d.id,d.car_id,d.name,d.password,d.mobile,d.credit,d.non_restore,d.create_time,d.can_sell,d.status, d.is_tester FROM driver d where status=1"); 
//		StringBuffer pageSQL = new StringBuffer("SELECT count(d.id) FROM driver d where status=1"); 
//		
//		if(!StringUtils.isBlank(queryParams.get("carId"))){
//			strSQL.append(" and d.car_id='"+queryParams.get("carId")+"'");
//			pageSQL.append(" and d.car_id='"+queryParams.get("carId")+"'");
//		}
//		
//		if(StringUtils.isNotBlank(queryParams.get("name"))){
//			strSQL.append(" and d.name like'%"+queryParams.get("name")+"%'");
//			pageSQL.append(" and d.name like '%"+queryParams.get("name")+"%'");
//		}
//		if(StringUtils.isNotBlank(queryParams.get("mobile"))){
//			strSQL.append(" and d.mobile ='"+queryParams.get("mobile")+"'");
//			pageSQL.append(" and d.mobile ='"+queryParams.get("mobile")+"'");
//		}
//		if(StringUtils.isNotBlank(queryParams.get("canSell"))){
//			strSQL.append(" and d.can_sell ='"+queryParams.get("canSell")+"'");
//			pageSQL.append(" and d.can_sell ='"+queryParams.get("canSell")+"'");
//		}
//		
//		strSQL.append(" ORDER BY d.id desc limit "+page.getFirst()+","+page.getPageSize()+"");
//	
//		return driverDao.queryUserList(page,strSQL.toString(),pageSQL.toString());
//	 }
	public Page<Driver> queryUserList(Page<Driver> page,HashMap<String, String> queryParams){
        StringBuffer strSQL = new StringBuffer("SELECT d.is_tester,d.id,d.car_id,d.name,d.password,d.mobile,d.credit,d.non_restore,d.create_time,d.can_sell,d.status,d.short_code FROM driver d where status=1"); 
        StringBuffer pageSQL = new StringBuffer("SELECT count(d.id) FROM driver d where status=1"); 
        
        strSQL = makeQueryUserListSql(strSQL,queryParams);
        pageSQL = makeQueryUserListSql(pageSQL,queryParams);
        
        strSQL.append(" ORDER BY d.id desc limit "+page.getFirst()+","+page.getPageSize()+"");
    
        return driverDao.queryUserList(page,strSQL.toString(),pageSQL.toString());
     }
    public List<Driver> queryUserList(HashMap<String, String> queryParams){
        StringBuffer strSQL = new StringBuffer("SELECT d.is_tester,d.id,d.car_id,d.name,d.password,d.mobile,d.credit,d.non_restore,d.create_time,d.can_sell,d.status,d.short_code FROM driver d where status=1"); 
        
        strSQL = makeQueryUserListSql(strSQL,queryParams);
        
        strSQL.append(" ORDER BY d.id desc ");
    
        return driverDao.queryUserList(strSQL.toString());
     }
    private StringBuffer makeQueryUserListSql(StringBuffer strSQL,HashMap<String, String> queryParams){
        if(!StringUtils.isBlank(queryParams.get("carId"))){
            strSQL.append(" and d.car_id='"+queryParams.get("carId")+"'");
        }
        
        if(StringUtils.isNotBlank(queryParams.get("name"))){
            strSQL.append(" and d.name like'%"+queryParams.get("name")+"%'");
        }
        if(StringUtils.isNotBlank(queryParams.get("mobile"))){
            strSQL.append(" and d.mobile ='"+queryParams.get("mobile")+"'");
        }
        if(StringUtils.isNotBlank(queryParams.get("canSell"))){
            strSQL.append(" and d.can_sell ='"+queryParams.get("canSell")+"'");
        }
        return strSQL;
    }
//	public Driver add(Driver d) {
//		final String sql = "insert into driver(car_id,name,mobile,password,credit,non_restore,create_time,can_sell,status)values(?,?,?,?,?,?,?,?,?);";
//		Object[] parmas={d.getCarId(),d.getName(),d.getMobile(),d.getPassword(),d.getCredit(),d.getNonRestore(),d.getCreateTime(),d.getCanSell(),d.getStatus()};
//		return driverDao.save(sql,parmas,d);
//		
//	}
//	
//	public void update(Driver d) {
//		String sql = "update driver set name=?,mobile=?,credit=?,non_restore=?,create_time=?,can_sell=?,status=? where id=?";
//		Object[] params = {d.getName(),d.getMobile(),d.getCredit(),d.getNonRestore(),d.getCreateTime(),d.getCanSell(),d.getStatus(),d.getId()};
//		driverDao.update(sql, params, d);
//	}
    public Driver add(Driver d) {
        final String sql = "insert into driver(car_id,name,mobile,password,credit,non_restore,create_time,can_sell,status,short_code)values(?,?,?,?,?,?,?,?,?,?);";
        Object[] parmas={d.getCarId(),d.getName(),d.getMobile(),d.getPassword(),d.getCredit(),d.getNonRestore(),d.getCreateTime(),d.getCanSell(),d.getStatus(),d.getShortCode()};
        return driverDao.save(sql,parmas,d);
        
    }
    
    public void update(Driver d) {
        String sql = "update driver set name=?,mobile=?,credit=?,non_restore=?,create_time=?,can_sell=?,status=?,short_code=? where id=? ";
        Object[] params = {d.getName(),d.getMobile(),d.getCredit(),d.getNonRestore(),d.getCreateTime(),d.getCanSell(),d.getStatus(),d.getShortCode(),d.getId()};
        driverDao.update(sql, params, d);
    }
	public Driver findBycarId(String carId){
		String sql = "select * from driver where status=1 and car_id=? ";
		Object[] params = {carId};
		
		return driverDao.findBycarId(sql,params);
		
	}

	public void updateStatus(long id, int status) {
		String sql = "update driver set status=? where id=?";
		Object[] parmas ={status,id};
		driverDao.updateStatus(sql,parmas);
	}
	
	public int addNonRestore(int id, int nonRestore) {
		return driverDao.addNonRestore(id, nonRestore);
	}
	
	public Driver findById(int id) {
		return driverDao.findById(id);
	}
	
	public Driver findByMobile(String mobile) {
		return driverDao.findByMobile(mobile);
	}
	
	public int updatePwd(int id, String pwd) {
		return driverDao.updatePwd(id, pwd);
	}

	public List selectCanDispatch(int money) {
		return driverDao.selectCanDispatch(money);
	}

	public boolean updateNonRestore(String carId, int money) {
		return driverDao.updateNonRestore(carId, money);
	}
	 public List<Driver> queryALLUserList(HashMap<String, String> params) {
	        params.put("export", "export");
	        return queryUserList(params);
	    }

	public int addNonRestore(String carId, int money) {
		return driverDao.addNonRestore(carId, money);
	}
}
