package tw.tw360.tccpay.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.tccpay.dao.CustomerDao;
import tw.tw360.tccpay.model.Customer;

@Service
@Transactional
public class CustomerManager {
	@Autowired
	private CustomerDao customerDao;
	
	public Customer findByPayFrom(String payFrom) {
		return customerDao.findByPayFrom(payFrom);
	}

	public List queryAllPayFrom() {
		return customerDao.queryAllPayFrom();
	}
}
