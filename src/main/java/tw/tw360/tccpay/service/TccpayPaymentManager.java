package tw.tw360.tccpay.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.tccpay.dao.TccpayPaymentDao;
import tw.tw360.tccpay.model.Payment;
import tw.tw360.tccpay.model.PaymentForView;

@Service
@Transactional
public class TccpayPaymentManager {
	@Autowired
	private TccpayPaymentDao paymentDao;

	public List<PaymentForView> findByViewCondition(String payOrderId, String payStatus, int restoreStatus, String carId, Timestamp startCreateTime,
			Timestamp endCreateTime, int isTest, String payFrom, int pageNo, int pageSize, int invoiceType, String invoiceNo, String channel) {
		return paymentDao.findByViewCondition(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, isTest, payFrom, pageNo, pageSize,
				invoiceType, invoiceNo, channel);
	}

	public int findByViewConditionCount(String payOrderId, String payStatus, int restoreStatus, String carId, Timestamp startCreateTime,
			Timestamp endCreateTime, int isTest, String payFrom, int invoiceType, String invoiceNo, String channel) {
		return paymentDao.findByViewConditionCount(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, isTest, payFrom, invoiceType,
				invoiceNo, channel);
	}

	public PaymentForView findOneById(long id) {
		return paymentDao.findOneById(id);
	}

	public int updatePayStatus(long id, int payStatus) {
		return paymentDao.updatePayStatus(id, payStatus);
	}

	public int updatePayStatus(long id, int nowPayStatus, int newPayStatus) {
		return paymentDao.updatePayStatus(id, nowPayStatus, newPayStatus);
	}

	public void updateById(long id, String remark, String name, String mobile, String address, String isTest, String channel, String restoreStatus) {
		paymentDao.updateById(id, remark, name, mobile, address, isTest, channel, restoreStatus);
	}

	public Payment add(final Payment pay) {
		return paymentDao.add(pay);
	}

	public int countPay(String uid) {
		return paymentDao.countPay(uid);
	}

	public void updateRestore(String[] payList, Date date, int rstatus) {
		paymentDao.updateRestore(payList, date, rstatus);
	}

	public void updateCarIdDerverID(Long id, String carId, Long derverId, int isTest) {
		paymentDao.updateCarIdDerverID(id, carId, derverId, isTest);
	}

	/**
	 * 搜尋且不做分頁
	 * 
	 * @param payOrderId
	 * @param payStatus
	 * @param restoreStatus
	 * @param carId
	 * @param startCreateTime
	 * @param endCreateTime
	 * @param isTest
	 * @return
	 */
	public List<PaymentForView> queryAll(String payOrderId, String payStatus, int restoreStatus, String carId, Timestamp startCreateTime,
			Timestamp endCreateTime, int isTest, String payFrom, int invoiceType, String invoiceNo, String channel, String sPayFrom) {

		return paymentDao.queryAll(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, isTest, payFrom, invoiceType, invoiceNo,
				channel, sPayFrom);
	}

	public PaymentForView findByOrderId(String orderId) {
		return paymentDao.findByOrderId(orderId);
	}

	public int findUnHandlePayment() {
		return paymentDao.queryUnHandlePaymentCount(null);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Double querySumForPayment(String payOrderId, String payStatus, int restoreStatus, String carId, Timestamp startCreateTime, Timestamp endCreateTime,
			int isTest, String payFrom, int invoiceType, String invoiceNo, String channel, String currency) {
		return paymentDao.sumForPayment(payOrderId, payStatus, restoreStatus, carId, startCreateTime, endCreateTime, isTest, payFrom, invoiceType, invoiceNo,
				channel, currency);
	}
}
