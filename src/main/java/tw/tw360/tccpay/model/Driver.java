package tw.tw360.tccpay.model;

/*
* File		: Driver.java
* Date Created	: Mon May 04 15:34:17 CST 2015
*/

public class Driver extends BaseEntity {

	private String name;
	private int status;
	private String mobile;
	private int credit;
	private int isTester;
	private String password;
	private int canSell;
	private int nonRestore;
	private String carId;
	private java.sql.Timestamp createTime;
	private String shortCode;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public int getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}

	public int getIsTester() {
		return isTester;
	}

	public void setIsTester(int isTester) {
		this.isTester = isTester;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getCanSell() {
		return canSell;
	}

	public void setCanSell(int canSell) {
		this.canSell = canSell;
	}

	public int getNonRestore() {
		return nonRestore;
	}

	public void setNonRestore(int nonRestore) {
		this.nonRestore = nonRestore;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }
}