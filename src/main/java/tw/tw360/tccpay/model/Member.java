package tw.tw360.tccpay.model;

/*
* File		: Driver.java
* Date Created	: Mon May 04 15:34:17 CST 2015
*/

public class Member extends BaseEntity {
	private static final long serialVersionUID = 1L;
	
	private String uid;
	private String payFrom;
	private String name;
	private String mobile;
	private String address;
	private java.sql.Timestamp createTime;
	private java.sql.Timestamp lastPayTime;
	private int status;
	
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid = uid;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}
	public java.sql.Timestamp getLastPayTime() {
		return lastPayTime;
	}
	public void setLastPayTime(java.sql.Timestamp lastPayTime) {
		this.lastPayTime = lastPayTime;
	}
	public String getPayFrom() {
		return payFrom;
	}
	public void setPayFrom(String payFrom) {
		this.payFrom = payFrom;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
}