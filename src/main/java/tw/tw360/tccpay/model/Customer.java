package tw.tw360.tccpay.model;

import java.sql.Timestamp;

public class Customer extends BaseEntity {
	private static final long serialVersionUID = 1L;
	private String payFrom;
	private String payName;
	private String callbackUrl;
	private String key;
	private int sendPoint;
	private Timestamp createTime;
	private int status;
	public String getPayFrom() {
		return payFrom;
	}
	public void setPayFrom(String payFrom) {
		this.payFrom = payFrom;
	}
	public String getPayName() {
		return payName;
	}
	public void setPayName(String payName) {
		this.payName = payName;
	}
	public String getCallbackUrl() {
		return callbackUrl;
	}
	public void setCallbackUrl(String callbackUrl) {
		this.callbackUrl = callbackUrl;
	}
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getSendPoint() {
		return sendPoint;
	}
	public void setSendPoint(int sendPoint) {
		this.sendPoint = sendPoint;
	}
}
