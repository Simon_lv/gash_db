package tw.tw360.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.InvoiceCardCount;
import tw.tw360.dto.TccInvoice;

@Repository
public class TccInvoiceDao extends BaseDao {
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;

	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public TccInvoiceDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	public TccInvoice insert(TccInvoice tccInvoice) {
		String sql = "INSERT INTO `tcc_invoice` (`title`, `invoice_no`, `remark`, `create_time`, `status`) VALUES (?, ?, ?, ?, ?)";
		return (TccInvoice) super.addForObject(dsMnMainUpd, sql , tccInvoice, new Object[]{
			tccInvoice.getTitle(), tccInvoice.getInvoiceNo(), tccInvoice.getRemark(), tccInvoice.getCreateTime(), tccInvoice.getStatus()
		});
	}

	public boolean checkInvoiceNo(String invoiceNo) {
		String sql = "SELECT COUNT(*) count FROM tcc_invoice WHERE invoice_no=? ";
		return super.queryForInt(dsMnMainQry, sql, new Object[]{invoiceNo}) > 0;
	}
	
	public List<InvoiceCardCount> findAll(HashMap<String, String> map)
	  {
	    StringBuilder sb = new StringBuilder();
	    ArrayList<String> param = new ArrayList();
	    if (StringUtils.isNotBlank((CharSequence)map.get("startTime")))
	    {
	      sb.append(" AND create_time >= ? ");
	      param.add((String)map.get("startTime"));
	    }
	    if (StringUtils.isNotBlank((CharSequence)map.get("endTime")))
	    {
	      sb.append(" AND create_time <= ? ");
	      param.add((String)map.get("endTime"));
	    }
	    String sql = "SELECT id, title, invoice_no, c.price, c.count FROM `tcc_invoice` JOIN (SELECT invoice_id, price, COUNT(price) count FROM card GROUP BY invoice_id, price) c ON tcc_invoice.id = c.invoice_id WHERE `status`=1 " + 
	    
	      sb.toString() + 
	      "ORDER BY invoice_no, price";
	    return super.queryForList(this.dsMnMainQry, sql, param.toArray(), InvoiceCardCount.class);
	  }
}
