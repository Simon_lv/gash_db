package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.SmsRecord;

/**
 * 簡訊紀錄
 * @version 1.0
 * */
@Repository
public class SMSRecordDao extends BaseDao {
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public SMSRecordDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}

	public SmsRecord findByMessageId(String messageId) {
		SmsRecord smsRecord = (SmsRecord) queryForObject(dsMnMainQry,"SELECT m.* FROM sms_record as m where message_id = ? limit 1",new Object[]{messageId}, SmsRecord.class);
		return smsRecord;
	}
	
	public SmsRecord add(final SmsRecord smsRecord) {
		final String sql = 
				"INSERT INTO sms_record("
				+"message_id"
				+",used_credit"
				+",member_id"
				+",credit"
				+",mobile_no"
				+",status"
				+",create_time"
				+") values "+
				"(?,?,?,?,?,?,?)";
		
		return (SmsRecord) addForObject(dsMnMainUpd, sql, smsRecord, new Object[] { 
				smsRecord.getMessageId(),
				smsRecord.getUsedCredit(),
				smsRecord.getMemberId(),
				smsRecord.getCredit(),
				smsRecord.getMobileNo(),
				smsRecord.getStatus(),
				smsRecord.getCreateTime()
		});
	}
	
	public int updateReport(String messageId,String reportStatus,String sourceProdId,String sourceMessageId, Timestamp reportTime) {
		final String sql = "update sms_record set report_status=?,source_prod_id=?,source_message_id=?,report_time=? where message_id=?";
		return updateForObject(dsMnMainUpd, sql, new Object[] { reportStatus, sourceProdId, sourceMessageId,reportTime, messageId });
	}
	
	public Page<SmsRecord> queryForPage(Page<SmsRecord> page, Map<String,String> params) {
		StringBuilder sb = new StringBuilder("select * from sms_record where 1=1 ");
		sb = makeAllSql(sb, params);
		String countSql = "select count(*) " + 
				sb.toString().substring(sb.indexOf("from"));
		
		sb.append(" order by create_time desc ");
		if(!params.containsKey("export")){
		    sb.append(" limit ").append(page.getFirst()).append(",").append(page.getPageSize());
		}
		
		page = queryForPage(dsMnMainQry, page, sb.toString(), countSql, new Object[]{}, new SmsRecordMapper());
		return page;
	}
	private StringBuilder makeAllSql(StringBuilder sb , Map<String,String> params){
	    if(StringUtils.isNotBlank(params.get("startTime"))) {
            sb.append(" and create_time>='").append(params.get("startTime")).append("'");
        }
        if(StringUtils.isNotBlank(params.get("endTime"))) {
            sb.append(" and create_time<='").append(params.get("endTime")).append("'");
        }
        if(StringUtils.isNotBlank(params.get("mobileNo"))) {
            sb.append(" and mobile_no='").append(params.get("mobileNo")).append("'");
        }
        if(StringUtils.isNotBlank(params.get("memberId"))) {

            if(StringUtils.equals(params.get("memberId"), "1")){
                sb.append(" and instr(member_id,'U')=1 ");
            }else if(StringUtils.equals(params.get("memberId"), "2")){
                sb.append(" and instr(member_id,'D')=1 ");
            }else if(StringUtils.equals(params.get("memberId"), "3")){
                sb.append(" and not (instr(member_id,'U')=1 OR instr(member_id,'D')=1) ");
            }       
        }
        return sb;
	}
	public List<SmsRecord> queryNoPage(Map<String,String> params) {
        StringBuilder sb = new StringBuilder("select * from sms_record where 1=1 ");
        sb = makeAllSql(sb, params);
        sb.append(" order by create_time desc");
        
        return queryForList(dsMnMainQry, sb.toString(), new Object[]{}, new SmsRecordMapper());
    }
	
	private class SmsRecordMapper implements RowMapper<SmsRecord> {

		@Override
		public SmsRecord mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			SmsRecord sm = new SmsRecord();
			sm.setId(rs.getLong("id"));
			sm.setMessageId(rs.getString("message_id"));
			sm.setUsedCredit(rs.getString("used_credit"));
			sm.setMemberId(rs.getString("member_id"));
			sm.setCredit(rs.getString("credit"));
			sm.setMobileNo(rs.getString("mobile_no"));
			sm.setStatus(rs.getString("status"));
			sm.setCreateTime(rs.getTimestamp("create_time"));
			sm.setReportStatus(rs.getString("report_status"));
			sm.setSourceProdId(rs.getString("source_prod_id"));
			sm.setSourceMessageId(rs.getString("source_message_id"));
			sm.setReportTime(rs.getTimestamp("report_time"));
			
			return sm;
		}
		
	}
	

}
