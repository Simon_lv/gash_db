package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.Authorities;
import tw.tw360.dto.PositionAuthority;
import tw.tw360.util.PrintUtil;


@Repository
public class PositionAuthorityDao extends BaseDao {

	private static final Logger LOG = LoggerFactory.getLogger(PositionAuthorityDao.class);
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;

	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;

	@Autowired
	public PositionAuthorityDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public List<PositionAuthority> queryPositionAuthorities(Long id) {
		String strSQL = "SELECT authority_id FROM `position_authority` where status = 1 and position_id = "
				+ id;
		LOG.info("queryPositionAuthorities strSQL={}",strSQL);
		List<PositionAuthority> list = new ArrayList<PositionAuthority>();
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		list = (List<PositionAuthority>) jdbcTemplate.query(strSQL,
				new Object[] {}, new PositionAuthorityMapper());

		return list;
	}

	private class PositionAuthorityMapper implements RowMapper {
		public PositionAuthority mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			PositionAuthority pa = new PositionAuthority();
			pa.setAuthorityId(rs.getInt("authority_id"));
			return pa;
		}
	}
	
	public List<Authorities> queryAdminAuthorities(Long id) {
		String strSQL = 
			"SELECT au.name " +
			"  FROM admin AS a, admin_position AS ap, position AS p, position_authority AS pa, authorities AS au " +
			" WHERE a.id = ap.admin_id " +
			"   AND p.id = ap.position_id " +
			"   AND p.id = pa.position_id " +
			"   AND au.id = pa.authority_id " +
			"   AND pa.status = 1 " +
			"   AND a.id = " + id;
		LOG.info("queryAdminAuthorities strSQL={}",strSQL);
		List<Authorities> list = new ArrayList<Authorities>();
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		list = (List<Authorities>) jdbcTemplate.query(strSQL, new Object[] {}, new AdminAuthoritiesMapper());

		return list;
	}

	public boolean existByAdminAndAuthorities(Long id, String authorityName) {
		boolean result = false;
		String strSQL = 
			"SELECT au.name " +
			"  FROM admin AS a, admin_position AS ap, position AS p, position_authority AS pa, authorities AS au " +
			" WHERE a.id = ap.admin_id " +
			"   AND p.id = ap.position_id " +
			"   AND p.id = pa.position_id " +
			"   AND au.id = pa.authority_id " +
			"   AND pa.status = 1 " +
			"   AND a.id = " + id + 
			"   AND au.name = '" + authorityName + "'";
		LOG.info("existByAdminAndAuthorities strSQL={}",strSQL);
		List<Authorities> list = new ArrayList<Authorities>();
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		System.out.println("--existByAdminAndAuthorities---" + strSQL);
		list = (List<Authorities>) jdbcTemplate.query(strSQL, new Object[] {}, new AdminAuthoritiesMapper());

		if (0 == list.size()) {
			result = false;
		} else {
			result = true;
		}
		
		return result;
	}
	
	private class AdminAuthoritiesMapper implements RowMapper {
		public Authorities mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			Authorities authorities = new Authorities();
			authorities.setName(rs.getString("name"));
			return authorities;
		}

	}

	public void updatePositionAuthInfo(Long id, String[] authorityIds) {
		inactivationPositionAuthority(id);
		for (int i = 0; i < authorityIds.length; i++) {
			String ids = authorityIds[i];
			if (checkExistPosition(id, ids)) {
				activationPositionAuthority(id, ids);// 如果该权限之前使用过，激活该权限
			} else {
				addAuthorityToPosition(id, ids);// 该权限在该角色中不存在，给该角色添加新的权限
			}

		}
	}

	/**
	 * 检查该权限是否存在于该角色关联表
	 * */
	private boolean checkExistPosition(Long id, String authorityId) {
		String strSQL = "select count(*) from position_authority where position_id = "
				+ id + " and authority_id=" + authorityId;
		LOG.info("checkExistPosition strSQL={}",strSQL);
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainUpd);
		int row = jdbcTemplate.queryForInt(strSQL);
		if (row > 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 给该角色添加权限
	 * 
	 * @author Donald time:2013-11-4 11:18:04
	 * */
	private void addAuthorityToPosition(Long id, String authorityId) {
		String strSQL = "INSERT INTO position_authority(position_id,authority_id,STATUS) VALUES ("
				+ id + "," + authorityId + ",1)";
		LOG.info("addAuthorityToPosition strSQL={}",strSQL);
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainUpd);
		if (jdbcTemplate.update(strSQL) > 0) {
			LOG.info("给当前角色添加权限成功!");
		} else {
			LOG.info("给当前角色添加权限失败!");
		}
	}

	private void activationPositionAuthority(Long id, String authorityId) {
		String strSQL = "update position_authority set status = 1 where position_id= "
				+ id + " and authority_id=" + authorityId + "";
		
		LOG.info("activationPositionAuthority strSQL={}",strSQL);
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainUpd);

		if (jdbcTemplate.update(strSQL) > 0) {
			LOG.info("修改当前角色权限成功!");
		} else {
			LOG.info("修改当前角色权限失败!");
		}
	}

	/**
	 * 将当前角色的所有权限修改为为激活状态
	 * */
	private void inactivationPositionAuthority(Long id) {
		String strSQL = "update position_authority set status = 2 where position_id= "
				+ id + "";
		LOG.info("inactivationPositionAuthority strSQL={}",strSQL);
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainUpd);

		if (jdbcTemplate.update(strSQL) > 0) {
			LOG.info("将当前角色所有权限设置为未激活状态成功!");
		} else {
			LOG.info("将当前角色所有权限设置为未激活状态失败!");
		}
	}

	/**
	 * 查询用户角色
	 * 
	 * @author Donald time:2013-11-4 12:01:09
	 * @param id
	 *            用户id
	 * */
	public int queryAdminPosition(Long id) {
		String strSQL = "SELECT position_id FROM admin_position WHERE admin_id ="
				+ id + " AND STATUS=1";
		LOG.info("queryAdminPosition strSQL={}",strSQL);
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);

		return jdbcTemplate.queryForInt(strSQL);
	}

}
