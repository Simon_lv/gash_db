package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.Card;
import tw.tw360.dto.InvoiceCard;
import tw.tw360.dto.PayedCardQuery;

/**
 * 會員資料
 * 
 * @version 1.0
 * */
@Repository
public class CardDao extends BaseDao {

	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;

	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;

	@Autowired
	public CardDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public int countByTypeAndPrice(int type, int price) {
		final String sql = "select count(*) from card where status=1 and sold_status=1 and type=? and price=?";
		int count = queryForInt(dsMnMainQry, sql, new Object[] { type, price });

		return count;
	}

	public int sumByIds(String ids) {
		final String sql = "select sum(price) from card where status=1 and id in ("
				+ ids + ")";
		int count = queryForInt(dsMnMainQry, sql, new Object[] {});

		return count;
	}

	public List<Map<String, Integer>> queryRemiansGroupByPrice(int soldStatus,
			int carType) {
		String sql = "select price,sum(if(sold_status<>2,1,0)) as notSoldCount,SUM(IF(sold_status =2,1,0)) soldCount,SUM(IF(sold_status = 2 AND invoice_id IS NULL, 1, 0)) carSoldCount, SUM(IF(sold_status = 2 AND invoice_id IS NOT NULL, 1, 0)) otherSoldCount from card where status=? and type=? group by price";

		return queryForList(dsMnMainQry, sql, new Object[] { soldStatus,
				carType }, new RemainNumMapper());
	}

	public List<Long> queryAvailableByPriceAndCount(int type, int price,
			int count) {
		final String sql = "select id from card where status=1 and sold_status=1 and type=? and price=? limit ?";
		return queryForLongList(dsMnMainQry, sql, new Object[] { type, price,
				count });
	}

	public List<Long> querySoldByPrice(int type, int price) {
		final String sql = "select id from card where sold_status=2 and type=? and price=?";
		return queryForLongList(dsMnMainQry, sql, new Object[] { type, price });
	}

	public int updateListStatus(String ids, int status) {
		final String sql = "update card set sold_status=? where id in (" + ids
				+ ")";
		return this.updateForObject(dsMnMainUpd, sql, new Object[] { status });
	}

	public List<PayedCardQuery> findAll(Map<String, String> map) {
		String sql = "select c.price,count(*) as count, "
				+ "sum(if(p.restore_status=1,c.price,0)) as notRestoreSum,"
				+ "sum(if(p.restore_status=2,c.price,0)) as restoreSum,p.order_status "
				+ "from payment p left join card c on p.card_id=c.id "
				+ "where p.status=1 ";
		if (StringUtils.isNotBlank(map.get("startTime")))
			sql += " and c.sold_time>='" + map.get("startTime") + "'";
		if (StringUtils.isNotBlank(map.get("endTime")))
			sql += " and c.sold_time<='" + map.get("endTime") + "'";
		else
			sql += " and c.sold_time<=NOW()";
		sql += " and p.order_status>='4'  group by c.price,p.order_status";

		return queryForList(dsMnMainQry, sql, null, new CardMapper());
	}

	private class RemainNumMapper implements RowMapper<Map<String, Integer>> {

		@Override
		public Map<String, Integer> mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			Map<String, Integer> map = new HashMap<String, Integer>();
			map.put("price", rs.getInt("price"));
			map.put("notSoldCount", rs.getInt("notSoldCount"));
			map.put("soldCount", rs.getInt("soldCount"));
			map.put("carSoldCount", rs.getInt("carSoldCount"));
			map.put("otherSoldCount", rs.getInt("otherSoldCount"));
			return map;
		}
	}

	private class CardMapper implements RowMapper<PayedCardQuery> {

		@Override
		public PayedCardQuery mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			PayedCardQuery q = new PayedCardQuery();
			q.setCount(rs.getInt("count"));
			q.setNotRestoreSum(rs.getLong("notRestoreSum"));
			q.setRestoreSum(rs.getLong("restoreSum"));
			q.setPrice(rs.getInt("price"));
			q.setOrderStatus(rs.getInt("order_status"));
			return q;
		}

	}

	public Card findById(long id) {
		final String sql = "select * from card where status=1 and id = ?";
		return (Card) queryForObject(dsMnMainQry, sql, new Object[] { id },
				Card.class);
	}

	public int updateSoldStatusAndTime(long id, int status, Timestamp time) {
		final String sql = "update card set sold_status=?,sold_time=? where id = ?";
		return this.updateForObject(dsMnMainUpd, sql, new Object[] { status,
				time, id });
	}

	public List<Card> querySoldTimout(int soldStatus) {
		final String sql = "select distinct c.* from card c join "
				+ "(select card_id from payment where order_status=1 and DATE_ADD(create_time,INTERVAL 1 HOUR)<NOW()) p "
				+ "on p.card_id=c.id where c.sold_status=?";
		return queryForList(dsMnMainQry, sql, new Object[] { soldStatus },
				Card.class);
	}

	public List<InvoiceCard> findAllForSoldpoint() {
		final String sql = "SELECT price, COUNT(*) `count` FROM card WHERE sold_status=1 AND sold_time IS NULL AND `status`=1 "
				+ "AND (dealline IS NULL OR dealline > NOW()) "
				+ "GROUP BY price "
				+ "ORDER BY price, import_time DESC, id DESC";
		return queryForList(dsMnMainQry, sql, new Object[] {},
				InvoiceCard.class);
	}

	public boolean updateForTccInvoice(String used, Long tccInvoiceId) {
		final String sql = "UPDATE card SET sold_time=NOW(), sold_status=2, invoice_id=?, `status`=1 WHERE id IN ("+used+")";
		return super.updateForObject(dsMnMainUpd, sql, new Object[]{tccInvoiceId}) > 0;
	}

	public List<Card> queryByTccInvoiceId(Long tccInvoiceId) {
		final String sql = "SELECT * FROM card WHERE invoice_id=? "
				+ "ORDER BY price, import_time DESC, id DESC";
		return queryForList(dsMnMainQry, sql, new Object[] {tccInvoiceId},
				Card.class);
	}

	public List<Long> getIdsForTccInvoice(InvoiceCard ic) {
		String sql = "SELECT id FROM card WHERE price = ? AND sold_status=1 AND sold_time IS NULL AND `status`=1 "
				+ "AND (dealline IS NULL OR dealline > NOW()) LIMIT ?";
		return queryForLongList(dsMnMainQry, sql, new Object[]{ic.getPrice(), ic.getCount()});
	}

	public boolean checkForTccInvoice(InvoiceCard invoiceCard) {
		String sql = "SELECT COUNT(id) FROM card WHERE price = ? AND sold_status=1 AND sold_time IS NULL AND `status`=1 "
				+ "AND (dealline IS NULL OR dealline > NOW())";
		return queryForInt(dsMnMainQry, sql, new Object[]{invoiceCard.getPrice()}) >= invoiceCard.getCount().intValue();
	}

}
