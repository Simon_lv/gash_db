package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.Admin;
import tw.tw360.util.MD5Util;
import tw.tw360.util.PrintUtil;

@Repository
public class AdminDao extends BaseDao {

	private static final Logger LOG = LoggerFactory.getLogger(AdminDao.class);
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;

	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;

	@Autowired
	public AdminDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public Admin findByUsername(String username) {
		Admin admin = (Admin) queryForObject(dsMnMainQry,
				"SELECT * FROM admin WHERE username = ?",
				new Object[] { username }, new UserMapper());
		return admin;
	}

	private class UserMapper implements RowMapper {
		public Admin mapRow(ResultSet rs, int rowNum) throws SQLException {
			Admin ad = new Admin();
			ad.setId(rs.getLong("id"));
			ad.setName(rs.getString("name"));
			ad.setPassword(rs.getString("password"));
			ad.setStatus(rs.getInt("status"));
			ad.setUsername(rs.getString("username"));
			ad.setEmail(rs.getString("email"));
			ad.setCreate_time(rs.getString("create_time"));
			return ad;
		}

	}

	@SuppressWarnings("unchecked")
	public void saveUser(Admin admin, HashMap<String, String> map) {
		String pwd = MD5Util.MD5Encode(admin.getPassword());

		int addPosition = 0;
		String strSQL = "INSERT INTO admin (name,username,password,email,status,cs_num,create_time) VALUES (?,?,?,?,1,?,now())";

		Admin newAdmin = (Admin) this.addForObject(
				dsMnMainUpd,
				strSQL,
				admin,
				new Object[] { admin.getName(), admin.getUsername(), pwd,
						admin.getEmail(), admin.getCsNum() });
		if (newAdmin != null) {
			LOG.info("添加用戶成功：{}", newAdmin);
			// 一個用戶只能有一個角色
			String checkSQL = "SELECT id from admin_position where admin_id=?";
			Map<String, String> dataMap = this.queryForMap(dsMnMainQry,
					checkSQL, new Object[] { newAdmin.getId() });
			if (StringUtils.isNotBlank(dataMap.get("id"))) {
				String updatePositionSQL = "UPDATE admin_position SET position_id = ? WHERE admin_id = ?";
				this.updateForObject(
						dsMnMainUpd,
						updatePositionSQL,
						new Object[] { map.get("positionId"), newAdmin.getId() });
			} else {
				// 添加用户与角色关联表
				JdbcTemplate jdbcTemplate = getJdbcTemplate();
				jdbcTemplate.setDataSource(dsMnMainUpd);
				LOG.info("使用的數據源是：dsMnMainUpd");
				String addPositionSQL = "INSERT INTO admin_position (admin_id,position_id,STATUS) VALUES (?,?,1)";
				addPosition = jdbcTemplate.update(addPositionSQL,
						newAdmin.getId(), map.get("positionId"));
				if (addPosition > 0) {
					LOG.info("用戶與角色關聯成功");
				} else {
					LOG.info("用戶與角色關聯失敗");
				}
			}
		} else {
			LOG.info("添加用戶失敗");
		}
	}

	public void updateUser(HashMap<String, String> map) {
		int updateAdmin = 0;
		int updatePosition = 0;
		String pwd = map.get("password");
		String passwordSQL = "";
		if (StringUtils.isNotBlank(pwd)) {
			pwd = MD5Util.MD5Encode(pwd);
			passwordSQL = "',password='" + pwd;
		}
		String strSQL = "update admin set name='" + map.get("name")
				+ "',username='" + map.get("username") + passwordSQL
				+ "',email='" + map.get("email") + "',cs_num="
				+ map.get("csNum") + "  where id = " + map.get("id") + "";
		PrintUtil.outputContent("修改賬號SQL:" + strSQL);
//		JdbcTemplate jdbcTemplate = getJdbcTemplate();
//		jdbcTemplate.setDataSource(dsMnMainUpd);
//		updateAdmin = jdbcTemplate.update(strSQL);
		updateAdmin = this.updateForObject(dsMnMainUpd, strSQL, new Object[]{});
		// 檢查是否已經關聯角色
		String sql = "SELECT COUNT(id) FROM admin_position WHERE admin_id = ?";
		int count = this.queryForInt(dsMnMainQry, sql,
				new Object[] { map.get("id") });
		String updatePositionSQL = "";

		if (count > 0) {
			updatePositionSQL = "UPDATE admin_position SET position_id = ? WHERE admin_id = ?";
			
		} else {
			updatePositionSQL = "INSERT INTO admin_position(position_id,admin_id,status) VALUES(?,?,1)";
		}
		PrintUtil.outputContent("修改账号与角色的关系：" + updatePositionSQL);
//		updatePosition = jdbcTemplate.update(updatePositionSQL,map.get("positionId"),map.get("id"));
		updatePosition = this.updateForObject(dsMnMainUpd, updatePositionSQL, new Object[]{map.get("positionId"),map.get("id")});
		if (updateAdmin > 0 && updatePosition > 0) {
			PrintUtil.outputContent("修改账号成功!");
		} else {
			PrintUtil.outputContent("修改账号失败!");
		}

	}

	@SuppressWarnings("unchecked")
	public Page<Admin> queryUserList(Page<Admin> page,
			HashMap<String, String> queryParams) {

		StringBuffer strSQL = new StringBuffer(
				"SELECT a.id,a.username,a.email,a.name,a.cs_num,a.status,p.name pname, p.id positionId FROM admin a LEFT JOIN admin_position ap ON a.id = ap.admin_id  LEFT JOIN `position` p ON ap.position_id = p.id where 1=1");
		StringBuffer pageSQL = new StringBuffer(
				"SELECT count(*) FROM admin a LEFT JOIN admin_position ap ON a.id = ap.admin_id  LEFT JOIN `position` p ON ap.position_id = p.id where 1=1");

		int totalCount = 0;

		if (!StringUtils.isBlank(queryParams.get("cs_num"))) {
			strSQL.append(" and a.cs_num='" + queryParams.get("cs_num") + "'");
			pageSQL.append(" and a.cs_num='" + queryParams.get("cs_num") + "'");
		}

		if (StringUtils.isNotBlank(queryParams.get("login_name"))) {
			strSQL.append(" and a.username='" + queryParams.get("login_name")
					+ "'");
			pageSQL.append(" and a.username='" + queryParams.get("login_name")
					+ "'");
		}
		if (StringUtils.isNotBlank(queryParams.get("uname"))) {
			strSQL.append(" and a.name='" + queryParams.get("uname") + "'");
			pageSQL.append(" and a.name='" + queryParams.get("uname") + "'");
		}

		strSQL.append(" ORDER BY a.status asc, p.name desc, a.create_time asc limit "
				+ page.getFirst() + "," + page.getPageSize() + "");

		PrintUtil.outputContent("獲取賬號列表SQL:" + strSQL.toString());

		List<Admin> list = new ArrayList<Admin>();

		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		list = (List<Admin>) jdbcTemplate.query(strSQL.toString(),
				new Object[] {}, new UserMapperList());

		totalCount = jdbcTemplate.queryForInt(pageSQL.toString());

		page.setResult(list);
		page.setTotalCount(totalCount);

		return page;
	}

	public List<Admin> queryAll() {
		StringBuffer strSQL = new StringBuffer("SELECT * FROM admin");
		PrintUtil.outputContent("獲取賬號列表SQL:" + strSQL.toString());

		List<Admin> list = new ArrayList<Admin>();

		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		list = (List<Admin>) jdbcTemplate.query(strSQL.toString(),
				new Object[] {}, new AdminMapperList());

		return list;
	}

	/*
	 * 查找是否有相同的用户名
	 */
	public int checkSameUser(HashMap<String, String> map) {
		int result = 0;
		String sql = "select count(*) from admin  where username='"
				+ map.get("login_name") + "'";
		PrintUtil.outputItem("AdminDao====>>checkSameUser", sql);
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		result = jdbcTemplate.queryForInt(sql);
		return result;
	}

	private class UserMapperList implements RowMapper {
		public Admin mapRow(ResultSet rs, int rowNum) throws SQLException {
			Admin ad = new Admin();
			ad.setCsNum(rs.getLong("cs_num"));
			ad.setEmail(rs.getString("email"));
			ad.setId(rs.getLong("id"));
			ad.setName(rs.getString("name"));
			ad.setUsername(rs.getString("username"));
			ad.setStatus(rs.getInt("status"));
			ad.setPositionId(rs.getInt("positionId"));
			ad.setPositionName(rs.getString("pname"));
			return ad;
		}
	}

	private class AdminMapperList implements RowMapper {
		public Admin mapRow(ResultSet rs, int rowNum) throws SQLException {
			Admin ad = new Admin();
			ad.setCsNum(rs.getLong("cs_num"));
			ad.setEmail(rs.getString("email"));
			ad.setId(rs.getLong("id"));
			ad.setName(rs.getString("name"));
			ad.setUsername(rs.getString("username"));
			ad.setStatus(rs.getInt("status"));
			return ad;
		}
	}

	public int updateUserStatus(int id, int status) {
		String strSQL = "update admin set status=" + status + " where id=" + id;
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainUpd);

		int row = jdbcTemplate.update(strSQL);

		return row;
	}

	public Admin queryUserinfo(int id) {
		String strSQL = "SELECT a.id,a.username,a.email,a.name,a.password,a.cs_num,a.status,p.name pname, p.id positionId FROM admin a LEFT JOIN admin_position ap ON a.id = ap.admin_id  LEFT JOIN `position` p ON ap.position_id = p.id where a.id = "
				+ id;
		PrintUtil.outputContent("查询用户账号详情:" + strSQL);
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		Admin user = (Admin) jdbcTemplate.queryForObject(strSQL,
				new Object[] {}, new AdminObjectMapper());

		return user;
	}

	private class AdminObjectMapper implements RowMapper {
		public Admin mapRow(ResultSet rs, int rowNum) throws SQLException {
			Admin ad = new Admin();

			ad.setId(rs.getLong("id"));
			ad.setCsNum(rs.getLong("cs_num"));
			ad.setEmail(rs.getString("email"));
			ad.setName(rs.getString("name"));
			ad.setUsername(rs.getString("username"));
			ad.setPassword(rs.getString("password"));
			ad.setPositionId(rs.getInt("positionId"));
			ad.setPositionName(rs.getString("pname"));

			return ad;

		}

	}

	public Admin loginSystem(String name, String password) {

		String strSQL = "select * from admin where username='" + name
				+ "' and password ='" + password + "' and status = 1";

		PrintUtil.outputContent("登录验证SQL:" + strSQL);

		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);

		Admin admin = null;
		try {
			admin = (Admin) jdbcTemplate.queryForObject(strSQL,
					new Object[] {}, new UserMapper());
		} catch (DataAccessException e) {
			PrintUtil.outputContent("Admin对象为null");
		}

		return admin;
	}

	public int updateAdmin(HashMap<String, String> map) {
		int result = 0;
		String sql = "update admin set name = ? , email = ? where id="
				+ map.get("id");
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainUpd);
		result = jdbcTemplate.update(sql,
				new Object[] { map.get("name"), map.get("email") });
		return result;
	}

	public int updatePwd(long id, String newPwd) {
		int result = 0;
		String pwd = MD5Util.MD5Encode(newPwd);
		String sql = "update admin set password = ? where id=" + id;
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainUpd);
		result = jdbcTemplate.update(sql, new Object[] { pwd });
		return result;
	}
}
