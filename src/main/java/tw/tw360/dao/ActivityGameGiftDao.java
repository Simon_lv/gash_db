package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.ActivityGameGift;

/**
 * <pre>
 * 程式功能: 活動虛寶發送
 * 程式日期: $Date$
 * 程式版本: $Id$
 * 上版人員: $Author$
 * 程式備註:
 * </pre> 
 */
@Repository
public class ActivityGameGiftDao extends BaseDao<ActivityGameGift> {

    private static final Logger logger = LoggerFactory.getLogger(ActivityGameGiftDao.class);
    @Resource(name = "dsMnMainQry")
    private DataSource dsMnMainQry;

    @Resource(name = "dsMnMainUpd")
    private DataSource dsMnMainUpd;
    
    @Autowired
    public ActivityGameGiftDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
        setDataSource(dataSource);
    }
    
    private class ActivityGameGiftMapper implements RowMapper<ActivityGameGift>{

        @Override
        public ActivityGameGift mapRow(ResultSet rs, int rowNum) throws SQLException {
            ActivityGameGift agg = new ActivityGameGift();
            agg.setCreateTime(rs.getTimestamp("create_time"));
            agg.setGameCode(rs.getString("game_code"));
            agg.setGiftSn(rs.getString("gift_sn"));
            agg.setId(rs.getLong("id"));
            agg.setPaymentId(rs.getLong("payment_id"));
            agg.setPw(rs.getString("pw"));
            agg.setReceiveTime(rs.getTimestamp("receive_time"));
            agg.setStatus(rs.getInt("status"));
            agg.setUserMobile(rs.getString("user_mobile"));
            agg.setUserName(rs.getString("user_name"));
            agg.setGameName(rs.getString("game_name"));
            agg.setSmsStatus(rs.getInt("sms_status")==0 ? "成功":"未發成功");
            agg.setOrderId(rs.getString("order_id"));
            agg.setPrice(rs.getInt("price"));
            return agg;
        }
        
    }
    
    private class CalcExchangeMapper implements RowMapper<Map<String, String>> {

        @Override
        public Map<String, String> mapRow(ResultSet rs, int rowNum) throws SQLException {
            int gross = rs.getInt("gross");
            int exchanged = rs.getInt("exchanged");
            int remain = gross-exchanged;
            int price = rs.getInt("price");
            Map<String, String> map = new HashMap<String, String>();
            map.put("gameName", rs.getString("game_name"));            
            map.put("gross", String.valueOf(gross));
            map.put("exchanged", String.valueOf(exchanged));
            map.put("remain", String.valueOf(remain));
            map.put("price", String.valueOf(price));
            return map;
        }
    }
        
    /**
     * 取列表
     * @param page
     * @param params
     * @return result list of activity_game_gift join with payment
     * 2015.6.8 add return column payment.order_id
     */
    public Page<ActivityGameGift> queryForPage(Page<ActivityGameGift> page, Map<String,String> params) {
        StringBuilder sb = new StringBuilder(" SELECT agg.*,")
        .append(" CASE agg.receive_time is NULL  WHEN true THEN 1 ELSE 0 END AS sms_status,")
        .append(" pay.order_id")
        .append(" , (select `name` from game_gift gg where gg.id=agg.game_code) game_name")
        .append(" FROM activity_game_gift AS agg")
        .append(" JOIN payment AS pay")
        .append(" ON agg.payment_id = pay.id")
        .append(" WHERE 1=1")
        .append(" AND agg.status = 2 ");
        sb = makeAllSql(sb, params);
        String countSql = "select count(1) " + 
                sb.toString().substring(sb.indexOf("FROM"));
        
        sb.append(" order by agg.receive_time desc ");
        
        if(!params.containsKey("export")){
            sb.append(" limit ").append(page.getFirst()).append(",").append(page.getPageSize());
        }
        
        page = (Page<ActivityGameGift>) queryForPage(dsMnMainQry, page, sb.toString(), countSql, new Object[]{}, new ActivityGameGiftMapper());
        return page;
    }
    
    private StringBuilder makeAllSql(StringBuilder sb , Map<String,String> params){
        if(StringUtils.isNotBlank(params.get("startTime"))) {
            sb.append(" AND agg.receive_time>='").append(params.get("startTime")).append("'");
        }
        if(StringUtils.isNotBlank(params.get("endTime"))) {
            sb.append(" AND agg.receive_time<='").append(params.get("endTime")).append("'");
        }
        if(StringUtils.isNotBlank(params.get("orderId"))) {
            sb.append(" AND pay.order_id='").append(StringUtils.trim(params.get("orderId"))).append("'");
        }
        if(StringUtils.isNotBlank(params.get("gameCode"))) {
            sb.append(" AND agg.game_code='").append(StringUtils.trim(params.get("gameCode"))).append("'");
        }
        
        return sb;
    }
    
    /**
     * 兌換數量表
     * @return
     */
    public List calcGamecode(){
        String sql = "SELECT agg.game_code, gg.`name` game_name, agg.price, COUNT(agg.game_code) AS gross, " +
                     "SUM(IF(agg.status = 2,1,0)) AS exchanged FROM activity_game_gift agg, game_gift gg where agg.game_code=gg.id " + 
                     "GROUP BY game_code, agg.price ORDER BY game_code ";

        return queryForList(dsMnMainQry, sql, new Object[]{}, new CalcExchangeMapper());
    }
}