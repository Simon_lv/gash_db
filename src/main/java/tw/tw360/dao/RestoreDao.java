package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.Admin;
import tw.tw360.dto.Driver;
import tw.tw360.dto.Restore;
import tw.tw360.query.PaymentQuery;
import tw.tw360.util.PrintUtil;

/**
 * @author jenco
 * time：2015-2-4 上午11:04:36
 */
@Repository
public class RestoreDao extends BaseDao {
	
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;

	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;

	@Resource(name = "dsMnGameQry")
	private DataSource dsMnGameQry;

	@Resource(name = "dsMnGameUpd")
	private DataSource dsMnGameUpd;
	
	@Autowired
	public RestoreDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	public Page<Restore> findAll(Page<Restore> page, HashMap<String, String> map) {
		boolean isCanQuery = false;
		StringBuilder sb = new StringBuilder("select r.id,r.driver_id,d.car_id,r.money,r.method,r.restore_time,r.admin_id,adm.name adminName,r.status from restore r join driver d on r.driver_id=d.id  left join admin adm on adm.id=admin_id where r.status=1 ");
		sb = makeFindAllSql(sb,map);  
//		if(StringUtils.isNotBlank(map.get("method"))) {
//			if(!map.get("method").equals("0"))//0表示method全选
//			    sb.append(" and r.method='").append(map.get("method")).append("'");
//			isCanQuery = true;
//		}
	//	if(!isCanQuery)
	//		return page;
		
		String countSql = "select count(*) " + sb.toString().substring(sb.toString().indexOf("from"));
		sb.append(" order by restore_time desc limit "+page.getFirst()+","+page.getPageSize()+" ");
		PrintUtil.outputContent("Restore列表SQL：" + sb.toString());
		page = queryForPage(dsMnGameQry, page, sb.toString(), countSql, new Object[]{}, new RestoreMapper());
		return page;
	}
	   public List<Restore> findAll(HashMap<String, String> map) {
	        StringBuilder sb = new StringBuilder("select r.id,r.driver_id,d.car_id,r.money,r.method,r.restore_time,r.admin_id,adm.name adminName,r.status from restore r join driver d on r.driver_id=d.id  left join admin adm on adm.id=admin_id where r.status=1 ");
	        sb = makeFindAllSql(sb,map);             
	        sb.append(" order by restore_time desc ");
	        PrintUtil.outputContent("Restore列表SQL：" + sb.toString());
	        return queryForList(dsMnGameQry, sb.toString(), new Object[]{}, new RestoreMapper());

	    }
	   private StringBuilder makeFindAllSql(StringBuilder sb ,HashMap<String,String> map){
	       if(StringUtils.isNotBlank(map.get("carId"))) {
               sb.append(" and d.car_id='").append(map.get("carId")).append("'");
           }
           if(StringUtils.isNotBlank(map.get("startTime"))) {
               sb.append(" and r.restore_time>='").append(map.get("startTime")).append("'");
           }
           if(StringUtils.isNotBlank(map.get("endTime"))) {
               sb.append(" and r.restore_time<='").append(map.get("endTime")).append("'");
           }
           return sb;
	   }
	
	public void batchAddRestore(List<PaymentQuery> list, Admin admin) {
		
		for(PaymentQuery p : list) {
			String sql = "INSERT INTO restore (driver_id,money,method,admin_id,restore_time,status) VALUES(?,?,?,?,NOW(),?)";
			Restore rst = new Restore();
			rst = (Restore) addForObject(dsMnMainUpd, sql, rst, new Object[]{
				p.getDriverId(),p.getPrice(),1,admin.getId(),1});
			
			long restoreId = rst.getId();
			sql = "UPDATE payment SET restore_id=?,restore_status=2 WHERE id=?";
			updateForObject(dsMnMainUpd, sql, new Object[]{restoreId, p.getId()});
		}
		
	}
	
	private class RestoreMapper implements RowMapper<Restore> {

		@Override
		public Restore mapRow(ResultSet rs, int rowNum) throws SQLException {
			Restore st = new Restore();
			st.setId(rs.getLong("id"));
			st.setDriverId(rs.getInt("driver_id"));
			st.setMoney(rs.getInt("money"));
			st.setStatus(rs.getInt("status"));
			st.setRestoreTime(rs.getTimestamp("restore_time"));
			st.setMethod(rs.getInt("method"));
			st.setAdminId(rs.getInt("admin_id"));
			st.setCarId(rs.getString("car_id"));
			st.setAdminName(rs.getString("adminName"));
			return st;
		}
		
	}

	public Restore save(String sql, Object[] parmas, Restore r) {
		return (Restore) addForObject(dsMnGameUpd, sql, r, parmas);
	}
	
    
}
