package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.GiftCardPointCount;

@Repository
public class GiftCardDao extends BaseDao{
	
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public GiftCardDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	public List<GiftCardPointCount> queryTypeOfPoint(int type) {
		final String sql = "select price, count(price) availableCount from gift_card where status=1 and admin_id IS NULL and type=? and (dealline > now() or dealline is null) group by price order by price desc";
		return queryForList(dsMnMainQry, sql, new Object[]{type}, new CardPointCountMapper());
//		JdbcTemplate jdbcTemplate = getJdbcTemplate();
//		jdbcTemplate.setDataSource(dsMnMainQry);
//		return jdbcTemplate.query(sql, new Object[]{type}, new CardPointCountMapper());
	}
	
	public String updateAdminIdPayment(List<GiftCardPointCount> availableGiftPoints, String selectedPaymentId, int type, String adminId) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainUpd);
		String sql = "UPDATE `gift_card` SET admin_id = ?, payment_id = ? WHERE admin_id IS NULL AND price = ? AND type = ? AND ( dealline IS NULL OR dealline > now() ) LIMIT ?";
		List<Object[]> batchArgs = new ArrayList<Object[]>();
		for(GiftCardPointCount gcp : availableGiftPoints) {
			Object[] p = new Object[]{adminId, selectedPaymentId, gcp.getPrice(), type, gcp.getIntendedUseCount()};
			batchArgs.add(p);
		}
		int[] batchUpdate = jdbcTemplate.batchUpdate(sql, batchArgs);
		
		for(int i = 0; i< availableGiftPoints.size(); i++) {
			if(availableGiftPoints.get(i).getIntendedUseCount() != batchUpdate[i]) {
				return availableGiftPoints.get(i).getPrice()+"的數量只有"+batchUpdate[i]+"請重新操作";
			}
		}
		
		return null;
	}

	public List<Map<String, String>> queryPwd(String selectedPaymentId, int type, String adminId) {
		final String sql = "SELECT pw, price FROM `gift_card` WHERE admin_id = ? AND payment_id = ? AND sold_time IS NULL AND type=?";
		
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		return jdbcTemplate.query(sql, new Object[]{adminId+"", Integer.valueOf(selectedPaymentId), Integer.valueOf(type)}, new CardPointPwMapper());
	}
	
	public void updateSoldTime(String selectedPaymentId, int type, String adminId) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainUpd);
		String sql = "UPDATE `gift_card` SET sold_time = now() WHERE admin_id = ? AND payment_id = ? AND type = ?";
		
		jdbcTemplate.update(sql, new Object[]{adminId, selectedPaymentId, type});
	}

	public void updateAdminIdPaymentToNull(String selectedPaymentId, int type, String adminId) {
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainUpd);
		String sql = "UPDATE `gift_card` SET admin_id = NULL, payment_id = NULL WHERE sold_time IS NULL AND admin_id = ? AND payment_id = ? AND type = ?";
		
		jdbcTemplate.update(sql, new Object[]{adminId, selectedPaymentId, type});
	}
	
	public boolean existPaymentId(String paymentId) {
		final String sql = "SELECT COUNT(*) FROM `gift_card` WHERE payment_id = ? ";
		
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		return jdbcTemplate.queryForObject(sql, new Object[]{paymentId}, Integer.class) != 0;
	}
	
	private class CardPointCountMapper implements RowMapper<GiftCardPointCount>{

		@Override
		public GiftCardPointCount mapRow(ResultSet rs, int arg1) throws SQLException {
			GiftCardPointCount cardPointCount = new GiftCardPointCount();
	        
	        cardPointCount.setPrice(rs.getInt("price"));
	        cardPointCount.setAvailableCount(rs.getInt("availableCount"));
			return cardPointCount;
		}
		
	}
	
	public List<String> getCantGiftPointsPaymentId(int type) {
		final String sql = "select payment_id from gift_card WHERE sold_time IS NOT NULL";
		return queryForObjectList(dsMnMainQry, sql, new Object[]{}, String.class);
	}
	
	private class CardPointPwMapper implements RowMapper<Map<String,String>>{

		@Override
		public Map<String,String> mapRow(ResultSet rs, int arg1) throws SQLException {
			Map<String, String> result = new HashMap<String, String>();
	        
			result.put("price", rs.getString("price"));
			result.put("pw", rs.getString("pw"));
			return result;
		}
		
	}

}
