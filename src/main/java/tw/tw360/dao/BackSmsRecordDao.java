package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.BackSmsRecord;

/**
 * 回覆簡訊紀錄
 * @version 1.0
 * */
@Repository
public class BackSmsRecordDao extends BaseDao {
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public BackSmsRecordDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}

	public BackSmsRecord findByMessageId(String messageId) {
		BackSmsRecord smsRecord = (BackSmsRecord) queryForObject(dsMnMainQry,"SELECT * FROM backsms_record where message_id = ? limit 1",new Object[]{messageId}, BackSmsRecord.class);
		return smsRecord;
	}
	
	public BackSmsRecord add(final BackSmsRecord smsRecord) {
		final String sql = 
				"INSERT INTO backsms_record("
				+"message_id"
				+",member_id"
				+",mobile_no"
				+",charset"
				+",sms_message"
				+",create_time"
				+",pay_ids"
				+",fail_pay_ids"
				+") values "+
				"(?,?,?,?,?,?,?,?)";
		
		return (BackSmsRecord) addForObject(dsMnMainUpd, sql, smsRecord, new Object[] { 
				smsRecord.getMessageId(),
				smsRecord.getMemberId(),
				smsRecord.getMobileNo(),
				smsRecord.getCharset(),
				smsRecord.getSmsMessage(),
				smsRecord.getCreateTime(),
				smsRecord.getPayIds(),
				smsRecord.getFailPayIds()
		});
	}

	public Page<HashMap<String, String>> findByViewCondition(Page<HashMap<String, String>> page, String mobileNo, String startTime, String endTime, boolean noPage) {
		ArrayList<String> params = new ArrayList<String>();
		StringBuilder condition = new StringBuilder(); // mobile_no='0918452002' AND create_time >= '2015-02-16 16:17:27' AND create_time <='2015-02-16 16:17:27'
		
		if(StringUtils.isNotBlank(mobileNo)) {
			condition.append("AND mobile_no=? ");
			params.add(mobileNo);
		}
		
		if(StringUtils.isNotBlank(startTime)) {
			condition.append("AND create_time >= ? ");
			params.add(startTime);
		}
		
		if(StringUtils.isNotBlank(endTime)) {
			condition.append("AND create_time <= ? ");
			params.add(endTime);
		}
		
		String sql = "SELECT a.message_id, a.mobile_no, a.sms_message, a.create_time, GROUP_CONCAT(distinct payment.order_id SEPARATOR  ', ' ) order_ids FROM "
				+ "((SELECT message_id, mobile_no, pay_ids, sms_message, create_time, id FROM backsms_record WHERE 1=1 "+condition.toString()+" ) a "
				+ "LEFT JOIN payment ON FIND_IN_SET(payment.id,a.pay_ids)) GROUP BY a.id ORDER BY a.create_time desc";
		if(!noPage) {
			sql += " limit "+page.getFirst()+","+page.getPageSize();
		}
		
		String countSql = "SELECT COUNT(*) FROM backsms_record WHERE 1 = 1 "+condition.toString();
		
		page = queryForPage(dsMnMainQry, page, sql.toString(), countSql, params.toArray(), rowMapperBackSmsRecord);
		return page;
	}
	
	RowMapper rowMapperBackSmsRecord = new RowMapper<HashMap<String, String>>() {

		@Override
		public HashMap<String, String> mapRow(ResultSet rs, int index) throws SQLException {
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("messageId", rs.getString("message_id"));
			params.put("mobileNo", rs.getString("mobile_no"));
			params.put("smsMessage", rs.getString("sms_message"));
			params.put("orderIds", rs.getString("order_ids"));
			params.put("createTime", rs.getString("create_time"));
			return params;
		}
	};

}
