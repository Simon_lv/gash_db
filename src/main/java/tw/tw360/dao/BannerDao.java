package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.Banner;
import tw.tw360.util.PrintUtil;

@Repository
public class BannerDao extends BaseDao<Banner> {

	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;

	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;

	@Resource(name = "dsMnGameQry")
	private DataSource dsMnGameQry;

	@Resource(name = "dsMnGameUpd")
	private DataSource dsMnGameUpd;

	@Autowired
	public BannerDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}
	
	private class BannerMapper implements RowMapper {
		public Banner mapRow(ResultSet rs, int rowNum) throws SQLException {
			Banner b = new Banner();
			b.setId(rs.getLong("id"));
			b.setBannerUrl(rs.getString("banner_url"));
			b.setOrderIndex(rs.getInt("order_index"));
			b.setStatus(rs.getInt("status"));
			return b;
		}
	}
	
	
	

	
	public Page<Banner> findAll(Page<Banner> page, HashMap<String, String> map) {
		StringBuffer strSQL = new StringBuffer(
				"select * from banner where status=1 ");
		StringBuffer pageSQL = new StringBuffer(
				"select count(*) from banner where status=1 ");

		strSQL.append(" order by order_index limit " + page.getFirst() + "," + page.getPageSize() + "");
		PrintUtil.outputContent("Banner列表SQL：" + strSQL.toString());
		page = (Page<Banner>) queryForPage(dsMnGameQry, page,
				strSQL.toString(), pageSQL.toString(), new Object[] {},
				new BannerMapper());
	
		return page;
	}
	
	public Banner loadById(long id){
		String sql = "select * from banner where id=? and status=1 ";
		return (Banner)queryForObject(dsMnGameQry, sql, new Object[]{id}, new BannerMapper());
	}
	
	public Banner queryOffsetRecord(long id, int offset){
		String gqOReq = offset>0 ?">" :"<";
		String ascORdesc = offset>0 ?"asc" :"desc";
		String sql = "select * from banner where order_index " + gqOReq
				+ "(select order_index from banner where id=?) and status=1 "
				+ "order by order_index " + ascORdesc + " limit 0,1";
		return (Banner)queryForObject(dsMnGameQry, sql, new Object[]{id}, new BannerMapper());
	}
	
	

	
	public void updateToTop(long id, int orderIndex){
		String otherSql = "update banner set order_index=order_index+1 where status=1 and id<>?";
		updateForObject(dsMnGameUpd, otherSql, new Object[]{id});
		updateOrderIndex(id, orderIndex);
	}
	
	public void updateOrderIndex(long id, int orderIndx) {
		String sql = "update banner set order_index=? where id=?";
		updateForObject(dsMnGameUpd, sql, new Object[]{orderIndx, id});
	}
	
	public void updateStatus(long id, int status) {
		String sql = "update banner set status=? where id=?";
		updateForObject(dsMnGameUpd, sql, new Object[]{status, id});
	}
	
	public Banner add(Banner banner) {
		final String sql = "insert into banner(order_index,banner_url,status) "
				+ "select ifnull(max(order_index+1),1),?,? from banner";

		return (Banner) addForObject(dsMnGameUpd, sql, 
				banner, new Object[] { banner.getBannerUrl(),banner.getStatus()});
	}

	public List<Banner> queryAll() {
		final String sql = "select * from banner where status=1 order by order_index";
		return queryForList(dsMnGameQry, sql, new Object[] { }, Banner.class);
	}
}
