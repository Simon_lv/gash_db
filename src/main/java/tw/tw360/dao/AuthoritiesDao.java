package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.Authorities;
import tw.tw360.dto.PositionAuthority;


@Repository
public class AuthoritiesDao extends BaseDao{
	
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Resource(name = "dsMnGameQry")
	private DataSource dsMnGameQry;
	
	@Resource(name = "dsMnGameUpd")
	private DataSource dsMnGameUpd;
	
	@Autowired
	public AuthoritiesDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	
	 public List<Authorities> queryAllAuthorities(){
		   String strSQL = "select * from authorities where status=1";
		   List<Authorities> list = new ArrayList<Authorities>();
		   
		   JdbcTemplate jdbcTemplate = getJdbcTemplate();
		   jdbcTemplate.setDataSource(dsMnMainQry);
		   list = (List<Authorities>) jdbcTemplate.query(strSQL.toString(), new Object[]{}, new AuthoritiesMapper());
			
			return list;
	   }

	  
	   private class AuthoritiesMapper implements RowMapper{
		    public Authorities mapRow(ResultSet rs, int rowNum)   throws SQLException {
		        Authorities aut = new Authorities();
		        
		         aut.setId(rs.getLong("id"));
		         aut.setDisplayName(rs.getString("display_name"));
		         aut.setName(rs.getString("name"));
		         aut.setPid(rs.getInt("pid"));
		        return aut;
		    }
		    
		}
	   
	  /**
	   * 根据角色id，查询角色对应的权限
	   * */
	   public List<Authorities> queryPositionAuthorities(int id){
		   List<Authorities> list = new ArrayList<Authorities>();
		   List<PositionAuthority> rl = new ArrayList<PositionAuthority>();
		   String str = "SELECT authority_id FROM `position_authority` where status =1 and position_id= "+id;
		   JdbcTemplate jdbcTemplate = getJdbcTemplate();
		   jdbcTemplate.setDataSource(dsMnMainQry);
		   rl = (List<PositionAuthority>) jdbcTemplate.query(str, new Object[]{}, new queryRolesAuthoritiesMapper());
		   
		   for (int i = 0; i < rl.size(); i++) {
			   PositionAuthority p = rl.get(i);
			  int ids = p.getAuthorityId();
			  Authorities ath = new Authorities();
			  ath = queryAuthorities(ids);
			  list.add(ath);
		    }
		   
		   return list;
	   }
	   
	   private Authorities queryAuthorities(int id){
		   String str = "select * from authorities where id="+id;
		   JdbcTemplate jdbcTemplate = getJdbcTemplate();
		   jdbcTemplate.setDataSource(dsMnMainQry);
		   Authorities auth =(Authorities)jdbcTemplate.queryForObject(str,  new Object[]{}, new AuthoritiesMapper());
		   
		   return auth;
	   }
	   
	   private class queryRolesAuthoritiesMapper implements RowMapper{
		    public  PositionAuthority mapRow(ResultSet rs, int rowNum)   throws SQLException {
		    	PositionAuthority pa = new PositionAuthority();
		         pa.setAuthorityId(rs.getInt("authority_id"));
		        return pa;
		    }
		    
		}

	public List<Authorities> queryParentAuthorities() {
		   String strSQL = "select * from authorities where status=1 and (pid is null or pid=0)";
		   List<Authorities> list = new ArrayList<Authorities>();
		   
		   JdbcTemplate jdbcTemplate = getJdbcTemplate();
		   jdbcTemplate.setDataSource(dsMnMainQry);
		   list = (List<Authorities>) jdbcTemplate.query(strSQL.toString(), new Object[]{}, new AuthoritiesMapper());
			
		   return list;
	}


	public List<Authorities> queryChildAuthorities() {
		   String strSQL = "select * from authorities where status=1 and pid is not null";
		   List<Authorities> list = new ArrayList<Authorities>();
		   
		   JdbcTemplate jdbcTemplate = getJdbcTemplate();
		   jdbcTemplate.setDataSource(dsMnMainQry);
		   list = (List<Authorities>) jdbcTemplate.query(strSQL.toString(), new Object[]{}, new AuthoritiesMapper());
			
		   return list;
	}


	public void updateStatus(long id, int status) {
		String sql = "update authorities set status=? where id=?";
		updateForObject(dsMnGameUpd, sql, new Object[]{status,id});
	}
	
	public Authorities add(Authorities authorities) {
		String sql = "insert into authorities(pid,name,display_name,status) values(?,?,?,1)";
		return (Authorities) addForObject(dsMnGameUpd, sql, authorities, new Object[]{
				authorities.getPid(),authorities.getName(),authorities.getDisplayName()
		});
	}


	public Authorities queryAuthoritiesById(long id) {
		String sql = "select * from authorities where id=" + id;
		return (Authorities) queryForObject(dsMnGameQry, sql, null, new AuthoritiesMapper());
	}


	public void save(Authorities au) {
		String sql = "update authorities set pid=?,name=?,display_name=? where id=?";
		updateForObject(dsMnGameUpd, sql, new Object[]{
				au.getPid(),au.getName(),au.getDisplayName(),au.getId()
				} );
	}
	   
}
