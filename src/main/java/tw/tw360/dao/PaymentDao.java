package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.Payment;
import tw.tw360.query.PaymentQuery;

/**
 * 會員資料
 * 
 * @version 1.0
 * */
@Repository
public class PaymentDao extends BaseDao {

	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;

	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;

	@Autowired
	public PaymentDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
		setDataSource(dataSource);
	}

	public Payment add(final Payment pay) {
		final String sql = "INSERT INTO payment("
				+ "order_id,driver_id,card_id,driver_mobile,order_status,"
				+ "restore_status,status,create_time,is_test) values "
				+ "(?,?,?,?,?,?,?,?,?)";

		return (Payment) addForObject(
				dsMnMainUpd,
				sql,
				pay,
				new Object[] { pay.getOrderId(), pay.getDriverId(),
						pay.getCardId(), pay.getDriverMobile(),
						pay.getOrderStatus(), pay.getRestoreStatus(),
						pay.getStatus(), pay.getCreateTime(), pay.getIsTest() });
	}

	public int updatePreSell(final Payment pay) {
		final String sql = "update payment set user_mobile=?, order_status=? where id=?";

		return this.updateForObject(
				dsMnMainUpd,
				sql,
				new Object[] { pay.getUserMobile(), pay.getOrderStatus(),
						pay.getId() });
	}

	public int updateConfirm(final Payment pay) {
		final String sql = "update payment set order_status=?, sms_time=? where id=?";

		return this.updateForObject(
				dsMnMainUpd,
				sql,
				new Object[] { pay.getOrderStatus(), pay.getSmsTime(),
						pay.getId() });
	}

	public int updateSmsBack(final Payment pay) {
		final String sql = "update payment set order_status=?, sms_time=?, pay_time=?, status=? where id=?";

		return this.updateForObject(
				dsMnMainUpd,
				sql,
				new Object[] { pay.getOrderStatus(), pay.getSmsTime(),
						pay.getPayTime(), pay.getStatus(), pay.getId() });
	}

	public int updateRestore(final Payment pay) {
		final String sql = "update payment set restore_status=?, restore_id=? where id=?";

		return this.updateForObject(
				dsMnMainUpd,
				sql,
				new Object[] { pay.getUserMobile(), pay.getOrderStatus(),
						pay.getId() });
	}

	public List<Payment> queryByIds(String ids) {
		final String sql = "select * from payment where id in (" + ids + ")";
		return queryForList(dsMnMainQry, sql, new Object[] {}, Payment.class);
	}

	public List<Payment> queryByDriverMobile(String mobile, int status) {
		final String sql = "select * from payment where status=2 and order_status=? and driver_mobile=?";
		return queryForList(dsMnMainQry, sql, new Object[] { status, mobile },
				Payment.class);
	}

	public List<Payment> queryByOrderStatusAndSmsTime(int status) {
		final String sql = "select * from payment where order_status=? and DATE_ADD(sms_time,INTERVAL 1 HOUR)<NOW()";
		return queryForList(dsMnMainQry, sql, new Object[] { status },
				Payment.class);
	}

	public Double sumForPayment(HashMap<String, String> params) {
		StringBuffer sql = new StringBuffer(
				"select sum(c.price) from payment p left join driver d on p.driver_id=d.id left join card c on p.card_id=c.id where 1=1 ");

		if (StringUtils.isNotBlank(params.get("orderId"))) {
			sql.append(" and order_id='" + params.get("orderId") + "'");
		}
		/*
		 * if(StringUtils.isNotBlank(params.get("vatNo"))) {
		 * sql.append(" and vat_no='"+params.get("vatNo")+"'");
		 * countSql.append(" and vat_no='"+params.get("vatNo")+"'"); }
		 */
		if (StringUtils.isNotBlank(params.get("carId"))) {
			sql.append(" and d.car_id='" + params.get("carId") + "'");
		}
		if (StringUtils.isNotBlank(params.get("userMobile"))) {
			sql.append(" and user_mobile='" + params.get("userMobile") + "'");
		}
		if (StringUtils.isNotBlank(params.get("startTime"))) {
			sql.append(" and p.create_time >='" + params.get("startTime") + "'");
		}
		if (StringUtils.isNotBlank(params.get("endTime"))) {
			sql.append(" and p.create_time <='" + params.get("endTime") + "'");
		}
		if (StringUtils.isNotBlank(params.get("orderStatus"))) {
			sql.append(" and order_status='" + params.get("orderStatus") + "'");
		}
		if (StringUtils.isNotBlank(params.get("invoiceType"))) {
			sql.append(" and invoice_type='" + params.get("invoiceType") + "'");
		}
		if (StringUtils.isNotBlank(params.get("restoreStatus"))) {
			sql.append(" and restore_status='" + params.get("restoreStatus")
					+ "'");
		}
		JdbcTemplate jdbcTemplate = this.getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		return jdbcTemplate.queryForObject(sql.toString(), Double.class);
	}

	public Page<PaymentQuery> queryForPage(Page<PaymentQuery> page, HashMap<String, String> params) {
		StringBuffer sql = new StringBuffer(
				"select p.*,d.car_id,c.price from payment p left join driver d on p.driver_id=d.id left join card c on p.card_id=c.id where 1=1 ");

		if(StringUtils.isNotBlank(params.get("orderId"))) {
			sql.append(" and order_id='" + params.get("orderId") + "'");
		}
		/*
		 * if(StringUtils.isNotBlank(params.get("vatNo"))) {
		 * sql.append(" and vat_no='"+params.get("vatNo")+"'");
		 * countSql.append(" and vat_no='"+params.get("vatNo")+"'"); }
		 */
		if(StringUtils.isNotBlank(params.get("carId"))) {
			sql.append(" and d.car_id='" + params.get("carId") + "'");
		}
		
		if(StringUtils.isNotBlank(params.get("cardType"))) {
			sql.append(" and c.type=" + params.get("cardType") + "");
		}
		
		if(StringUtils.isNotBlank(params.get("userMobile"))) {
			sql.append(" and user_mobile='" + params.get("userMobile") + "'");
		}
		
		if(StringUtils.isNotBlank(params.get("startTime"))) {
			sql.append(" and p.create_time >='" + params.get("startTime") + "'");
		}
		
		if(StringUtils.isNotBlank(params.get("endTime"))) {
			sql.append(" and p.create_time <='" + params.get("endTime") + "'");
		}
		
		if(StringUtils.isNotBlank(params.get("orderStatus"))) {
			sql.append(" and order_status='" + params.get("orderStatus") + "'");
		}
		
		if(StringUtils.isNotBlank(params.get("invoiceType"))) {
			sql.append(" and invoice_type='" + params.get("invoiceType") + "'");
		}
		
		if(StringUtils.isNotBlank(params.get("restoreStatus"))) {
			sql.append(" and restore_status='" + params.get("restoreStatus") + "'");
		}
		
		if(StringUtils.isNotBlank(params.get("isTest"))) {
			sql.append(" and is_test='" + params.get("isTest") + "'");
		}
		
		if(StringUtils.isNotBlank(params.get("invoiceNo"))) {
			sql.append(" and invoice_no='" + params.get("invoiceNo") + "'");
		}
		
		if(StringUtils.isNotBlank(params.get("hadInvoiceNo"))) {
			if(params.get("hadInvoiceNo").equals("0")) {
				sql.append(" and invoice_no IS NULL");
			} else if(params.get("hadInvoiceNo").equals("1")) {
				sql.append(" and invoice_no IS NOT NULL");
			}  
		}

		String countSql = "select count(*) "
				+ sql.substring(sql.indexOf(" from"));
		if (!params.containsKey("export")) {
			sql.append(" order by id desc limit " + page.getFirst() + ","
					+ page.getPageSize() + "");
		} else {
			sql.append(" order by id desc ");
		}

		page = queryForPage(dsMnMainQry, page, sql.toString(), countSql,
				new Object[] {}, new PaymentMapper());

		return page;
	}

	private class PaymentMapper implements RowMapper {

		public PaymentQuery mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			PaymentQuery payment = new PaymentQuery();
			payment.setId(rs.getLong("id"));
			payment.setCreateTime(rs.getTimestamp("create_time"));
			payment.setVatNo(rs.getString("vat_no"));
			payment.setDriverMobile(rs.getString("driver_mobile"));
			payment.setDriverId(rs.getInt("driver_id"));
			payment.setUserMobile(rs.getString("user_mobile"));
			payment.setUserEmail(rs.getString("user_email"));
			payment.setSmsTime(rs.getTimestamp("sms_time"));
			payment.setRestoreId(rs.getLong("restore_id"));
			payment.setCardId(rs.getLong("card_id"));
			payment.setPayTime(rs.getTimestamp("pay_time"));
			payment.setOrderStatus(rs.getInt("order_status"));
			payment.setRestoreStatus(rs.getInt("restore_status"));
			payment.setInvoiceAddress(rs.getString("invoice_address"));
			payment.setInvoiceType(rs.getInt("invoice_type"));
			payment.setInvoiceNo(rs.getString("invoice_no"));
			payment.setStatus(rs.getInt("status"));
			payment.setOrderId(rs.getString("order_id"));
			payment.setIsTest(rs.getInt("is_test"));
			payment.setCarId(rs.getString("car_id"));
			payment.setPrice(rs.getInt("price"));
			payment.setInvoiceNo(rs.getString("invoice_no"));
			return payment;
		}
	}

	public PaymentQuery findById(long id) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT");
		sb.append(" p.*, d.car_id,c.sn,");
		sb.append(" c.price,rst.restore_time");
		sb.append(" FROM ");
		sb.append(" payment p ");
		sb.append(" LEFT JOIN driver d ON p.driver_id = d.id");
		sb.append(" LEFT JOIN card c ON p.card_id = c.id");
		sb.append(" LEFT JOIN restore rst ON p.restore_id=rst.id");
		sb.append(" WHERE p.id=?");
		return (PaymentQuery) this.queryForObject(dsMnMainQry, sb.toString(),
				new Object[] { id }, new PaymentQueryMapper());
	}

	public void updateInvoiceData(Payment payment) {
		String sql = "update payment set invoice_no = ?, invoice_type=?,invoice_address=?,user_email=?,order_status=? where id=?";
		this.updateForObject(dsMnMainUpd, sql,
				new Object[] { payment.getInvoiceNo(),
						payment.getInvoiceType(), payment.getInvoiceAddress(),
						payment.getUserEmail(), payment.getOrderStatus(),
						payment.getId() });
	}

	private class PaymentQueryMapper implements RowMapper {

		public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
			// ResultSetMetaData rsmt = rs.getMetaData();
			PaymentQuery payment = new PaymentQuery();
			payment.setId(rs.getLong("id"));
			payment.setCreateTime(rs.getTimestamp("create_time"));
			payment.setVatNo(rs.getString("vat_no"));
			payment.setDriverMobile(rs.getString("driver_mobile"));
			payment.setDriverId(rs.getInt("driver_id"));
			payment.setUserMobile(rs.getString("user_mobile"));
			payment.setUserEmail(rs.getString("user_email"));
			payment.setSmsTime(rs.getTimestamp("sms_time"));
			payment.setRestoreId(rs.getLong("restore_id"));
			payment.setCardId(rs.getLong("card_id"));
			payment.setPayTime(rs.getTimestamp("pay_time"));
			payment.setOrderStatus(rs.getInt("order_status"));
			payment.setRestoreStatus(rs.getInt("restore_status"));
			payment.setInvoiceAddress(rs.getString("invoice_address"));
			payment.setInvoiceType(rs.getInt("invoice_type"));
			payment.setInvoiceNo(rs.getString("invoice_no"));
			payment.setStatus(rs.getInt("status"));
			payment.setIsTest(rs.getInt("is_test"));
			payment.setOrderId(rs.getString("order_id"));

			payment.setRestoreTime(rs.getTimestamp("restore_time"));
			payment.setSn(rs.getString("sn"));
			payment.setCarId(rs.getString("car_id"));
			payment.setPrice(rs.getInt("price"));
			return payment;
		}
	}

	private class PaymentQueryMapperExp implements RowMapper {

		public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
			PaymentQuery payment = new PaymentQuery();
			payment.setId(rs.getLong("id"));
			payment.setCreateTime(rs.getTimestamp("create_time"));
			payment.setSmsTime(rs.getTimestamp("sms_time"));
			payment.setCreateTime(rs.getTimestamp("create_time"));
			payment.setOrderStatus(rs.getInt("order_status"));
			payment.setStatus(rs.getInt("status"));
			payment.setOrderId(rs.getString("order_id"));
			payment.setDriverId(rs.getInt("driver_id"));
			payment.setCarId(rs.getString("car_id"));

			payment.setPrice(rs.getInt("price"));
			return payment;
		}
	}

	public int updateOrderStatus(long id, int status) {
		final String sql = "update payment set order_status=? where id=?";
		return this.updateForObject(dsMnMainUpd, sql,
				new Object[] { status, id });
	}

	public int updateOrderStatusAndSmsTime(long id, int status, Timestamp time) {
		final String sql = "update payment set order_status=?, sms_time=? where id=?";
		return this.updateForObject(dsMnMainUpd, sql, new Object[] { status,
				time, id });
	}

	public int updateStatus(long id, int status) {
		final String sql = "update payment set status=? where id=?";
		return this.updateForObject(dsMnMainUpd, sql,
				new Object[] { status, id });
	}

	public List<PaymentQuery> queryNotRestoreByCarId(String carId) {
		StringBuilder sb = new StringBuilder();
		sb.append("	SELECT p.*,c.price,dr.car_id");
		sb.append(" FROM payment p");
		sb.append(" LEFT JOIN card c ON p.card_id = c.id");
		sb.append(" LEFT JOIN driver dr ON p.driver_id = dr.id");
		sb.append(" WHERE p.status=1 AND p.restore_status=1 AND p.order_status >= 2 AND dr.car_id =?");

		return queryForList(dsMnMainQry, sb.toString(), new Object[] { carId },
				new PaymentQueryMapperExp());
	}

	public List<PaymentQuery> queryFinishedPayment() {
		StringBuilder sb = new StringBuilder();
		sb.append("	SELECT p.*,c.price,dr.car_id");
		sb.append(" FROM payment p");
		sb.append(" LEFT JOIN card c ON p.card_id = c.id");
		sb.append(" LEFT JOIN driver dr ON p.driver_id = dr.id");
		sb.append(" WHERE p.order_status=7 ");

		return queryForList(dsMnMainQry, sb.toString(), new Object[] {},
				new PaymentQueryMapperExp());
	}

	private class PaymentMapperExp implements RowMapper {

		public Payment mapRow(ResultSet rs, int rowNum) throws SQLException {
			Payment payment = new Payment();
			payment.setOrderId(rs.getString("order_id"));
			payment.setDriverId(rs.getInt("driver_id"));
			payment.setCardId(rs.getLong("card_id"));
			payment.setUserMobile(rs.getString("user_mobile"));
			payment.setDriverMobile(rs.getString("driver_mobile"));
			payment.setCreateTime(rs.getTimestamp("create_time"));
			payment.setPayTime(rs.getTimestamp("pay_time"));
			payment.setSmsTime(rs.getTimestamp("sms_time"));
			payment.setOrderStatus(rs.getInt("order_status"));
			payment.setInvoiceType(rs.getInt("invoice_type"));
			payment.setInvoiceNo(rs.getString("invoice_no"));
			payment.setInvoiceAddress(rs.getString("invoice_address"));
			payment.setVatNo(rs.getString("vat_no"));
			payment.setUserEmail(rs.getString("user_email"));
			payment.setRestoreStatus(rs.getInt("restore_status"));
			payment.setRestoreId(rs.getLong("restore_id"));
			payment.setStatus(rs.getInt("status"));
			payment.setIsTest(rs.getInt("is_test"));

			return payment;
		}
	}

	public List<Payment> findRestoreByOrderIds(String[] orderId) {
		StringBuilder sb = new StringBuilder();

		sb.append("	SELECT p.*");
		sb.append(" FROM payment p");
		sb.append(" WHERE p.restore_status=1 and p.order_id in (");

		for (int i = 0; i < orderId.length; i++) {
			sb.append("'").append(orderId[i]).append("',");
		}

		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");

		return queryForList(dsMnMainQry, sb.toString(), new Object[] {},
				new PaymentMapperExp());
	}

	public void updateRestore(List<Payment> payList, int i, long id) {
		StringBuilder sb = new StringBuilder();
		ArrayList<String> p = new ArrayList<String>();
		sb.append("update payment set restore_status=" + i + ", restore_id="
				+ id + " where order_id in (");

		for (Payment py : payList) {
			sb.append("'").append(py.getOrderId()).append("',");
		}

		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");
		super.updateForObject(dsMnMainUpd, sb.toString(), new Object[] {});
	}

	public List<String> queryUserMobileByCardId(List<Long> cardIds) {
		if (cardIds.size() == 0) {
			return new ArrayList<String>();
		}
		String sql = "select user_mobile from payment where card_id in (";
		StringBuilder sb = new StringBuilder();
		for (long cardId : cardIds) {
			sb.append(cardId).append(",");
		}
		sb.deleteCharAt(sb.length() - 1);
		sb.append(")");

		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);

		return jdbcTemplate.queryForList(sql + sb.toString(), new Object[] {},
				String.class);
	}

	public List<String> queryHasInvoiceNo(List<String> orderId) {
		String sql = "SELECT order_id FROM payment WHERE invoice_no is not null and (";
		for (int i = 0; i < orderId.size(); i++) {
			if (i != 0) {
				sql = sql + " or order_id = '" + orderId.get(i) + "'";
			} else {
				sql = sql + " order_id = '" + orderId.get(i) + "'";
			}
		}
		sql = sql + ")";
		return this.queryForObjectList(dsMnMainQry, sql, null, String.class);

	}

	public List<String> queryHasNotInvoiceNo(List<String> orderId) {
		String sql = "SELECT order_id FROM payment WHERE (invoice_no is null or invoice_no = '') and (";
		for (int i = 0; i < orderId.size(); i++) {
			if (i != 0) {
				sql = sql + " or order_id = '" + orderId.get(i) + "'";
			} else {
				sql = sql + " order_id = '" + orderId.get(i) + "'";
			}
		}
		sql = sql + ")";
		return this.queryForObjectList(dsMnMainQry, sql, null, String.class);

	}

	public List<String> queryExistOrder(List<String> orderId) {
		String sql = "SELECT order_id FROM payment WHERE ";
		for (int i = 0; i < orderId.size(); i++) {
			if (i != 0) {
				sql = sql + " or order_id = '" + orderId.get(i) + "'";
			} else {
				sql = sql + " order_id = '" + orderId.get(i) + "'";
			}
		}
		return this.queryForObjectList(dsMnMainQry, sql, null, String.class);

	}

	public void updateOrderInvoiceNo(List<Object[]> params) {
		String sql = "update payment set invoice_no = ? where order_id = ?";
		for (int i = 0; i < params.size(); i++) {
			this.updateForObject(dsMnMainUpd, sql, params.get(i));
		}

	}
}