package tw.tw360.dao;

import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.GameGift;
import tw.tw360.util.PrintUtil;

@Repository
public class GameGiftDao extends BaseDao {
	
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public GameGiftDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}

	public List<GameGift> queryAll() {
		final String sql = "select * from game_gift order by id";
		return queryForList(dsMnMainQry, sql, new Object[]{}, GameGift.class);
	}
	
	public Page<GameGift> findAll(Page<GameGift> page, HashMap<String, String> map) {
		StringBuffer strSQL = new StringBuffer(
				"select * from game_gift where status=1 ");
		StringBuffer pageSQL = new StringBuffer(
				"select count(*) from game_gift where status=1 ");

		strSQL.append(" order by `order` limit " + page.getFirst() + "," + page.getPageSize() + "");
		PrintUtil.outputContent("GameGift列表SQL：" + strSQL.toString());
		page = (Page<GameGift>) queryForPage(dsMnMainQry, page, strSQL.toString(), pageSQL.toString(), new Object[] {},
				new BeanPropertyRowMapper(GameGift.class));
	
		return page;
	}
	
	public void updateStatus(long id, int status) {
		String sql = "update game_gift set status=? where id=?";
		updateForObject(dsMnMainUpd, sql, new Object[]{status, id});
	}
	
	public void update(GameGift gameGift) {
		String sql = "update game_gift set `name`=?, `description`=?, `deadline`=?, `img_url`=?, `price`=?, `create_time`=CURRENT_TIMESTAMP() where id=?";
		updateForObject(dsMnMainUpd, sql, new Object[]{gameGift.getName(), gameGift.getDescription(), gameGift.getDeadline(), 
				gameGift.getImgUrl(), gameGift.getPrice(), gameGift.getId()});
	}
	
	public GameGift add(GameGift gameGift) {
		final String sql = "insert into game_gift(`name`, `description`, `deadline`, `order`, `img_url`, `price`, `status`, `create_time`) "
				+ "select ?, ?, ?, ifnull(max(`order` + 1),1), ?, ?, ?, CURRENT_TIMESTAMP() from game_gift";

		return (GameGift) addForObject(dsMnMainUpd, sql, 
				gameGift, new Object[] { gameGift.getName(), gameGift.getDescription(), gameGift.getDeadline(), gameGift.getImgUrl(), 
				gameGift.getPrice(), gameGift.getStatus()});
	}
	
	public GameGift loadById(long id) {
		String sql = "select * from game_gift where id=? and status=1 ";
		return (GameGift)queryForObject(dsMnMainQry, sql, new Object[]{id}, new BeanPropertyRowMapper(GameGift.class));
	}
	
	public GameGift queryOffsetRecord(long id, int offset) {
		String gqOReq = offset > 0 ? ">" : "<";
		String ascORdesc = offset > 0 ? "asc" : "desc";
		String sql = "select * from game_gift where `order` " + gqOReq
				+ "(select `order` from game_gift where id=?) and status=1 "
				+ "order by `order` " + ascORdesc + " limit 0,1";
		return (GameGift)queryForObject(dsMnMainQry, sql, new Object[]{id}, new BeanPropertyRowMapper(GameGift.class));
	}
	
	public void updateOrder(long id, int order) {
		String sql = "update game_gift set `order`=? where id=?";
		updateForObject(dsMnMainUpd, sql, new Object[]{order, id});
	}
	
	public void updateToTop(long id, int order) {
		String otherSql = "update game_gift set `order`=order + 1 where status=1 and id<>?";
		updateForObject(dsMnMainUpd, otherSql, new Object[]{id});
		updateOrder(id, order);
	}
}