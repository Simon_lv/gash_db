package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import tw.tw360.dto.Authorities;
import tw.tw360.dto.Position;
import tw.tw360.dto.PositionAuthority;
import tw.tw360.util.PrintUtil;



/**
 * 角色Dao
 * @author Donald
 * time：2013-11-4 17:56:07
 * @version 1.5
 * */
@Repository
public class PositionDao extends BaseDao{
	
	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;
	
	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;
	
	@Autowired
	public PositionDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
	 public List<Position> queryAllPosition(){
		   String strSQL = "select id,name from `position` where status=1";
		   List<Position> list = new ArrayList<Position>();
		   
		   JdbcTemplate jdbcTemplate = getJdbcTemplate();
		   jdbcTemplate.setDataSource(dsMnMainQry);
		   list = (List<Position>) jdbcTemplate.query(strSQL, new Object[]{}, new PositionMapper());
			
			return list;
	   }
	 
	  private class PositionMapper implements RowMapper{

		    public Position mapRow(ResultSet rs, int rowNum) throws SQLException {
		    	Position p = new Position();
		        p.setName(rs.getString("name"));
		        p.setId(rs.getLong("id"));
		      return p;
		    }
		    
		  }
	  
	  
	  /**
	    *权限列表
	    * */
	   public List<Position> queryAllPositionAndauthorities(){
		   String strSQL = "select id,name, status from position";
		   
		   List<Position> list = new ArrayList<Position>();
		   
		   JdbcTemplate jdbcTemplate = getJdbcTemplate();
		   jdbcTemplate.setDataSource(dsMnMainQry);
		   list = (List<Position>) jdbcTemplate.query(strSQL.toString(), new Object[]{}, new PositionAndauthoritiesMapper());
			
			return list;
	   }
	   
	   private class PositionAndauthoritiesMapper implements RowMapper{

		    public Position mapRow(ResultSet rs, int rowNum)
		        throws SQLException {
		    	Position p = new Position();
		        Long id = (rs.getLong("id"));
		        p.setName(rs.getString("name"));
		        p.setStatus(rs.getInt("status"));
		        p.setId(rs.getLong("id"));
		        p.setAuthorities(queryPositionAuthorities(id));
		        
		      return p;
		    }
		    
		  }
	   
	   
	   /**
	    * 查询权限和角色中间表
	    * */
	   private List<Authorities> queryPositionAuthorities(Long id){
		   List<Authorities> list = new ArrayList<Authorities>();
		   List<PositionAuthority> rl = new ArrayList<PositionAuthority>();
		   String str = "SELECT authority_id FROM `position_authority` where status =1 and position_id= "+id;
		   JdbcTemplate jdbcTemplate = getJdbcTemplate();
		   jdbcTemplate.setDataSource(dsMnMainQry);
		   rl = (List<PositionAuthority>) jdbcTemplate.query(str, new Object[]{}, new queryRolesAuthoritiesMapper());
		   
		   for (int i = 0; i < rl.size(); i++) {
			   PositionAuthority p = rl.get(i);
			  int ids = p.getAuthorityId();
			  Authorities ath = new Authorities();
			  ath = queryAuthorities(ids);
			  list.add(ath);
		    }
		   
		   return list;
	   }
	   
	   private Authorities queryAuthorities(int id){
		   String str = "select * from authorities where id="+id;
		   JdbcTemplate jdbcTemplate = getJdbcTemplate();
		   jdbcTemplate.setDataSource(dsMnMainQry);
		   Authorities auth =(Authorities)jdbcTemplate.queryForObject(str,  new Object[]{}, new AuthoritiesMapper());
		   
		   return auth;
	   }
	   
	   private class AuthoritiesMapper implements RowMapper{
		    public Authorities mapRow(ResultSet rs, int rowNum)   throws SQLException {
		        Authorities aut = new Authorities();
		        
		         aut.setId(rs.getLong("id"));
		         aut.setDisplayName(rs.getString("display_name"));
		         aut.setName(rs.getString("name"));
		        
		        return aut;
		    }
		    
		}
	   
	   private class queryRolesAuthoritiesMapper implements RowMapper{
		    public  PositionAuthority mapRow(ResultSet rs, int rowNum)   throws SQLException {
		    	PositionAuthority pa = new PositionAuthority();
		         pa.setAuthorityId(rs.getInt("authority_id"));
		        return pa;
		    }
		}
	   
	   /**
	    * 新增角色
	    * */
	   public void createPosition(String name,String[] authoritiesIds) {
		   String positionSQL = "INSERT INTO `position`(NAME,STATUS) VALUES ('"+name+"',1)";
		   JdbcTemplate jdbcTemplate = getJdbcTemplate();
		   jdbcTemplate.setDataSource(dsMnMainUpd);
		   int positionNum = jdbcTemplate.update(positionSQL);
		   
		   int authorityNum = 0;//權限個數
		   int row = 0;//執行成功的權限個數
		   if(positionNum>0){ //給角色表中添加角色信息
			   if(authoritiesIds!=null && authoritiesIds.length > 0) {
				   authorityNum = authoritiesIds.length;
				   String maxIdSQL = "select max(id) from `position`";
				   int maxId = jdbcTemplate.queryForInt(maxIdSQL);
				   for (int i = 0; i < authoritiesIds.length; i++) {
					 String sql = "INSERT INTO position_authority(position_id,authority_id,STATUS) VALUES ("+maxId+","+authoritiesIds[i]+",1)";
					 if(jdbcTemplate.update(sql)>0){
						 row++;
					 }
				   }
			   }
			   
		   }
		   if(positionNum > 0) {
			   PrintUtil.outputContent("添加角色成功!");
		   }
		   PrintUtil.outputContent("準備添加的權限個數："+authorityNum);
		   PrintUtil.outputContent("添加成功的權限個數："+row);
		  
		   
		}
	   
	   /**
		 * 查询角色名字
		 * @author Donald
		 * time:2013-12-9 09:28:11
		 * @param id 角色id
		 * */
		public String queryPositionName(int id){
			String strSQL = "select name from position where id = "+id;
			JdbcTemplate jdbcTemplate = getJdbcTemplate();
			
		   jdbcTemplate.setDataSource(dsMnMainQry);
		   String name = (String) jdbcTemplate.queryForObject(strSQL, String.class);
			   
		   return name;
		}
		
	    public int updatePositionStatus(int id,int status){
			String strSQL = "update position set status="+status+" where id="+id;
			JdbcTemplate jdbcTemplate = getJdbcTemplate();
			jdbcTemplate.setDataSource(dsMnMainUpd);
			
			int row = jdbcTemplate.update(strSQL);
			
			return row;
		 }
}
