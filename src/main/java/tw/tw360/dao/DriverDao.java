package tw.tw360.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springside.modules.orm.Page;

import tw.tw360.dto.Driver;
import tw.tw360.util.PrintUtil;

@Repository
public class DriverDao extends BaseDao{

	@Resource(name = "dsMnMainQry")
	private DataSource dsMnMainQry;

	@Resource(name = "dsMnMainUpd")
	private DataSource dsMnMainUpd;

	@Resource(name = "dsMnGameQry")
	private DataSource dsMnGameQry;

	@Resource(name = "dsMnGameUpd")
	private DataSource dsMnGameUpd;
	
	@Autowired
	public DriverDao(@Qualifier("dsMnMainUpd") DataSource dataSource) {
	    setDataSource(dataSource);
	}
	
    public Page<Driver> queryUserList(Page<Driver> page,String strSQL,String pageSQL){
		List<Driver> list = new ArrayList<Driver>();
		int totalCount = 0;
		JdbcTemplate jdbcTemplate = getJdbcTemplate();
		jdbcTemplate.setDataSource(dsMnMainQry);
		list = (List<Driver>) jdbcTemplate.query(strSQL, new DriverMapper());
		PrintUtil.outputContent("List<Driver> SQL:"+strSQL);
		totalCount = jdbcTemplate.queryForInt(pageSQL);
		page.setResult(list);
		page.setTotalCount(totalCount);
		return page;
	}
    public List<Driver> queryUserList(String strSQL){
        List<Driver> list = new ArrayList<Driver>();
        JdbcTemplate jdbcTemplate = getJdbcTemplate();
        jdbcTemplate.setDataSource(dsMnMainQry);
        list = (List<Driver>) jdbcTemplate.query(strSQL, new DriverMapper());
        PrintUtil.outputContent("List<Driver> SQL:"+strSQL);
        return list;
    }

	private class DriverMapper implements RowMapper{
		public Driver mapRow(ResultSet rs, int rowNum)throws SQLException {
			Driver d = new Driver();
			d.setId(rs.getLong("id"));
			d.setCarId(rs.getString("car_id"));
			d.setMobile(rs.getString("mobile"));
			d.setPassword(rs.getString("password"));
			d.setCredit(rs.getInt("credit"));
			d.setName(rs.getString("name"));
			d.setNonRestore(rs.getInt("non_restore"));
			d.setCreateTime(rs.getTimestamp("create_time"));
			d.setCanSell(rs.getInt("can_sell"));
			d.setIsTester(rs.getInt("is_tester"));
			d.setStatus(rs.getInt("status"));
			d.setShortCode(rs.getString("short_code"));
			return d;
		}
	}

	public Driver save(String sql, Object[] parmas,Driver d) {
		return (Driver) addForObject(dsMnGameUpd, sql,d, parmas);
	}
	public void update(String sql, Object[] params, Driver d) {
		updateForObject(dsMnGameUpd, sql, params);
	}

	public Driver findBycarId(String sql, Object[] params) {
		return (Driver) queryForObject(dsMnMainQry,sql,params,new DriverMapper());
	}

	public void updateStatus(String sql, Object[] parmas) {
		updateForObject(dsMnGameUpd, sql, parmas);
	}
   
	public int addNonRestore(int id, int nonRestore) {
		final String sql = "update driver set can_sell=IF(credit>non_restore+?,1,2), non_restore=non_restore+? where id=?";
		
		return this.updateForObject(dsMnMainUpd, sql, 
				new Object[] { nonRestore, nonRestore, id });
	}
	
	public Driver findById(int id) {
		final String sql = "select * from driver where id=?";
		return (Driver) queryForObject(dsMnMainQry,sql,new Object[] { id },new DriverMapper());
	}
	
	public Driver findByMobile(String mobile) {
		final String sql = "select * from driver where mobile=? and status=1";
		return (Driver) queryForObject(dsMnMainQry,sql,new Object[] { mobile },new DriverMapper());
	}
	
	public int updatePwd(int id, String pwd) {
		final String sql = "update driver set password=? where id=?";
		return updateForObject(dsMnMainUpd,sql,new Object[] { pwd, id });
	}
}
