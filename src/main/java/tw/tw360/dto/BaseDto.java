package tw.tw360.dto;

import java.io.Serializable;

/**
 * DTO基类的标识
 * */

public class BaseDto implements Serializable { 

	private static final long serialVersionUID = 1L;

	public static enum STATUS {
		NONE, NORMAL, DELETE
	};

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
}
