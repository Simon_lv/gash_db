package tw.tw360.dto;

import java.util.List;

public class Position extends BaseDto{
 
	private String name;
    private int status;
    
    private List<Authorities> authorities;
    
	public List<Authorities> getAuthorities() {
		return authorities;
	}
	public void setAuthorities(List<Authorities> authorities) {
		this.authorities = authorities;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
