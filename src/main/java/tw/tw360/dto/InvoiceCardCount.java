package tw.tw360.dto;

public class InvoiceCardCount extends BaseDto {
	private static final long serialVersionUID = 1L;
	  private String invoiceNo;
	  private String title;
	  private Integer price;
	  private Integer count;
	  
	  public String getInvoiceNo()
	  {
	    return this.invoiceNo;
	  }
	  
	  public void setInvoiceNo(String invoiceNo)
	  {
	    this.invoiceNo = invoiceNo;
	  }
	  
	  public String getTitle()
	  {
	    return this.title;
	  }
	  
	  public void setTitle(String title)
	  {
	    this.title = title;
	  }
	  
	  public Integer getPrice()
	  {
	    return this.price;
	  }
	  
	  public void setPrice(Integer price)
	  {
	    this.price = price;
	  }
	  
	  public Integer getCount()
	  {
	    return this.count;
	  }
	  
	  public void setCount(Integer count)
	  {
	    this.count = count;
	  }
}
