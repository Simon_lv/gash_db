package tw.tw360.dto;

public class Admin extends BaseDto { 

	public static final int STATUS_VALID = 1;
	public static final int STATUS_INVALID = 2;

	private String username; // admin.username 帳號
	private String password; // admin.password 密碼
	private String name; // admin.name 姓名
	private String email; // admin.email EMAIL
	private Long csNum; // admin.cs_num 客服編號
	private String create_time; // admin.create_time 產生時間
	private int status; // admin.status 狀態(1:正常 2:停權)
	private int positionId;  // position.id ID
	private String positionName; // position.name 職位名稱 SELECT a.*, p.name as positionName from admin as a, admin_position as ap, position as p where a.id = ap.admin_id and  p.id = ap.position_id


	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getCsNum() {
		return csNum;
	}

	public void setCsNum(Long csNum) {
		this.csNum = csNum;
	}

	public String getCreate_time() {
		return create_time;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getPositionId() {
		return positionId;
	}

	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}

	public String getPositionName() {
		return positionName;
	}

	public void setPositionName(String positionName) {
		this.positionName = positionName;
	}

	@Override
	public String toString() {
		return "Admin [username=" + username + ", name=" + name + ", email="
				+ email + ", csNum=" + csNum + ", create_time=" + create_time
				+ ", status=" + status + "]";
	}

	
}
