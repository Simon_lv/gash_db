package tw.tw360.dto;

/*
* File		: SmsRecord.java
* Date Created	: Tue Feb 03 16:14:22 CST 2015
*/

public class SmsRecord extends BaseDto {

	private String usedCredit;
	private String sourceMessageId;
	private String reportStatus;
	private String mobileNo;
	private java.sql.Timestamp createTime;
	private String sourceProdId;
	private String memberId;
	private String status;
	private String credit;
	private java.sql.Timestamp reportTime;
	private String messageId;

	public String getUsedCredit() {
		return usedCredit;
	}

	public void setUsedCredit(String usedCredit) {
		this.usedCredit = usedCredit;
	}

	public String getSourceMessageId() {
		return sourceMessageId;
	}

	public void setSourceMessageId(String sourceMessageId) {
		this.sourceMessageId = sourceMessageId;
	}

	public String getReportStatus() {
		return reportStatus;
	}

	public void setReportStatus(String reportStatus) {
		this.reportStatus = reportStatus;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getSourceProdId() {
		return sourceProdId;
	}

	public void setSourceProdId(String sourceProdId) {
		this.sourceProdId = sourceProdId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCredit() {
		return credit;
	}

	public void setCredit(String credit) {
		this.credit = credit;
	}

	public java.sql.Timestamp getReportTime() {
		return reportTime;
	}

	public void setReportTime(java.sql.Timestamp reportTime) {
		this.reportTime = reportTime;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
}