package tw.tw360.dto;

public class InvoiceCard extends BaseDto {
	private static final long serialVersionUID = 1L;
	
	private Integer price;
	private Integer count;
	
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	
	
}
