package tw.tw360.dto;

/*
* File		: Banner.java
* Date Created	: Tue Feb 03 16:14:22 CST 2015
*/

public class Banner extends BaseDto {

	private int status;
	private String bannerUrl;
	private int orderIndex;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getBannerUrl() {
		return bannerUrl;
	}

	public void setBannerUrl(String bannerUrl) {
		this.bannerUrl = bannerUrl;
	}

	public int getOrderIndex() {
		return orderIndex;
	}

	public void setOrderIndex(int orderIndex) {
		this.orderIndex = orderIndex;
	}
}