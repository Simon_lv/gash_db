package tw.tw360.dto;
/**
 * @author jenco
 * time：2015-2-4 下午6:04:56
 */
public class PayedCardQuery {
    private int price;//面額
    private long notRestoreSum;//未回存金額
    private long restoreSum;//已回存金額
    private int count;//數量
    private int orderStatus;
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public long getNotRestoreSum() {
		return notRestoreSum;
	}
	public void setNotRestoreSum(long notRestoreSum) {
		this.notRestoreSum = notRestoreSum;
	}
	public long getRestoreSum() {
		return restoreSum;
	}
	public void setRestoreSum(long restoreSum) {
		this.restoreSum = restoreSum;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public int getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}
}
