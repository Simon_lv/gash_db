package tw.tw360.dto;

/*
* File		: Payment.java
* Date Created	: Tue Feb 03 16:14:22 CST 2015
*/

public class Payment extends BaseDto {
	private static final long serialVersionUID = 1L;
	private String orderId;
	private int driverId;
	private long cardId;
	private String userMobile;
	private String driverMobile;
	private java.sql.Timestamp createTime;
	private java.sql.Timestamp payTime;
	private java.sql.Timestamp smsTime;
	private int orderStatus;
	private int invoiceType;
	private String invoiceNo;
	private String invoiceAddress;
	private String vatNo;
	private String userEmail;
	private int restoreStatus;
	private long restoreId;
	private int isTest;
	private int status;

	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getVatNo() {
		return vatNo;
	}

	public void setVatNo(String vatNo) {
		this.vatNo = vatNo;
	}

	public String getDriverMobile() {
		return driverMobile;
	}

	public void setDriverMobile(String driverMobile) {
		this.driverMobile = driverMobile;
	}

	public int getDriverId() {
		return driverId;
	}

	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}

	public String getUserMobile() {
		return userMobile;
	}

	public void setUserMobile(String userMobile) {
		this.userMobile = userMobile;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public java.sql.Timestamp getSmsTime() {
		return smsTime;
	}

	public void setSmsTime(java.sql.Timestamp smsTime) {
		this.smsTime = smsTime;
	}

	public long getRestoreId() {
		return restoreId;
	}

	public void setRestoreId(long restoreId) {
		this.restoreId = restoreId;
	}

	public long getCardId() {
		return cardId;
	}

	public void setCardId(long cardId) {
		this.cardId = cardId;
	}

	public java.sql.Timestamp getPayTime() {
		return payTime;
	}

	public void setPayTime(java.sql.Timestamp payTime) {
		this.payTime = payTime;
	}

	public int getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(int orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getInvoiceAddress() {
		return invoiceAddress;
	}

	public void setInvoiceAddress(String invoiceAddress) {
		this.invoiceAddress = invoiceAddress;
	}

	public int getInvoiceType() {
		return invoiceType;
	}

	public void setInvoiceType(int invoiceType) {
		this.invoiceType = invoiceType;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int getIsTest() {
		return isTest;
	}

	public void setIsTest(int isTest) {
		this.isTest = isTest;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public int getRestoreStatus() {
		return restoreStatus;
	}

	public void setRestoreStatus(int restoreStatus) {
		this.restoreStatus = restoreStatus;
	}
	
	
}