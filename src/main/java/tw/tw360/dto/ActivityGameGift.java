package tw.tw360.dto;

import java.sql.Timestamp;

/**
 * <pre>
 * 程式功能: 遊戲虛寶資料
 * 程式日期: $Date$
 * 程式版本: $Id$
 * 上版人員: $Author$
 * 程式備註:
 * </pre> 
 */
public class ActivityGameGift extends BaseDto {

    @Override
    public String toString() {
        return "ActivityGameGift [" + (gameCode != null ? "gameCode=" + gameCode + ", " : "") + (giftSn != null ? "giftSn=" + giftSn + ", " : "") + "paymentId=" + paymentId + ", "
                + (pw != null ? "pw=" + pw + ", " : "") + (userName != null ? "userName=" + userName + ", " : "") + (userMobile != null ? "userMobile=" + userMobile + ", " : "")
                + (receiveTime != null ? "receiveTime=" + receiveTime + ", " : "") + "status=" + status + ", " + (createTime != null ? "createTime=" + createTime + ", " : "")
                + (gameName != null ? "gameName=" + gameName + ", " : "") + (smsStatus != null ? "smsStatus=" + smsStatus + ", " : "") + (orderId != null ? "orderId=" + orderId : "") + "]";
    }
    //遊戲代碼
    private String gameCode;
    //虛寶序號
    private String giftSn;
    //購買點卡紀錄ID
    private long paymentId;
    //Gash序號
    private String pw;
    //用戶姓名
    private String userName;
    //用戶手機
    private String userMobile;
    //領取日期
    private Timestamp receiveTime;
    //狀態(1:未領取, 2;已領取)
    private int status;
    //建立時間
    private Timestamp createTime;
    //遊戲名稱
    private String gameName;
    //簡訊發送狀態 (0為用戶手機接收成功，1為用戶手機接收失敗)
    private String smsStatus;
    //訂單號
    private String orderId;
    // 面額
    private int price;
    
    public String getGameCode() {
        return gameCode;
    }
    public void setGameCode(String gameCode) {
        this.gameCode = gameCode;
    }
    public String getGiftSn() {
        return giftSn;
    }
    public void setGiftSn(String giftSn) {
        this.giftSn = giftSn;
    }
    public long getPaymentId() {
        return paymentId;
    }
    public void setPaymentId(long paymentId) {
        this.paymentId = paymentId;
    }
    public String getPw() {
        return pw;
    }
    public void setPw(String pw) {
        this.pw = pw;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserMobile() {
        return userMobile;
    }
    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }
    public Timestamp getReceiveTime() {
        return receiveTime;
    }
    public void setReceiveTime(Timestamp receiveTime) {
        this.receiveTime = receiveTime;
    }
    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public Timestamp getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }
    public String getGameName() {
        return gameName;
    }
    public void setGameName(String gameName) {
        this.gameName = gameName;
    }
    public String getSmsStatus() {
        return smsStatus;
    }
    public void setSmsStatus(String smsStatus) {
        this.smsStatus = smsStatus;
    }
    public String getOrderId() {
        return orderId;
    }
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}    
}