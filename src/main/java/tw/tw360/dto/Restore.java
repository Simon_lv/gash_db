package tw.tw360.dto;

/*
* File		: Restore.java
* Date Created	: Tue Feb 03 16:14:22 CST 2015
*/

@SuppressWarnings("serial")
public class Restore extends BaseDto {

	private int driverId;
	private int money;
	private int method;
	private long adminId;
	private java.sql.Timestamp restoreTime;
	private int status;
	private String carId;
	private String adminName;
	
	public String getCarId() {
        return carId;
    }
    public void setCarId(String carId) {
        this.carId = carId;
    }
    public String getAdminName() {
        return adminName;
    }
    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }
    public int getDriverId() {
		return driverId;
	}
	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public int getMethod() {
		return method;
	}
	public void setMethod(int method) {
		this.method = method;
	}
	public long getAdminId() {
		return adminId;
	}
	public void setAdminId(long adminId) {
		this.adminId = adminId;
	}
	public java.sql.Timestamp getRestoreTime() {
		return restoreTime;
	}
	public void setRestoreTime(java.sql.Timestamp restoreTime) {
		this.restoreTime = restoreTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}