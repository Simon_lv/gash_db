package tw.tw360.dto;

/*
* File		: Card.java
* Date Created	: Tue Feb 03 16:14:22 CST 2015
*/

public class Card extends BaseDto {

	private int status;
	private java.sql.Timestamp importTime;
	private java.sql.Timestamp soldTime;
	private java.sql.Timestamp dealline;
	private String pw;
	private int type;
	private int price;
	private String sn;
	private int soldStatus;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public java.sql.Timestamp getImportTime() {
		return importTime;
	}

	public void setImportTime(java.sql.Timestamp importTime) {
		this.importTime = importTime;
	}

	public java.sql.Timestamp getSoldTime() {
		return soldTime;
	}

	public void setSoldTime(java.sql.Timestamp soldTime) {
		this.soldTime = soldTime;
	}

	public java.sql.Timestamp getDealline() {
		return dealline;
	}

	public void setDealline(java.sql.Timestamp dealline) {
		this.dealline = dealline;
	}

	public String getPw() {
		return pw;
	}

	public void setPw(String pw) {
		this.pw = pw;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

	public int getSoldStatus() {
		return soldStatus;
	}

	public void setSoldStatus(int soldStatus) {
		this.soldStatus = soldStatus;
	}
}