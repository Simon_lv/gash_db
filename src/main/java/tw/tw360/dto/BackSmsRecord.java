package tw.tw360.dto;

/*
* File		: BacksmsRecord.java
* Date Created	: Wed Feb 04 14:11:59 CST 2015
*/

public class BackSmsRecord extends BaseDto {
	private static final long serialVersionUID = 1L;
	private java.sql.Timestamp createTime;
	private String smsMessage;
	private String messageId;
	private String memberId;
	private String charset;
	private String mobileNo;
	private String payIds;
	private String failPayIds;

	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getSmsMessage() {
		return smsMessage;
	}

	public void setSmsMessage(String smsMessage) {
		this.smsMessage = smsMessage;
	}

	public String getMessageId() {
		return messageId;
	}

	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getPayIds() {
		return payIds;
	}

	public void setPayIds(String payIds) {
		this.payIds = payIds;
	}

	public String getFailPayIds() {
		return failPayIds;
	}

	public void setFailPayIds(String failPayIds) {
		this.failPayIds = failPayIds;
	}
}