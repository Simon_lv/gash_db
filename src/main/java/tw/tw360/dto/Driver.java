package tw.tw360.dto;

/*
* File		: Driver.java
* Date Created	: Tue Feb 03 16:14:22 CST 2015
*/

public class Driver extends BaseDto {
	private static final long serialVersionUID = 1L;
	private int credit;
	private int canSell;
	private java.sql.Timestamp createTime;
	private String mobile;
	private String password;
	private String carId;
	private int nonRestore;
	private int isTester;
	private int status;
	private String name;
	private String shortCode;

	public int getCredit() {
		return credit;
	}

	public void setCredit(int credit) {
		this.credit = credit;
	}

	public int getCanSell() {
		return canSell;
	}

	public void setCanSell(int canSell) {
		this.canSell = canSell;
	}

	public java.sql.Timestamp getCreateTime() {
		return createTime;
	}

	public void setCreateTime(java.sql.Timestamp createTime) {
		this.createTime = createTime;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCarId() {
		return carId;
	}

	public void setCarId(String carId) {
		this.carId = carId;
	}

	public int getNonRestore() {
		return nonRestore;
	}

	public void setNonRestore(int nonRestore) {
		this.nonRestore = nonRestore;
	}

	public int getIsTester() {
		return isTester;
	}

	public void setIsTester(int isTester) {
		this.isTester = isTester;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }
}