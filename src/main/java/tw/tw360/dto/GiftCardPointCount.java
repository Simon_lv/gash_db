package tw.tw360.dto;

public class GiftCardPointCount extends BaseDto {
	private static final long serialVersionUID = 1L;
	private int price;
	private int availableCount;
	private int intendedUseCount;
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getAvailableCount() {
		return availableCount;
	}
	public void setAvailableCount(int availableCount) {
		this.availableCount = availableCount;
	}
	public int getIntendedUseCount() {
		return intendedUseCount;
	}
	public void setIntendedUseCount(int intendedUseCount) {
		this.intendedUseCount = intendedUseCount;
	}
	
	
}
