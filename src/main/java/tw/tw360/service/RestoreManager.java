package tw.tw360.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.RestoreDao;
import tw.tw360.dto.Admin;
import tw.tw360.dto.Driver;
import tw.tw360.dto.Restore;
import tw.tw360.query.PaymentQuery;

/**
 * @author jenco
 * time：2015-2-4 上午11:04:13
 */
@Service
@Transactional
public class RestoreManager {
    
	@Autowired
	private RestoreDao restoreDao;
	
	public Page<Restore> findAll(Page<Restore> page, HashMap<String, String> map) {
		return restoreDao.findAll(page,map);
	}
	
	public void batchAddRestore(List<PaymentQuery> list,Admin admin) {
		restoreDao.batchAddRestore(list,admin);
	}
	
	public Restore add(Restore r) {
		final String sql = "insert into restore(driver_id, money, method, admin_id, restore_time, status)values(?,?,?,?,?,?);";
		Object[] parmas={r.getDriverId(),r.getMoney(),r.getMethod(),r.getAdminId(),r.getRestoreTime(),r.getStatus()};
		return restoreDao.save(sql,parmas,r);
	}
	public List<Restore> queryALLRestoreList(HashMap<String, String> params) {
        params.put("export", "export");
        return restoreDao.findAll(params);
    }
    
}
