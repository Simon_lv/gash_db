package tw.tw360.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.ActivityGameGiftDao;
import tw.tw360.dao.GameGiftDao;
import tw.tw360.dto.ActivityGameGift;
import tw.tw360.dto.GameGift;

/**
 * <pre>
 * 程式功能: 
 * 程式日期: $Date$
 * 程式版本: $Id$
 * 上版人員: $Author$
 * 程式備註:
 * </pre> 
 */
@Service
@Transactional
public class ActivityGameGiftManager {
    
    @Autowired
    private ActivityGameGiftDao activityGameGiftDao;    	
    
    /**
     * 分頁查詢記錄
     */
    @Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
    public Page<ActivityGameGift> queryForPage(Page<ActivityGameGift> page,HashMap<String, String> params) {
        return activityGameGiftDao.queryForPage(page,params);
    }

    /**
     * 匯出excel
     * @param params
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List<ActivityGameGift> queryAll(HashMap<String, String> params) {
        params.put("export", "export");
        Page<ActivityGameGift> page = new Page<ActivityGameGift>(Integer.MAX_VALUE);
        page = activityGameGiftDao.queryForPage(page, params);
        return page.getResult();
    }
    
    /**
     * 取兌換數量表
     * @return
     */
    @Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
    public List queryCalcExchange(){ 
        return activityGameGiftDao.calcGamecode();
    }        
}