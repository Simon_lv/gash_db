package tw.tw360.service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.SMSRecordDao;
import tw.tw360.dto.SmsRecord;

/**
 * 簡訊紀錄
 * @version 1.0
 * */
@Service
@Transactional
public class SMSRecordManager {
	@Autowired
    private SMSRecordDao smsRecordDao;
	
	/**
	 * 用簡訊ID找簡訊
	 * */
	public SmsRecord findByMessageId(String messageId){
		return smsRecordDao.findByMessageId(messageId);
	}
	
	/**
	 * 儲存簡訊
	 * */
	public SmsRecord add(SmsRecord smsRecord){
		return smsRecordDao.add(smsRecord);
	}
	
	/**
	 * 儲存簡訊 回報狀態
	 * */
	public int updateReport(String messageId,String reportStatus, String sourceProdId, String sourceMessageId, Timestamp reportTime){
		return smsRecordDao.updateReport(messageId, reportStatus, sourceProdId, sourceMessageId, reportTime);
	}
	/**
	 * 分頁查詢記錄
	 */
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Page<SmsRecord> queryForPage(Page<SmsRecord> page,HashMap<String, String> params) {
		return smsRecordDao.queryForPage(page,params);
	}
	/**
     * 不分頁
     */
    @Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
    public List<SmsRecord> queryNoPage(HashMap<String, String> params) {
        return smsRecordDao.queryNoPage(params);
    }
    
}
