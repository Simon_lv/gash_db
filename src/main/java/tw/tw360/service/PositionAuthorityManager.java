package tw.tw360.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.PositionAuthorityDao;
import tw.tw360.dto.Authorities;
import tw.tw360.dto.PositionAuthority;


@Service
@Transactional
public class PositionAuthorityManager {
	@Autowired
	private PositionAuthorityDao positionAuthorityDao;
	
	
	public List<PositionAuthority> queryPositionAuthorities(Long id){
		return positionAuthorityDao.queryPositionAuthorities(id);
	}
	
	/**
	 * 修改角色权限
	 * @author Donald
	 * time:2013-11-4 11:04:17
	 * @param id 角色id
	 * @param String[] 权限Id
	 * */
	public void updatePositionAuthInfo(Long id,String[] authorityIds){
		positionAuthorityDao.updatePositionAuthInfo(id, authorityIds);
	}
	
	public int queryAdminPosition(Long id){
		return positionAuthorityDao.queryAdminPosition(id);
	}
	
	public List<Authorities> queryAdminAuthorities(Long id) {
		return positionAuthorityDao.queryAdminAuthorities(id);
	}

	public boolean existByAdminAndAuthorities(Long id, String authorityName) {
		return positionAuthorityDao.existByAdminAndAuthorities(id, authorityName);
	}
}
