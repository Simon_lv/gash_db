package tw.tw360.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.AuthoritiesDao;
import tw.tw360.dto.Authorities;


@Service
@Transactional
public class AuthoritiesManager {
	@Autowired
   private AuthoritiesDao authoritiesDao;
	
	public List<Authorities> queryAllAuthorities(){
		return authoritiesDao.queryAllAuthorities();
	}
	/**
	 * 查詢父菜單
	 * @return
	 */
	public List<Authorities> queryParentAuthorities(){
		return authoritiesDao.queryParentAuthorities();
	}
	/**
	 * 查詢子菜單
	 * @return
	 */
	public List<Authorities> queryChildAuthorities(){
		return authoritiesDao.queryChildAuthorities();
	}
	
	/**
	 * 根据角色id查询角色对应的权限
	 * @author Donald
	 * time:2013-11-4 12:42:28
	 * @param int id
	 * */
	 public List<Authorities> queryPositionAuthorities(int id){
		 return authoritiesDao.queryPositionAuthorities(id);
	 }
	 
	public void updateStatus(long id, int status) {
		authoritiesDao.updateStatus(id,status);
		
	}
	public void add(Authorities authorities) {
		authoritiesDao.add(authorities);
	}
	public Authorities queryAuthoritiesById(long id) {
		return authoritiesDao.queryAuthoritiesById(id);
	}
	public void save(Authorities au) {
		 authoritiesDao.save(au);
	}
}
