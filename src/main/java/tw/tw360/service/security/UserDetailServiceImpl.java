package tw.tw360.service.security;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.GrantedAuthority;
import org.springframework.security.GrantedAuthorityImpl;
import org.springframework.security.userdetails.UserDetails;
import org.springframework.security.userdetails.UserDetailsService;
import org.springframework.security.userdetails.UsernameNotFoundException;

import tw.tw360.dto.Admin;
import tw.tw360.dto.Authorities;
import tw.tw360.service.AdminManager;
import tw.tw360.service.PositionAuthorityManager;



/**
 * 实现SpringSecurity的UserDetailsService接口,实现获取用户Detail信息的回调函数.
 * 
 */
public class UserDetailServiceImpl implements UserDetailsService {
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private AdminManager adminManager;
	
	@Autowired
	private PositionAuthorityManager positionAuthorityManager;

	/**
	 * 获取用户Detail信息的回调函数.
	 * time:2013-7-5 10:41:46
	 */
	public UserDetails loadUserByUsername(String userName) {
		logger.info("===============  loadUserByUsername   =====================");
		String j_username = userName.substring(0, userName.indexOf("/"));
		Admin user = adminManager.findByUsername(j_username);
		
		Boolean login = false;
	  
		if (user == null){
			throw new UsernameNotFoundException("用户[" + j_username + " ]不存在");
		}
		else{
			  login = true;
		}
		if (!login){
			throw new UsernameNotFoundException("用户[" + j_username + " ]不存在");
		}
		
//		GrantedAuthority[] grantedAuths = new GrantedAuthority[] {};
		GrantedAuthority[] grantedAuths = obtainGrantedAuthorities(user);
		boolean enabled = true;
		boolean accountNonExpired = true;
		boolean credentialsNonExpired = true;
		boolean accountNonLocked = true;
		
		org.springframework.security.userdetails.User userdetail = new org.springframework.security.userdetails.User(
				user.getUsername(), user.getPassword(), enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, grantedAuths);

		return userdetail;
	}
	
	/**
	 * 获得用户所有角色的权限.
	 */
	private GrantedAuthority[] obtainGrantedAuthorities(Admin user) {
		Set<GrantedAuthority> authSet = new HashSet<GrantedAuthority>();
		List<Authorities> authorities = positionAuthorityManager.queryAdminAuthorities(user.getId());
		
		for (Authorities authority : authorities) {
			logger.info("---------authority---getName----------" + authority.getName());
			authSet.add(new GrantedAuthorityImpl(authority.getName()));
		}
		return authSet.toArray(new GrantedAuthority[authSet.size()]);
	}
}
