package tw.tw360.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.PositionDao;
import tw.tw360.dto.Position;


@Service
@Transactional
public class PositionManager {

	@Autowired
	private PositionDao positionDao;

	public List<Position> queryAllPosition() {
		return positionDao.queryAllPosition();
	}

	public List<Position> queryAllPositionAndauthorities() {
		return positionDao.queryAllPositionAndauthorities();
	}

	public void createPosition(String name,String[] authoritiesIds) {
		positionDao.createPosition(name, authoritiesIds);
	}
	
	/**
	 * 查询角色名字
	 * @author Donald
	 * time:2013-12-9 09:28:11
	 * @param id 角色id
	 * */
	public String queryPositionName(int id){
		return positionDao.queryPositionName(id);
	}

	 public int updatePositionStatus(int id,int status){
		 return positionDao.updatePositionStatus(id, status);
	 }
}
