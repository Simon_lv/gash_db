package tw.tw360.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.TccInvoiceDao;
import tw.tw360.dto.InvoiceCardCount;
import tw.tw360.dto.TccInvoice;

@Service
@Transactional
public class TccInvoiceManager {
	@Autowired
	private TccInvoiceDao tccInvoiceDao;
	
	public TccInvoice insert(TccInvoice tccInvoice) {
		return tccInvoiceDao.insert(tccInvoice);
	}

	public boolean checkInvoiceNo(String invoiceNo) {
		return tccInvoiceDao.checkInvoiceNo(invoiceNo);
	}
	
	public List<InvoiceCardCount> findAll(HashMap<String, String> map)
	{
	    return this.tccInvoiceDao.findAll(map);
	}
}
