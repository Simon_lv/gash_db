package tw.tw360.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.GameGiftDao;
import tw.tw360.dto.GameGift;

@Service
@Transactional
public class GameGiftManager {

	@Autowired
	private GameGiftDao ggDao;
	
	/**
     * 所有虛寶品項
     * 
     * @return
     */
    @Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<GameGift> queryAllGift() {
		return ggDao.queryAll();
	}
    
    public Page<GameGift> findAll(Page<GameGift> page, HashMap<String, String> map) {
		return ggDao.findAll(page, map);
	}
    
    public void updateStatus(long id, int status) {
    	ggDao.updateStatus(id, status);
	}
	
	public GameGift add(GameGift gameGift) {
		return ggDao.add(gameGift);		
	}
	
	public void update(GameGift gameGift) {
		ggDao.update(gameGift);		
	}
	
	public GameGift loadById(long id) {
		return ggDao.loadById(id);
	}
	
	public GameGift queryOffsetRecord(long id, int offset) {
		return ggDao.queryOffsetRecord(id, offset);
	}
	
	public void updateOrder(long id, int order) {
		ggDao.updateOrder(id, order);
	}
	
	public void updateToTop(long id, int order) {
		ggDao.updateToTop(id, order);
	}	
}