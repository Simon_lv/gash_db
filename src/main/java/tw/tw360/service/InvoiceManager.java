package tw.tw360.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.PaymentDao;

@Service
@Transactional
public class InvoiceManager {

	@Autowired
	private PaymentDao paymentDao;

	public Map<String, Object> saveInvoice(Map<Integer, String> data) {
		List<String> orderIds = new ArrayList<String>();
		List<String> invoiceNos = new ArrayList<String>();
		for (Entry<Integer, String> entry : data.entrySet()) {
			String d = entry.getValue();
			String[] dataArr = d.split(":");
			String orderId = dataArr[0];
			String invoiceNo = dataArr[1];
			if (orderId.contains(".")) {
				orderId = orderId.substring(0, orderId.indexOf("."));
			}
			orderIds.add(orderId);
			if (invoiceNo.contains(".")) {
				invoiceNo = invoiceNo.substring(0, invoiceNo.indexOf("."));
			}
			invoiceNos.add(invoiceNo);
		}
		List<String> hasInvoiceNo = paymentDao.queryHasInvoiceNo(orderIds);
		List<String> existOrderIds = paymentDao.queryExistOrder(orderIds);
		List<String> notExistOrders = new ArrayList<String>();
		for (String o : orderIds) {
			if (!existOrderIds.contains(o)) {
				notExistOrders.add(o);
			}
		}
		List<String> order2Update = paymentDao.queryHasNotInvoiceNo(orderIds);
		List<Object[]> params = new ArrayList<Object[]>();
		for (int i = 0; i < orderIds.size(); i++) {
			if (order2Update.contains(orderIds.get(i))) {
				Object[] p = new Object[] { invoiceNos.get(i), orderIds.get(i) };
				params.add(p);
			}
		}
		paymentDao.updateOrderInvoiceNo(params);
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("total", orderIds.size());
		result.put("success", order2Update.size());
		result.put("hasInvoceNo", hasInvoiceNo);
		result.put("notExistOrder", notExistOrders);
		return result;
	}
}
