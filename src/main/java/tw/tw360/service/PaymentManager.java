package tw.tw360.service;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.PaymentDao;
import tw.tw360.dto.Payment;
import tw.tw360.query.PaymentQuery;

@Service
@Transactional
public class PaymentManager {
	@Autowired
	private PaymentDao payDao;

	public Payment add(final Payment pay) {
		return payDao.add(pay);
	}

	public int updatePreSell(final Payment pay) {
		return payDao.updatePreSell(pay);
	}

	public int updateConfirm(final Payment pay) {
		return payDao.updateConfirm(pay);
	}

	public int updateSmsBack(final Payment pay) {
		return payDao.updateSmsBack(pay);
	}

	public int updateRestore(final Payment pay) {
		return payDao.updateRestore(pay);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<Payment> queryByIds(String ids) {
		return payDao.queryByIds(ids);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<Payment> queryByDriverMobile(String mobile, int status) {
		return payDao.queryByDriverMobile(mobile, status);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Page<PaymentQuery> queryForPage(Page<PaymentQuery> page,
			HashMap<String, String> params) {
		return payDao.queryForPage(page, params);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public Double querySumForPayment(HashMap<String, String> params) {
		return payDao.sumForPayment(params);
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public List<PaymentQuery> queryALLPayment(HashMap<String, String> params) {
		params.put("export", "export");
		Page<PaymentQuery> page = new Page<PaymentQuery>(Integer.MAX_VALUE);
		page = payDao.queryForPage(page, params);
		return page.getResult();
	}

	@Transactional(propagation = Propagation.NOT_SUPPORTED, readOnly = true)
	public PaymentQuery findById(long id) {
		return payDao.findById(id);
	}

	public void updateInvoiceData(Payment payment) {
		payDao.updateInvoiceData(payment);
	}

	public int updateOrderStatus(long id, int status) {
		return payDao.updateOrderStatus(id, status);
	}

	public int updateOrderStatusAndSmsTime(long id, int status, Timestamp time) {
		return payDao.updateOrderStatusAndSmsTime(id, status, time);
	}

	public int updateStatus(long id, int status) {
		return payDao.updateStatus(id, status);
	}

	public List<PaymentQuery> queryNeedRestoreByCarId(String carId) {
		return payDao.queryNotRestoreByCarId(carId);
	}

	public List<Payment> queryByOrderStatusAndSmsTime(int status) {
		return payDao.queryByOrderStatusAndSmsTime(status);
	}

	public List<PaymentQuery> queryFinishedPayment() {
		return payDao.queryFinishedPayment();
	}

	public List<Payment> findRestoreByOrderIds(String[] orderId) {
		return payDao.findRestoreByOrderIds(orderId);
	}

	public void updateRestore(List<Payment> payList, int i, long id) {
		payDao.updateRestore(payList, i, id);
	}
}
