package tw.tw360.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.BackSmsRecordDao;
import tw.tw360.dto.BackSmsRecord;
import tw.tw360.dto.SmsRecord;

/**
 * 回覆簡訊紀錄
 * @version 1.0
 * */
@Service
@Transactional
public class BackSmsRecordManager {
	@Autowired
    private BackSmsRecordDao smsRecordDao;
	
	/**
	 * 用簡訊ID找簡訊
	 * */
	public BackSmsRecord findByMessageId(String messageId){
		return smsRecordDao.findByMessageId(messageId);
	}
	
	/**
	 * 儲存簡訊
	 * */
	public BackSmsRecord add(BackSmsRecord smsRecord){
		return smsRecordDao.add(smsRecord);
	}
	
	/**
	 * 用手機號碼,建立時間range 找簡訊
	 * */
	public Page<HashMap<String, String>> findByViewCondition(Page<HashMap<String, String>> page, String mobileNo, String startTime, String endTime, boolean noPage){
		return smsRecordDao.findByViewCondition(page, mobileNo, startTime, endTime, noPage);
	}

}
