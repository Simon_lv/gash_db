package tw.tw360.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springside.modules.orm.Page;

import tw.tw360.dao.BannerDao;
import tw.tw360.dto.Banner;

@Service
@Transactional
public class BannerManager {

	@Autowired
	private BannerDao bannerDao;
	
	public Page<Banner> findAll(Page<Banner> page, HashMap<String, String> map) {
		return bannerDao.findAll(page,map);
	}
	
	public Banner loadById(long id){
		return bannerDao.loadById(id);
	}
	
	public Banner queryOffsetRecord(long id, int offset){
		return bannerDao.queryOffsetRecord(id, offset);
	}
	
	
	public void updateToTop(long id, int orderIndex){
		bannerDao.updateToTop(id, orderIndex);
	}
	
	public void updateOrderIndex(long id, int orderIndx){
		bannerDao.updateOrderIndex(id, orderIndx);
	}
	
	public void updateStatus(long id, int status) {
		bannerDao.updateStatus(id, status);
	}
	
	public Banner add(Banner banner) {
		return bannerDao.add(banner);		
	}

	public List<Banner> queryAll() {
		return bannerDao.queryAll();
	}
}
