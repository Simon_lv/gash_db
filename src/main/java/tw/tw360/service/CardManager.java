package tw.tw360.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.CardDao;
import tw.tw360.dto.Card;
import tw.tw360.dto.InvoiceCard;
import tw.tw360.dto.PayedCardQuery;


@Service
@Transactional
public class CardManager {
	@Autowired
   private CardDao cardDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public int countByTypeAndPrice(int type, int price){
		return cardDao.countByTypeAndPrice(type, price);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<Map<String, Integer>> queryRemiansGroupByPrice(int soldStatus, int cardType) {
		return cardDao.queryRemiansGroupByPrice(soldStatus, cardType);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<Long> queryAvailableByPriceAndCount(int type, int price, int count) {
		return cardDao.queryAvailableByPriceAndCount(type, price, count);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<PayedCardQuery> findAll(Map<String,String> map) {
		return cardDao.findAll(map);
	}
	public int updateListStatus(List<Long> idList, int status) {
		StringBuffer ids = new StringBuffer();
		for (long id : idList) {
			ids.append(',').append(id);
		}
		return cardDao.updateListStatus(ids.substring(1), status);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public int sumByIds(String ids) {
		return cardDao.sumByIds(ids);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public Card findById(long id){
		return cardDao.findById(id);
	}
	
	public int updateSoldStatusAndTime(long id, int status, Timestamp time) {
		return cardDao.updateSoldStatusAndTime(id, status, time);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<Card> querySoldTimout(int soldStatus) {
		return cardDao.querySoldTimout(soldStatus);
	}

	public List<InvoiceCard> findAllForSoldpoint() {
		return cardDao.findAllForSoldpoint();
	}

	public boolean updateForTccInvoice(ArrayList<InvoiceCard> used, Long tccInvoiceId) {
		ArrayList<Long> ids = new ArrayList<Long>();
		StringBuilder sUsed = new StringBuilder();
		for(InvoiceCard ic : used) {
			ids.addAll(cardDao.getIdsForTccInvoice(ic));
		}
		
		for(Long i : ids) {
			sUsed.append(i).append(",");
		}
		sUsed.deleteCharAt(sUsed.length()-1);
		return cardDao.updateForTccInvoice(sUsed.toString(), tccInvoiceId);
	}

	public List<Card> queryByTccInvoiceId(Long tccInvoiceId) {
		return cardDao.queryByTccInvoiceId(tccInvoiceId);
	}

	public boolean checkForTccInvoice(ArrayList<InvoiceCard> parse) {
		boolean result = true;
		for(InvoiceCard invoiceCard : parse) {
			result = result && cardDao.checkForTccInvoice(invoiceCard);
			if(!result) {
				return result;
			}
		}
		return result;
	}
}
