package tw.tw360.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.GiftCardDao;
import tw.tw360.dto.GiftCardPointCount;

@Service
@Transactional
public class GiftCardManager {
	
	@Autowired
	private GiftCardDao gcDao;
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<GiftCardPointCount> availableGiftPoints(int type) {
		return gcDao.queryTypeOfPoint(type);
	}
	
	public String updateAdminIdPayment(List<GiftCardPointCount> availableGiftPoints, String selectedPaymentId, int type, String adminId) {
		return gcDao.updateAdminIdPayment(availableGiftPoints, selectedPaymentId, type, adminId);
	}

	public void updateAdminIdPaymentToNull(String selectedPaymentId, int type, String adminId) {
		gcDao.updateAdminIdPaymentToNull(selectedPaymentId, type, adminId);
	}
	
	public List<Map<String, String>> queryPwd(String selectedPaymentId, int type, String adminId) {
		return gcDao.queryPwd(selectedPaymentId, type, adminId);
	}
	
	public void updateSoldTime(String selectedPaymentId, int type, String adminId) {
		gcDao.updateSoldTime(selectedPaymentId, type, adminId);
	}
	
	public boolean existPaymentId(String paymentId) {
		return gcDao.existPaymentId(paymentId);
	}
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public List<String> getCantGiftPointsPaymentId(int type) {
		return gcDao.getCantGiftPointsPaymentId(type);
	}
}
