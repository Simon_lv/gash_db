package tw.tw360.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tw.tw360.dao.CardDao;
import tw.tw360.dao.PaymentDao;
import tw.tw360.util.SMSUtil;

@Service
@Transactional
public class SendSMSForGiftPointsManager {
	@Autowired
	private CardDao cardDao;
	@Autowired
	private PaymentDao paymentDao;
	private static final String FAIL_DESC = "發送失敗的destAddress:";
	private static final String SMS = "您已兌換%s點";
	
	@Transactional(propagation=Propagation.NOT_SUPPORTED,readOnly=true)
	public String sendSMS(int type, int point) {
		String pointSMS = String.format(SMS, point);
		StringBuilder sb = new StringBuilder();
		sb.append(FAIL_DESC);
		//1.  到card搜出符合點數的序號和id
		List<Long> cardIds = cardDao.querySoldByPrice(type, point);
		//2. 利用card.id到payment找出user_mobile
		List<String> userMobile = paymentDao.queryUserMobileByCardId(cardIds);
		//3. 發簡訊
		for(String s:userMobile) {
			try {
				SMSUtil.sendSMS(pointSMS, s);
			} catch (IOException e) {
				sb.append(s).append(" ");
			}
		}
		if(sb.length() == FAIL_DESC.length()) {
			return "簡訊發送成功";
		}
		return sb.toString();
	}
	
}
