package tw.tw360.util;


import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * JSON util tools
 * @author Donald
 * time:2013-12-5 16:21:24
 * @version 1.3
 * */
public class JsonUtil {
	
	/**
	 * 将Object转为JSON,返回json字符串
	 * @author Donald
	 * time:2013-12-5 16:24:06
	 * */
	public static String returnJsonStringForObject(Object object){
		 JSONObject json = JSONObject.fromObject(object);
		 return json.toString();
	}
	
	
	/**
	 * 将list对象装换为json字符串
	 * @author Donald
	 * time:2014-3-26 14:40:56
	 * */
	public static String returnJsonStringForArrayObject(Object object){
		JSONArray json = JSONArray.fromObject(object);
		 return  json.toString();
	}
}
