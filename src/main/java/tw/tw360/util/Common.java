package tw.tw360.util;

/**
 * @author Donald
 * time:2013-6-13 19:11:04
 * @version 1.0
 * */
public class Common {

	public static final String REPORT_DIRECTORY = "report";
	
	//正式機開始
		public static final String MPUSH_SYS_ID = "IVANTECY"; //遠傳簡訊服務API帳號代碼
		public static final String MPUSH_SRC_ADDRESS = "01916800021163900223";//遠傳簡訊服務用來發送訊息的來源位址(固定)
		public static final String MPUSH_API_URL = "http://61.20.32.60:6600";
	//正式機結束
		
	//測試機開始
	//public static final String MPUSH_SYS_ID = "K6ZXHVTA"; //遠傳簡訊服務API帳號代碼
	//public static final String MPUSH_SRC_ADDRESS = "01916800013234700000";//遠傳簡訊服務用來發送訊息的來源位址(固定)
	//public static final String MPUSH_API_URL = "http://61.20.32.60:6600";
	//測試機結束
	
	public static final int SESSION_TIMEOUT = 12*60*60;
}
