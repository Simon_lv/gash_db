package tw.tw360.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


import java.sql.Timestamp;
import java.util.ArrayList;

import java.util.List;



import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 導出excel工具類
 * @author jim
 * @version 1.3
 * */
public class ReportExcelUtil {
	
	protected final Logger logger = LoggerFactory.getLogger(getClass());
	
	private static List<String> xlsTitle = new ArrayList<String>();
	private static HSSFWorkbook wb = null;
	private static HSSFSheet sheet = null;
	private static String excelSheetName = "";
	private static String fullFileName = "";
	public static String createExcelFile(String filePath, String fileName,String sheetName,List<String> headerTitle) {

		fullFileName = filePath + fileName;
		excelSheetName = sheetName;
		xlsTitle = headerTitle;
		File path = new File(filePath);
		if (!path.exists()) {
			path.mkdirs();
		}
		File f = new File(fullFileName);
		createReport(f.getPath());

		return fileName;
	}

	public static String getReportFileFullPath(String folder) {
		String classPath = ReportExcelUtil.class.getClassLoader().getResource("").getPath();
		String filePath = classPath.substring(0, classPath.indexOf("WEB-INF")) + folder + File.separator;
		
		PrintUtil.outputContent("createReport file path is :"+ filePath);
		
		return filePath;
	}


	// 添加新月份的數據，先創建 寫表頭 再添加數據
	public static void createReport(String fileName) {
		wb = new HSSFWorkbook();
		sheet = wb.createSheet(excelSheetName);
		//sheet = createReportHeader(sheet);
		sheet = createReportHeader();
		//sheet = setDataToReport(sheet,data);
	}
	
	public static void finish(){
		try {
			FileOutputStream out = new FileOutputStream(fullFileName);
			out.flush();
	//		sheet.createFreezePane(1, 2, 1, 2);
			wb.write(out);
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static HSSFSheet createReportHeader() {
		int currRowNum = sheet.getLastRowNum();

		HSSFFont fontConsignee = sheet.getWorkbook().createFont();
		fontConsignee.setFontName("宋体");
		fontConsignee.setColor(HSSFColor.RED.index);
		HSSFCellStyle styleTotal = sheet.getWorkbook().createCellStyle();
		HSSFFont fontTotal = sheet.getWorkbook().createFont();
		fontTotal.setFontName("宋体");
		fontTotal.setColor(HSSFColor.BLACK.index);
		fontTotal.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		styleTotal.setFont(fontTotal);
		styleTotal.setAlignment(HSSFCellStyle.ALIGN_LEFT);
		styleTotal.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
		styleTotal.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		HSSFRow currRow = sheet.createRow(currRowNum);

		for (int i = 0; i < xlsTitle.size(); i++) {
			currRow.createCell(i).setCellValue(new HSSFRichTextString(xlsTitle.get(i)));
		}
		
		return sheet;
	}

	public static HSSFSheet addDataToReport(List<String> data) {
		try {
			HSSFRow currRow = sheet.createRow(sheet.getLastRowNum() + 1);
			// 统计列样式
			/*HSSFCellStyle styleTotal = sheet.getWorkbook().createCellStyle();
			HSSFFont fontTotal = sheet.getWorkbook().createFont();
			fontTotal.setFontName("宋体");
			fontTotal.setColor(HSSFColor.BLACK.index);
			fontTotal.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			styleTotal.setFont(fontTotal);
			styleTotal.setAlignment(HSSFCellStyle.ALIGN_LEFT);
			styleTotal.setFillForegroundColor(HSSFColor.GREY_40_PERCENT.index);
			styleTotal.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);*/
	
			for (int i = 0; i < data.size(); i++){
				currRow.createCell(i).setCellValue(data.get(i));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sheet;
	}

	

	/**
	 * 文件拷贝，将源文件拷贝到目标文件夹下
	 * 
	 * @param sourceFile
	 *            源文件（文件或文件夹）
	 * @param desFile
	 *            目标文件（文件夹）
	 * @return
	 */
	public static boolean copyFile(String sourceFile, String desFile,String date) throws IOException {

		File source = new File(sourceFile);
		if (!source.exists()) {
			PrintUtil.outputContent(source.getAbsolutePath()+ "========源文件不存在！=======");
			return false;
		}
		File des = new File(desFile);
		
		if (!des.exists()){
			des.mkdirs();// 不存在目标文件就创建
		}
		
		FileInputStream input = null;
		FileOutputStream output = null;
		try {
			if (source.isFile()) { // 如果是文件 则读源文件 写入目标文件
				input = new FileInputStream(source);
				output = new FileOutputStream(new File(desFile + "/"+ source.getName() + ".bak." + date));
				byte[] b = new byte[1024 * 5];
				int len;
				while ((len = input.read(b)) != -1) { // 读文件
					output.write(b, 0, len); // 向目标文件写文件
				}
				input.close();
				output.flush();
				output.close();
			} else {
				PrintUtil.outputContent(source.getAbsolutePath()+ "========这不是文件类型！=======");
				return false;
			}

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			if (input != null){
				input.close();
			}
			if (output != null){
				output.close();
			}
		}
	}
	
	/**
	 * check time is null
	 * @author Donald
	 * time:2013-8-15 9:54:03
	 * */
	public static String checkTimeNull(Timestamp str){
		String string = null;
		if(str==null){
			string="";
		}else{
			string = str+"";
		}
		return string;
	}
}
