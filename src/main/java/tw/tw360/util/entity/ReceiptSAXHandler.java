package tw.tw360.util.entity;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.AttributeList;
import org.xml.sax.HandlerBase;
import org.xml.sax.SAXException;

public class ReceiptSAXHandler extends HandlerBase {
	
	private Hashtable table = new Hashtable();
	
	private Hashtable childTable = new Hashtable();
	
	private LinkedHashMap<String, String> receiptsMap = new LinkedHashMap<String, String>();
	
	private List<LinkedHashMap> list = new ArrayList<LinkedHashMap>();
	
	private boolean isRepeat = false;

	private String currentElement = null;

	private String currentValue = null;
	
	private String parentElement = "";
	
	private boolean isParent = false;
	
	private int step = 0;

	public void setTable(Hashtable table)

	{

		this.table = table;

	}

	public Hashtable getTable()

	{

		return table;

	}

	public void startElement(String tag, AttributeList attrs)

	throws SAXException

	{

		System.out.println(tag);
		
		if(!isParent){
			parentElement = tag;
			isParent = true;
		}
		if(!tag.equals(parentElement) && currentElement!=null && !currentElement.equals(parentElement) && step == 1){
			isRepeat = true;
		}
		currentElement = tag;

		step = 1;
	}

	public void characters(char[] ch, int start, int length)

	throws SAXException

	{

		currentValue = new String(ch, start, length);

	}

	public void endElement(String name) throws SAXException

	{
		System.out.println("2=="+name);
		if(!currentElement.equals(name)){
			if(parentElement.equals(name)){
				Iterator it = childTable.entrySet().iterator(); 
				   while(it.hasNext()){ 
				    Map.Entry m=(Map.Entry)it.next(); 
				    table.put(m.getKey(), m.getValue());
				   } 
				
			}else{
				list.add(receiptsMap);
				childTable.put(name, list);
				isRepeat = false;
				receiptsMap = new LinkedHashMap<String, String>(); 
			}
			
		}else{
			if(isRepeat){
				receiptsMap.put(currentElement, currentValue);
			}else
			   table.put(currentElement, currentValue);
		}
		
		step = 2;
			

	}
	
	public Hashtable getXMLdata(String url) {
		Hashtable hashTable = new Hashtable();
		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();

			SAXParser sp;
			sp = spf.newSAXParser();
			ReceiptSAXHandler handler = new ReceiptSAXHandler();
			System.out.println(url);
			sp.parse(url, handler);

			hashTable = handler.getTable();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return hashTable;
	}
	
	public Hashtable getXMLdata(InputStream input) {
		Hashtable hashTable = new Hashtable();
		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();
			
			SAXParser sp;
			sp = spf.newSAXParser();
			ReceiptSAXHandler handler = new ReceiptSAXHandler();
//			System.out.println(url);
			sp.parse(input, handler);
			
			hashTable = handler.getTable();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return hashTable;
	}
	
	
}

class Receipt {
	private String messageId;
	private String destAddress;
	private String deliveryStatus;
	private String submitDate;
	private String doneDate;
	private String seq;
	public String getMessageId() {
		return messageId;
	}
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public String getDestAddress() {
		return destAddress;
	}
	public void setDestAddress(String destAddress) {
		this.destAddress = destAddress;
	}
	public String getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public String getSubmitDate() {
		return submitDate;
	}
	public void setSubmitDate(String submitDate) {
		this.submitDate = submitDate;
	}
	public String getDoneDate() {
		return doneDate;
	}
	public void setDoneDate(String doneDate) {
		this.doneDate = doneDate;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	
	
}
