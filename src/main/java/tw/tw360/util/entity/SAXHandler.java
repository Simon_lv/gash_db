package tw.tw360.util.entity;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.AttributeList;
import org.xml.sax.HandlerBase;
import org.xml.sax.SAXException;

public class SAXHandler extends HandlerBase {
	
	private Hashtable table = new Hashtable();

	private String currentElement = null;

	private String currentValue = null;

	public void setTable(Hashtable table)

	{

		this.table = table;

	}

	public Hashtable getTable()
	{

		return table;

	}

	public void startElement(String tag, AttributeList attrs)

	throws SAXException

	{

		currentElement = tag;

	}

	public void characters(char[] ch, int start, int length)

	throws SAXException

	{

		currentValue = new String(ch, start, length);

	}

	public void endElement(String name) throws SAXException

	{

		if (currentElement.equals(name))

			table.put(currentElement, currentValue);

	}
	
	public Hashtable getXMLdata(String url) {
		Hashtable hashTable = new Hashtable();
		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();

			SAXParser sp;
			sp = spf.newSAXParser();
			SAXHandler handler = new SAXHandler();
			System.out.println(url);
			sp.parse(url, handler);

			hashTable = handler.getTable();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return hashTable;
	}
	
	public Hashtable getXMLdata(InputStream input) {
		Hashtable hashTable = new Hashtable();
		try {
			SAXParserFactory spf = SAXParserFactory.newInstance();
			
			SAXParser sp;
			sp = spf.newSAXParser();
			SAXHandler handler = new SAXHandler();
//			System.out.println(url);
			sp.parse(input, handler);
			
			hashTable = handler.getTable();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return hashTable;
	}
	
	
}
