package tw.tw360.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * File tools
 * 
 * @author Donald time:2013-12-2 12:07:05
 * @version 1.0
 * */
public class FileUtil {

	/**
	 * 给一个文件追加写入内容
	 * 
	 * @author Donald time:2013-12-2 14:23:59
	 * @param String
	 *            filePath
	 * @param String
	 *            content
	 */
	public static void appendContent(String fileName, String content) {
		try {
			FileWriter writer = new FileWriter(fileName, false);
			writer.write(content);
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String downloadFromNet(String fileUrl, String savePath)
			throws MalformedURLException {
		String saveFilePath = "";
		// 下载网络文件
		int bytesum = 0;
		int byteread = 0;

		File pathFile = new File(savePath); // 创建文件对象
		String fileName = fileUrl.substring(fileUrl.lastIndexOf("/"));
		URL url = new URL(fileUrl);
		if (!pathFile.exists()) {
			pathFile.mkdir();
		}

		saveFilePath = savePath + fileName;

		try {
			URLConnection conn = url.openConnection();
			InputStream inStream = conn.getInputStream();
			FileOutputStream fs = new FileOutputStream(saveFilePath);

			byte[] buffer = new byte[1204];
			int length;
			while ((byteread = inStream.read(buffer)) != -1) {
				bytesum += byteread;
				fs.write(buffer, 0, byteread);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return saveFilePath;
	}

	
	public static String filesToZip(List<String> files,String filePath,String fileName) throws Exception 
	{
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		String zipFilePath = filePath + File.separatorChar + fileName;
		File zipFile = new File(zipFilePath);
		if (zipFile.exists()) {
			PrintUtil.outputContent(zipFilePath+"已經存在！");
			return fileName;
		} else 
		{
			
			try {
			List<File> sourceFiles = new ArrayList<File>();
			
			for(int i=0;i<files.size();i++)
			{
				PrintUtil.outputItem("file-"+i, files.get(i));
				sourceFiles.add(new File(files.get(i)));
			}
			fos = new FileOutputStream(zipFile);
			zos = new ZipOutputStream(new BufferedOutputStream(fos));
			byte[] bufs = new byte[1024 * 10];
			for (int i = 0; i < sourceFiles.size(); i++) 
			{
				// 创建ZIP实体,并添加进压缩包
				ZipEntry zipEntry = new ZipEntry(sourceFiles.get(i).getName());
				zos.putNextEntry(zipEntry);
				// 读取待压缩的文件并写进压缩包里
				fis = new FileInputStream(sourceFiles.get(i));
				bis = new BufferedInputStream(fis, 1024 * 10);
				int read = 0;
				while ((read = bis.read(bufs, 0, 1024 * 10)) != -1) 
				{
					zos.write(bufs, 0, read);
				}
			}
			
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			
			} catch (IOException e) {
				e.printStackTrace();
		
			} finally {
				// 关闭流
				try {
					if (null != bis)
						bis.close();
					if (null != zos)
						zos.close();
				} catch (IOException e) {
					e.printStackTrace();
		
				}
			}
		}
		
		return fileName;
	}
	/**
	 * 将存放在sourceFilePath目录下的源文件,打包成fileName名称的ZIP文件,并存放到zipFilePath。
	 * 
	 * @param sourceFilePath
	 *            待压缩的文件路径
	 * @param zipFilePath
	 *            压缩后存放路径
	 * @param fileName
	 *            压缩后文件的名称
	 * @return flag
	 */
	public static String folderToZip(String sourceFilePath, String zipFilePath,String fileName) 
	{
		boolean flag = false;
		String zipFileName = zipFilePath + File.separatorChar + fileName;
		File sourceFile = new File(sourceFilePath);
		FileInputStream fis = null;
		BufferedInputStream bis = null;
		FileOutputStream fos = null;
		ZipOutputStream zos = null;
		if (sourceFile.exists() == false) {
			PrintUtil.outputContent("壓縮的目錄不存在！");
		} else {
			try {
				File zipFile = new File(zipFileName);
				if (zipFile.exists()) {
					PrintUtil.outputContent(zipFileName+"已經存在！");
					return zipFileName;
				} else {
					File[] sourceFiles = sourceFile.listFiles();
					if (null == sourceFiles || sourceFiles.length < 1) {
						PrintUtil.outputContent(sourceFile+"下不存在要壓縮的文件！");
					} else {
						fos = new FileOutputStream(zipFile);
						zos = new ZipOutputStream(new BufferedOutputStream(fos));
						byte[] bufs = new byte[1024 * 10];
						for (int i = 0; i < sourceFiles.length; i++) 
						{
							// 创建ZIP实体,并添加进压缩包
							ZipEntry zipEntry = new ZipEntry(
									sourceFiles[i].getName());
							zos.putNextEntry(zipEntry);
							// 读取待压缩的文件并写进压缩包里
							fis = new FileInputStream(sourceFiles[i]);
							bis = new BufferedInputStream(fis, 1024 * 10);
							int read = 0;
							while ((read = bis.read(bufs, 0, 1024 * 10)) != -1) {
								zos.write(bufs, 0, read);
							}
						}
					
					}
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException(e);
			} finally {
				// 关闭流
				try {
					if (null != bis)
						bis.close();
					if (null != zos)
						zos.close();
				} catch (IOException e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		}
		return zipFileName;
	}
	
	public static void main(String[] args) throws Exception
	{
		//fileToZip("E:\\希悦\\temp\\abc\\","E:\\希悦\\temp\\abc","test.zip");
		List<String> files = new ArrayList<String>();
		files.add("E:\\希悦\\temp\\abc\\a.gif");
		filesToZip(files,"E:\\希悦\\temp\\abc","test2.zip");
	}

}
