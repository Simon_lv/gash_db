package tw.tw360.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 获取系统图片上传配置path
 * @author Donald
 * time:2013-12-5 16:59:49
 * */
public class SystemProperty {
	private Properties property = new Properties();

	private String location;

	protected void loadProperties() {
		if (this.location != null) {
			InputStream is = null;
			try {
				InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(location);
				if (inputStream == null) {
					throw new FileNotFoundException("property file '"
							+ location + "' not found in the classpath");
				}
				property.load(inputStream);
			} catch (IOException ex) {
				ex.printStackTrace();
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	public Properties getProperty() {
		return property;
	}

	public void setProperty(Properties property) {
		this.property = property;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
		loadProperties();
	}

	public String getImageServerUrl() {
		return property.getProperty("image.server.url");
	}

	public String getImageServerPath() {
		return property.getProperty("image.server.path");
	}
}
