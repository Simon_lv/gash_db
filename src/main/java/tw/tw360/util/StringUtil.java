package tw.tw360.util;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.RandomStringUtils;

/**
 * 字符串處理
 * 
 * @author Donald time:2013-6-13 19:07:16
 * @version 1.4
 * */
public class StringUtil {

	private final static char[] STRRING_DATA = { '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9' };
	
	private static final Pattern EMAIL_PATTERN = Pattern
		      .compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		    			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

	/**
	 * 随机生成数字随机码
	 * 
	 * @param count
	 * @return
	 */
	public static String createRandomString(int count) {
//		StringBuilder str = new StringBuilder();
//		int length = STRRING_DATA.length;
//		Random random = new Random();
//
//		for (int i = 0; i < count; i++) {
//			str.append(STRRING_DATA[random.nextInt(length)].toString().trim());
//		}
//
//		return str.toString();
		return RandomStringUtils.random(count, STRRING_DATA);
	}

	/**
	 * 判斷字符串是否為空
	 * 
	 * @author Donald time:2013-6-13 19:07:47
	 * @param text
	 *            String 字符串
	 * */
	public static String isNull(String text) {
		if (text == null) {
			return "";
		} else {
			return text;
		}
	}

	/**
	 * 將字符串轉換為Long
	 * 
	 * @author Donald time：2013-10-17 12:07:34
	 * */
	public static Long getLongFromString(String str) {
		Long longStirng = 0L;
		try {
			longStirng = Long.parseLong(str);
		} catch (Exception e) {

		}
		return longStirng;
	}

	/**
	 * 將字符串轉換為int
	 * 
	 * @author Donald time：2013-10-17 12:07:34
	 * @return 轉換異常返回 0
	 * */
	public static int getIntFromString(String str) {
		int number = 0;
		try {
			number = Integer.parseInt(str);
		} catch (Exception e) {

		}
		return number;
	}

	/**
	 * 將字符串轉換為int
	 * 
	 * @author Donald time：2013-10-17 12:07:34
	 * @return 轉換異常返回 -1
	 * */
	public static int getStringToInt(String str) {
		int number = -1;
		try {
			number = Integer.parseInt(str);
		} catch (Exception e) {

		}
		return number;
	}

	/**
	 * 將字符串轉換為float
	 * 
	 * @author Donald time：2013-10-17 12:07:34
	 * */
	public static float getFloatFromString(String str) {
		float floatNumber = 0;
		try {
			floatNumber = Float.parseFloat(str);
		} catch (Exception e) {

		}
		return floatNumber;
	}
	
	/**
	 * 對手機的合法性進行判斷
	 * 
	 * @param mobile
	 * @return
	 */
	public static boolean checkMoblile(String mobile) {
		// 台灣手機正則表達式
		String check1 = "09[0-9]{2}[0-9]{3}[0-9]{3}";
		//String check = "^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
		//Pattern regex = Pattern.compile(check);
		//Matcher matcher = regex.matcher(mobile);
		Pattern regex1 = Pattern.compile(check1);
		Matcher matcher1 = regex1.matcher(mobile);
		//if (matcher.matches() || matcher1.matches()) {
		if (matcher1.matches()) {
			System.out.println("手机号符合要求");
			return true;
		} else {
			System.out.println("手机号不符合要求");
			return false;
		}

	}
	
	/**
     * Email 格式檢查程式
     **/
	public static boolean isValidEmail(String email) {
		Matcher matcher = EMAIL_PATTERN.matcher(email);
		return matcher.matches();
	}
}
