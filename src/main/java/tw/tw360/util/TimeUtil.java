package tw.tw360.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author jim 
 * time:2013-11-13 19:11:04
 * @version 1.2
 * */
public class TimeUtil {

	private final static int MINUTE_UNIT = 60 * 1000;
	private final static int HOUR_UNIT = 60 * 60 * 1000;

	/**
	 * 比较分钟数
	 * 
	 * @param startTime
	 *            millisec
	 * @param endTime
	 *            millisec
	 * @param minuteCount
	 * @return
	 */
	public static boolean compareTimeByMinute(Long startTime, Long endTime,
			int minuteCount) {
		Long min = (endTime - startTime) / MINUTE_UNIT;
		if (min.intValue() > minuteCount) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 比较小时数
	 * 
	 * @param startTime
	 *            millisec
	 * @param endTime
	 *            millisec
	 * @param hourCount
	 * @return
	 */
	public static boolean compareTimeByHour(Long startTime, Long endTime,
			int hourCount) {
		Long hour = (endTime - startTime) / HOUR_UNIT;
		if (hour.intValue() > hourCount) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * 获得当前时间
	 * @author Donald
	 * time:2013-11-21 18:44:17
	 * */
	public static Timestamp createNowTimestamp() {
		return new Timestamp(System.currentTimeMillis());
	}
	
	
	/**
	 * 获取系统当天日期【yyyy-MM-dd】
	 * @author Donald
	 * time:2013-12-2 15:55:08
	 * */
	public static String createNowDate(){
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.format(date);
	}
	
	/**
	 * 获取系统当前时间【yyyy-MM-dd HH:ss:mm】
	 * @author Donald
	 * time:2013-12-5 16:57:33
	 * */
	public static String createNowTime(){
		Date date = new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:ss:mm");
		return df.format(date);
	}
}
