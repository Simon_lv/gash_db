package tw.tw360.util;

import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

public class PrintUtil {

	private static final String OUTPUT_LOG_STRING = "%s-->%s-->%s"; 

	public static void printRequest(String tag, String method,
			HttpServletRequest request) {
		System.out
				.println("*****************************************************************");
		System.out.println("input url：" + request.getRequestURI());
		Enumeration paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {// 遍历Enumeration
			String name = (String) paramNames.nextElement();// 取出下一个元素
			String value = request.getParameter(name);// 获取元素的值
			outputItem(name, value);

		}
		System.out
				.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	}

	public static void printResponse(Iterator it) {
		System.out
				.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println("output result：");
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String key = (String) entry.getKey();
			Object value = entry.getValue();
			outputItem(key, value);
		}
		System.out
				.println("*****************************************************************");
	}

	public static HashMap<String, String> printAndReturnRequest(String tag,
			String method, HttpServletRequest request) {
		System.out
				.println("*****************************************************************");
		HashMap<String, String> params = new HashMap<String, String>();
		System.out.println("input url：" + request.getRequestURI());
		Enumeration paramNames = request.getParameterNames();
		while (paramNames.hasMoreElements()) {// 遍历Enumeration
			String name = (String) paramNames.nextElement();// 取出下一个元素
			String value = "";
			String pageValue = request.getParameterValues(name).length > 1 ? StringUtils.join(request.getParameterValues(name), ",") : request.getParameter(name);// 获取元素的值
			try {
				String utf = new String(pageValue.getBytes("ISO-8859-1"),
						"UTF-8"); // 将字符编码装换为UTF-8
				if (utf.indexOf("?") != -1) { // 如果字符本身就是UTF-8字符，使用原来的字符
					value = pageValue;
				} else {
					value = utf;
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			params.put(name, value);
			outputItem(name, value);
		}

		System.out
				.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		return params;
	}

	public static void printResponse(Map resultMap) {
		Iterator it = resultMap.entrySet().iterator();
		System.out
				.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		System.out.println("output result：");
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			String key = (String) entry.getKey();
			Object value = entry.getValue();
			outputItem(key, value);
		}
		System.out
				.println("*****************************************************************");
	}

	public static void logOutput(String tag, String method, String content) {
		System.out.println(String.format(OUTPUT_LOG_STRING, tag, method,
				content));
	}

	public static void outputItem(String name, Object value) {
		if (name.length() <= 5) {
			name = name + "			=	";
		} else if (name.length() > 13) {
			name = name + "	=	";
		} else {
			name = name + "		=	";

		}

		System.out.println(name + value);

	}

	public static void outputItem(String name, String value) {
		if (name.length() <= 5) {
			name = name + "			=	";
		} else {
			name = name + "		=	";

		}
		System.out.println(name + value);

	}

	public static void outputContent(String content) {

		System.out.println("			=	" + content);

	}

	public static void outputContent(Object content) {

		System.out.println("			=	" + content);

	}

}
