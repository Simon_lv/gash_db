package tw.tw360.util;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.List;

import org.apache.axis.encoding.Base64;

import tw.tw360.util.entity.ReceiptSAXHandler;
import tw.tw360.util.entity.SAXHandler;


/**
 * 短信【发送\查询\收取】 操作类
 * 
 * @author Wilson
 * 
 */
public class SMSUtil {

	/**
	 * 透過MPush API發送SMS至收件人(s)
	 * @param smsBody
	 * @param destAddress
	 * @param isShort  when is true 簡訊內容 (70中文字或160ASCII字元),or 簡訊內容1000 utf-8 bytes
	 * @return
	 * @throws IOException
	 */
	public static boolean sendSMS(String smsBody, String destAddress, boolean isShort) throws IOException {
		String smsType = "";
		String smsApi = "";
		if(isShort){
			smsType = "ShortSmsSubmitReq";
			smsApi = "shortsmssubmit";
		}else{
			smsType= "SmsSubmitReq";
			smsApi = "smssubmit";
		}
		PrintUtil.outputItem("簡訊內容", smsBody);
		smsBody = Base64.encode(smsBody.getBytes("UTF-8"));
		String xml =
			 "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " + 
			"<"+smsType+">" + 
			"    <SysId>" + Common.MPUSH_SYS_ID + "</SysId >" + 
			"    <SrcAddress>" + Common.MPUSH_SRC_ADDRESS + "</SrcAddress>" + 
			"    <DestAddress>" + destAddress + "</DestAddress>" + 
//			"    <DestAddress>886986001003</DestAddress>" +  
			"    <SmsBody>" + smsBody + "</SmsBody>" + 
			"   <DrFlag>true</DrFlag>" + 
			"</"+smsType+">";
		
		URL url = new URL(Common.MPUSH_API_URL+"/mpushapi/"+smsApi);
		
		URLConnection urlConn = url.openConnection();
		urlConn.setDoInput(true);
		urlConn.setDoOutput(true);
		urlConn.setUseCaches(false);
		urlConn.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
		DataOutputStream printout = new DataOutputStream(urlConn.getOutputStream()); 
		String content = URLEncoder.encode(xml,"UTF-8");
		printout.writeBytes ("xml=" + content);
		
		printout.flush();
		printout.close();
		
		/******** expected result XML: 
		<?xml version="1.0" encoding="UTF-8"?> 
		<SubmitRes> 
		    <MessageId>A13D98F</MessageId>
		    <ResultCode>00000</ResultCode>
		    <ResultText>Message accepted for processing.</ResultText>
		</SubmitRes>
		　*********/
		
		Hashtable hashTable = new Hashtable();
		SAXHandler handler = new SAXHandler();
		InputStream in = urlConn.getInputStream();
		hashTable = handler.getXMLdata(in);
		String result = (String) hashTable.get("ResultCode");
		in.close();
		PrintUtil.outputItem("MessageId" ,(String) hashTable.get("MessageId"));
		PrintUtil.outputItem("ResultCode",result);
		PrintUtil.outputItem("ResultText",(String) hashTable.get("ResultText"));
		if(result != null && result.equals("00000")){
			return true;
		}else
			return false;
	}
	
	/**
	 * 透過MPush單則SMS限定API發送簡訊至收件人(s)
	 * @param smsBody
	 * @param destAddress
	 * @return
	 * @throws IOException
	 */
	public static boolean sendSMS(String smsBody, String destAddress) throws IOException {
		return sendSMS(smsBody, destAddress, true);
	}
	
	/**
	 * 提供服務提供商(Service Provider)取得兩週內SMSC已送達MPush的DR(Delivery
	 * Report)資訊。如果呼叫API時，所請求的DR資訊尚未送達，則MPush的API呼叫回覆將不會包含Receipt參數資訊。
	 * 
	 * @return 返回格式——{Receipt=[{MessageId=111774, DestAddress=886955227034,
	 *         DeliveryStatus=delivered, SubmitDate=080401163445,
	 *         DoneDate=080401163631, Seq=1/3}, {MessageId=111774,
	 *         DestAddress=sdfsfsfs}], ResultText=Request successfully
	 *         processed., ResultCode=00000}
	 * @throws IOException
	 */
	public static Hashtable smsRetrieveDr() throws IOException {
		String smsApi = "smsretrievedr";
		String xml =
			 "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " + 
			"<SmsRetrieveDrReq>" + 
			"    <SysId>" + Common.MPUSH_SYS_ID + "</SysId >" + 
			"</SmsRetrieveDrReq>";
		
		URL url = new URL(Common.MPUSH_API_URL+"/mpushapi/"+smsApi);
		
		URLConnection urlConn = url.openConnection();
		urlConn.setDoInput(true);
		urlConn.setDoOutput(true);
		urlConn.setUseCaches(false);
		urlConn.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
		DataOutputStream printout = new DataOutputStream(urlConn.getOutputStream()); 
		String content = URLEncoder.encode(xml,"UTF-8");
		printout.writeBytes ("xml=" + content);
		
		printout.flush();
		printout.close();
		
		/******** expected result XML: 
		<?xml version="1.0" encoding="UTF-8"?>
<SmsRetrieveDrRes>
   
    <ResultCode>00000</ResultCode>
    <ResultText>Request successfully processed.</ResultText>
    <Receipt>
       <MessageId>111774</MessageId>
       <DestAddress>886955227034</DestAddress>
       <DeliveryStatus>delivered</DeliveryStatus>
       <SubmitDate>080401163445</SubmitDate>
       <DoneDate>080401163631</DoneDate>
       <Seq>1/3</Seq>
    </Receipt>
    <Receipt>
       <MessageId>111774</MessageId>
       <DestAddress>886955227035</DestAddress>
       <DeliveryStatus>enroute</DeliveryStatus>
       <ErrorCode>107</ErrorCode>
       <SubmitDate> 0804011635</SubmitDate>
    </Receipt>
</SmsRetrieveDrRes>
		　*********/
		
		Hashtable hashTable = new Hashtable();
		ReceiptSAXHandler handler = new ReceiptSAXHandler();
		InputStream in = urlConn.getInputStream();
		hashTable = handler.getXMLdata(in);
		String result = (String) hashTable.get("ResultCode");
		in.close();
		PrintUtil.outputItem("ResultCode",result);
		if(result != null && result.equals("00000")){
			return hashTable;
		}else
			return null;
	}
	
	/**
	 * 　提供外部系統查詢簡訊發送狀態。長簡訊發送成功後，將拆分成多則簡訊發送狀態資料。無論長短簡訊皆可藉由Receipt/
	 * Seq欄位值得知根據查詢條件所回覆的發送狀態屬於簡訊發送的第幾則MT
	 * 。當前訊息發送狀態由Receipt/DeliveryStatus欄位值取得。可針對特定的job ID 及 門號 進行查詢。一般是在使用Push
	 * DR或Retrieve DR後，若有需要重新確認 DR資訊的外部系統，可透過query DR 來補齊遺漏的資訊。
	 * 
	 * @return
	 * @throws IOException
	 */
	public static Hashtable smsQueryDr(String msid, List<String> destAddresses) throws IOException {
		String smsApi = "smsquerydr";
		String xml =
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?> " + 
			"<SmsQueryDrReq>" + 
			"    <SysId>" + Common.MPUSH_SYS_ID + "</SysId >" + 
			"    <MessageId>" + msid + "</MessageId >";
			for(String destAddress : destAddresses){
				xml += "<DestAddress>"+destAddress+"</DestAddress>";
			}
			xml += "</SmsQueryDrReq>";
		
		URL url = new URL(Common.MPUSH_API_URL+"/mpushapi/"+smsApi);
		
		URLConnection urlConn = url.openConnection();
		urlConn.setDoInput(true);
		urlConn.setDoOutput(true);
		urlConn.setUseCaches(false);
		urlConn.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
		DataOutputStream printout = new DataOutputStream(urlConn.getOutputStream()); 
		String content = URLEncoder.encode(xml,"UTF-8");
		printout.writeBytes ("xml=" + content);
		
		printout.flush();
		printout.close();
		
		/******** expected result XML: 
		<?xml version="1.0" encoding="UTF-8"?>
<SmsQueryDrRes>
    
    <ResultCode>00000</ResultCode>
    <ResultText>Request successfully processed.</ResultText>
    <Receipt>
       <MessageId>111774</MessageId>
       <DestAddress>886955227034</DestAddress>
       <DeliveryStatus>delivered</DeliveryStatus>
       <SubmitDate>080401163445</SubmitDate>
       <DoneDate>080401163631</DoneDate>
       <Seq>1/3</Seq>
    </Receipt>
    <Receipt>
       <MessageId>111774</MessageId>
       <DestAddress>886955227035</DestAddress>
       <DeliveryStatus>enroute</DeliveryStatus>
       <ErrorCode>107</ErrorCode>
       <SubmitDate> 0804011635</SubmitDate>
    </Receipt>
</SmsQueryDrRes>
		 *********/
		
		Hashtable hashTable = new Hashtable();
		ReceiptSAXHandler handler = new ReceiptSAXHandler();
		InputStream in = urlConn.getInputStream();
		hashTable = handler.getXMLdata(in);
		String result = (String) hashTable.get("ResultCode");
		in.close();
		PrintUtil.outputItem("ResultCode",result);
		if(result != null && result.equals("00000")){
			return hashTable;
		}else
			return null;
	}
	
	/**
	 * 提供外部系統取得兩週內簡訊中心送到MPush的SMS。如果用戶上傳一個長簡訊至簡訊中心，則簡訊中心將會拆分成多則SMS傳送給MPush，
	 * MPush會在24小時內等待所有長簡訊內容都收到後，再整合成一則SMS以供外部系統取得。
	 * 
	 * @return
	 * @throws IOException
	 */
	public static Hashtable smsRetrieveMo() throws IOException {
		String smsApi = "smsretrievemo";
		String xml =
			 "<?xml version=\"1.0\" encoding=\"UTF-8\"?> " + 
			"<SmsRetrieveMoReq>" + 
			"    <SysId>" + Common.MPUSH_SYS_ID + "</SysId >" + 
			"</SmsRetrieveMoReq>";
		
		URL url = new URL(Common.MPUSH_API_URL+"/mpushapi/"+smsApi);
		
		URLConnection urlConn = url.openConnection();
		urlConn.setDoInput(true);
		urlConn.setDoOutput(true);
		urlConn.setUseCaches(false);
		urlConn.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 
		DataOutputStream printout = new DataOutputStream(urlConn.getOutputStream()); 
		String content = URLEncoder.encode(xml,"UTF-8");
		printout.writeBytes ("xml=" + content);
		
		printout.flush();
		printout.close();
		
		/******** expected result XML: 
		<?xml version="1.0" encoding="UTF-8"?>
<SmsRetrieveMoRes>
   
    <ResultCode>00000</ResultCode>
    <ResultText>Request successfully processed.</ResultText>
　<SmsMessage>
　    <MessageId>12340001</MessageId>
        <SrcAddress>886955227034</SrcAddress>
        <DestAddress>886955227035</DestAddress>
        <SmsBody>[base64 encoded string of Hi. Test Message.]</SmsBody>
    </SmsMessage>
　<SmsMessage>
　    <MessageId>12340002</MessageId>
        <SrcAddress>886955227036</SrcAddress>
        <DestAddress>886955227037</DestAddress>
        <SmsBody>[base64 encoded string of Hi. Test Message.]</SmsBody>
    </SmsMessage>
</SmsRetrieveMoRes>
		　*********/
		
		Hashtable hashTable = new Hashtable();
		ReceiptSAXHandler handler = new ReceiptSAXHandler();
		InputStream in = urlConn.getInputStream();
		hashTable = handler.getXMLdata(in);
		String result = (String) hashTable.get("ResultCode");
		in.close();
		PrintUtil.outputItem("ResultCode",result);
		if(result != null && result.equals("00000")){
			return hashTable;
		}else
			return null;
	}

	public static void main(String[] args) throws IOException {
//		SMSUtil.sendSMS("test", "8618675951332");
		SMSUtil.sendSMS("e55688平台已受理您的更換門號申請，以下是本次申請驗證碼"
				+ "45678" + " [驗證碼60秒內有效]", "+8618675951332");//0926898693
	//	smsRetrieveDr();
	}

}